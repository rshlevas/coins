<?php
// src/AppBundle/Repository/RegionRepository.php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class RegionRepository extends EntityRepository
{
    public function findAllOrderedById()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT r FROM AppBundle:Region r ORDER BY r.id ASC'
            )
            ->getArrayResult();
    }
}