<?php
// src/AppBundle/Repository/ProductRepository.php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
    public function findOneByIdJoinedToCountry($productId)
    {
        $query = $this->getEntityManager()->createQuery(
            'SELECT p, c FROM AppBundle:Product p
            JOIN p.country c
            WHERE p.id = :id'
        )->setParameter('id', $productId);

        try {
            return $query->getArrayResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    public function getProductsByType($type)
    {
        $query = $this->getEntityManager()->createQuery(
            'SELECT p.id, p.name, p.price, c.name as country_name 
            FROM AppBundle:Product p
            JOIN p.country c
            WHERE p.type = :type'
        )->setParameter('type', $type);

        try {
            return $query->getArrayResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}

