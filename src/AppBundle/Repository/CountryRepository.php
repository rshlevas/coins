<?php
// src/AppBundle/Repository/CountryRepository.php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class CountryRepository extends EntityRepository
{
    public function findAllOrderedByName()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT c FROM AppBundle:Country c ORDER BY c.name ASC'
            )
            ->getArrayResult();
    }
    
    public function findByRegionOrderedByName($region_id)
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT c FROM AppBundle:Country c 
                WHERE c.region_id = :id
                ORDER BY c.name ASC'
            )->setParameter('id', $region_id)
            ->getResult();
    }
}