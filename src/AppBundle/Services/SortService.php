<?php

 // src/AppBundle/Services/SortSevice.php
    namespace AppBundle\Services;

class SortService
{
    /*
     * Static function to sort products
     */
    public static function sortProducts($products)
    {
        $products_sort = [];
        foreach ($products as $product) {
            if (!empty($product)) {
                foreach ($product as $item) {
                    $products_sort[] = $item;
                }
            }
        }
        return $products_sort;
    } 
    
    /*
     * Static function to remove product from array by ID
     */
    public static function removeProductByID($all_products, $product)
    {
        $other_products = [];
        foreach ($all_products as $product_item) {
            if ($product_item->getId() !== $product->getId()) {
                $other_products[] = $product_item;
            } 
        }
        return $other_products;
    } 
    
    /*
     * Static function to remove product from array by TypeId
     */
    public static function removeProductByType($products, $type_id)
    {
        $other_products = [];
        foreach ($products as $product_item) {
            if ($product_item->getType() == $type_id) {
                $other_products[] = $product_item;
            } 
        }
        return $other_products;
    } 
    /*
     * Static function to form product_info array
     */
    public static function formProductsInfoForOrder($products, $productsInCart)
    {
        $product_info = [];
        $i = 0;
        foreach ($products as $product) {
            $product_info[$i]['id'] = $product->getId();
            $product_info[$i]['count'] = $productsInCart[$product->getId()];
            $product_info[$i]['name'] = $product->getName();
            $product_info[$i]['price'] = $product->getPrice();
            $i++;
        }
        return $product_info;
    } 
    /*
     * Static function to form user_info array
     */
    public static function formUserInfoForOrder($data, $user)
    {
        $user_info = [];
        if ($user) {
            $user_info['id'] = $user->getId();
        } else {
            $user_info['id'] = NULL;
        }
        $user_info['email'] = $data['email'];
        $user_info['name'] = $data['name'];
        $user_info['phone'] = $data['phone'];
        return $user_info;
    }
}
   
