<?php
// src/AppBundle/Entity/Product.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 * @ORM\Table(name="product")
 */
class Product
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;
    
    /**
     * @ORM\Column(type="string", length=10)
     */
    private $year;
    
    /**
     * @ORM\Column(type="string", length=30)
     */
    private $price;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $count;
    
    /**
     * @ORM\Column(type="text")
     */
    private $description;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $type;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $country_id;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $status;
    
    /**
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="products")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $country;
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function getYear()
    {
        return $this->year;
    }
    
    public function getPrice()
    {
        return $this->price;
    }
    
    public function getCount()
    {
        return $this->count;
    }
    
    public function getDescription()
    {
        return $this->description;
    }
    
    public function getType()
    {
        return $this->type;
    }
    
    public function getCountryId()
    {
        return $this->country_id;
    }
    
    public function getStatus()
    {
        return $this->status;
    }
    
    public function getCountry()
    {
        return $this->country;
    }
}
