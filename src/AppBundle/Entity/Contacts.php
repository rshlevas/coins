<?php
// src/AppBundle/Entity/Contacts.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CountactRepository")
 * @ORM\Table(name="contacts")
 */
class Contacts
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $email;
    /**
     * @ORM\Column(type="text")
     */
    private $text;
    
    /**
     * @ORM\Column(type="date")
     */
    private $date;
    
    public function getId()
    {
        return $this->id;
    }
    
    public function setId($id)
    {
        return $this->id = $id;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function setName($name)
    {
        return $this->name = $name;
    }
    
    public function getEmail()
    {
        return $this->email;
    }
    
    public function setEmail($email)
    {
        return $this->email = $email;
    }
    
    public function getText()
    {
        return $this->text;
    }
    
    public function setText($text)
    {
        return $this->text = $text;
    }
    
    public function getDate()
    {
        return $this->date;
    }
    
    public function setDate(\DateTime $dueDate = null)
    {
        return $this->date = $dueDate;
    }
}