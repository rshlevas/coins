<?php
// src/AppBundle/Entity/Country.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CountryRepository")
 * @ORM\Table(name="country")
 */
class Country
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $region_id;
    

    /**
     * @ORM\ManyToOne(targetEntity="Region", inversedBy="countries")
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     */
    private $region;
    
    /**
     * @ORM\OneToMany(targetEntity="Product", mappedBy="country")
     */
    private $products;
    
    public function __construct()
    {
        $this->products = new ArrayCollection();
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function getProducts()
    {
        return $this->products;
    }
    
    public function getRegion()
    {
        return $this->region;
    }
    
    public function getRegionId()
    {
        return $this->region_id;
    }
}
