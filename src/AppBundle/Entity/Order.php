<?php
// src/AppBundle/Entity/Order.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrderRepository")
 * @ORM\Table(name="order_table")
 */
class Order
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(type="text")
     */
    private $products;
    
    /**
     * @ORM\Column(type="text")
     */
    private $user;
    
    /**
     * @ORM\Column(type="date")
     */
    private $date;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $status;
    
    /**
     * @ORM\Column(type="float")
     */
    private $summ;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $user_id;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $payment_type;
    
    /**
     * @ORM\Column(type="integer")
     */
    private $delivery_type;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="orders")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user_info;
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getProducts()
    {
        return $this->products;
    }
    
    public function setProducts($products)
    {
        return $this->products = $products;
    }
    
    public function getUser()
    {
        return $this->user;
    }
    
    public function setUser($user)
    {
        return $this->user = $user;
    }
    
    public function getSumm()
    {
        return $this->summ;
    }
    
    public function setSumm($summ)
    {
        return $this->summ = $summ;
    }
    
    public function getUserId()
    {
        return $this->user_id;
    }
    
    public function setUserId($user_id)
    {
        return $this->user_id = $user_id;
    }
    
    public function getDate()
    {
        return $this->date;
    }
    
    public function setDate(\DateTime $dueDate = null)
    {
        return $this->date = $dueDate;
    }
    
    public function getStatus()
    {
        return $this->status;
    }
      
    public function setStatus($status)
    {
        return $this->status = $status;
    }
    
    public function getPaymentType()
    {
        return $this->payment_type;
    }
      
    public function setPaymentType($payment_type)
    {
        return $this->payment_type = $payment_type;
    }
    
    public function getDeliveryType()
    {
        return $this->delivery_type;
    }
      
    public function setDeliveryType($delivery_type)
    {
        return $this->delivery_type = $delivery_type;
    }
    
    public function getUserInfo() 
    {
        return $this->user_info;
    }
    
    public function setUserInfo(User $user_info) 
    {
        return $this->user_info = $user_info;
    }
}
