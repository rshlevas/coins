<?php

    // src/AppBundle/Controller/SiteController.php
    namespace AppBundle\Controller;

    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpFoundation\Request;
    use Symfony\Component\HttpFoundation\Session\Session;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use AppBundle\Entity\Contacts;
    use AppBundle\Entity\User;
    use Doctrine\ORM\EntityManagerInterface;
    use Doctrine\Common\Persistence\ManagerRegistry;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\Extension\Core\Type\TextareaType;
    use Symfony\Component\Form\Extension\Core\Type\EmailType;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;

    class SiteController extends Controller 
    {
        /**
         * @Route("/")
         */
        public function indexAction(Session $session)
        {
            
            return new \Symfony\Component\HttpFoundation\RedirectResponse ('\monety');
           
        }
        /**
         * @Route("/contacts/", name="contacts")
         */
        public function contactsAction(
                EntityManagerInterface $em, 
                Request $request, 
                \Swift_Mailer $mailer,
                Session $session
            )
        {
            $user = $this->getUser();
            $contact = new Contacts();
            if ($user == NULL) {
                $contact->setName('Ваше имя');
                $contact->setEmail('Ваш email');   
            }
            else {
                $contact->setName($user->getUsername());
                $contact->setEmail($user->getEmail());
            }
            $contact->setText('Введите текст сообщения');
            $form = $this->createFormBuilder($contact)
                ->add('name', TextType::class) 
                ->add('email', EmailType::class) 
                ->add('text', TextareaType::class) 
                ->add('save', SubmitType::class, ['label' => 'Отправить'])
                ->getForm();
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $contact = $form->getData();   
                $contact->setDate(new \DateTime);
                $em->persist($contact);
                $em->flush();
                $message = (new \Swift_Message('Hello Email'))
                    ->setFrom('send@example.com')
                    ->setTo('rshlevas@gmail.com')
                    ->setBody($this->renderView('emails/contact.html.twig', [
                            'contact' => $contact,
                        ]),
                        'text/html'
                    );

                $mailer->send($message);
                return $this->render('site/contacts_success.html.twig');
            }
            return $this->render('site/contacts.html.twig', [
                'form' => $form->createView(),
            ]);
        }
    }
    