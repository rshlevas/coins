<?php

    // src/AppBundle/Controller/ProductController.php
    namespace AppBundle\Controller;

    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use AppBundle\Entity\Country;
    use AppBundle\Entity\Product;
    use AppBundle\Entity\Region;
    use AppBundle\Services\SortService;
    use AppBundle\Services\NamesFolder;
    use Doctrine\ORM\EntityManagerInterface;
    use Doctrine\Common\Persistence\ManagerRegistry;
    
    class ProductController extends Controller 
    {
        /**
         * @Route("/product/{id}", requirements={"id": "\d+"})
         */
        public function viewAction(Product $product, EntityManagerInterface $em)
        {
            $all_products = $product->getCountry()->getProducts()->getValues();
            $other_products = SortService::removeProductByID($all_products, $product);
            
            $region = $product->getCountry()->getRegion();
            $type = NamesFolder::getTypeRouteById($product->getType());

            return $this->render('product/view.html.twig', [
                'product' => $product,
                'other_products' => $other_products,
                'region' => $region,
                'type' => $type,
            ]);
        }
    }