<?php

    // src/AppBundle/Controller/CategoryController.php
    namespace AppBundle\Controller;

    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use AppBundle\Entity\Country;
    use AppBundle\Entity\Product;
    use AppBundle\Entity\Region;
    use AppBundle\Services\NamesFolder;
    use AppBundle\Services\SortService;
    use Doctrine\ORM\EntityManagerInterface;
    use Doctrine\Common\Persistence\ManagerRegistry;
    
    class CategoryController extends Controller 
    {
        /**
         * @Route("/{type}/", requirements={"type": "monety|banknoty|aksesuary"})
         */
        public function viewTypeAction($type, EntityManagerInterface $em)
        {   
            $type_int = NamesFolder::getType($type);
            if ($type_int == -1) {
                return $this->render('404/index.html.twig');
            }
            $regions_for_route = NamesFolder::getRegionCollection();
            $regions = $em->getRepository('AppBundle:Region')
                ->findAllOrderedById();
            $countries = $em->getRepository('AppBundle:Country')
                ->findAllOrderedByName();
            $products = $em->getRepository('AppBundle:Product')
                ->getProductsByType($type_int);

            return $this->render('category/main.html.twig', [
                'regions' => $regions,
                'countries' => $countries,
                'products' => $products,
                'type' => $type,
                'regions_for_route' => $regions_for_route,
            ]);
        }
        
        /**
         * @Route(
         *     "/{type}/{region}", 
         *     requirements={
         *         "type": "monety|banknoty|aksesuary",
         *         "region": "europe|africa|n_america|s_america|asia|australia|other" 
         *     }
         * )
         */
        public function viewRegionAction($type, $region, EntityManagerInterface $em)
        {   
            $type_int = NamesFolder::getType($type);
            if ($type_int == -1) {
                return $this->render('404/index.html.twig');
            }
            $region_int = NamesFolder::getRegion($region);
            if ($region_int == -1) {
                return $this->render('404/index.html.twig');
            }
            $regions_for_route = NamesFolder::getRegionCollection();
            $regions = $em->getRepository('AppBundle:Region')
                ->findAllOrderedById();
            $countries = $em->getRepository('AppBundle:Country')
                ->findByRegionOrderedByName($region_int);
            $products = [];
            foreach ($countries as $country) {
               $products[] = $country->getProducts()->getValues();
            }
            $products_sort = SortService::sortProducts($products);
            $products_collection = SortService::removeProductByType($products_sort, $type_int);
            return $this->render('category/region.html.twig', [
                'regions' => $regions,
                'countries' => $countries,
                'products' => $products_collection,
                'type' => $type,
                'regions_for_route' => $regions_for_route,
            ]);
        }

    }