<?php

    namespace AppBundle\Form;

    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Symfony\Component\Form\Extension\Core\Type\SubmitType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\Extension\Core\Type\TextareaType;
    use AppBundle\Services\NamesFolder;
    use Symfony\Component\Form\Extension\Core\Type\EmailType;
    use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

    class OrderType extends AbstractType
    {
        public function buildForm(FormBuilderInterface $builder, array $options)
        {
            $builder
                ->add('name', TextType::class, ['label' => 'Укажите ваше имя']) 
                ->add('email', EmailType::class, ['label' => 'Email'])
                ->add('phone', TextType::class, ['label' => 'Телефон']) 
                ->add('delivery_type', ChoiceType::class, [
                    'choices'  => NamesFolder::getDeliveryType(),
                    'label' => 'Выберите способ доставки',
                ])->add('payment_type', ChoiceType::class, [
                    'choices'  => NamesFolder::getPaymentType(),
                    'label' => 'Выберите способ оплаты',
                ])
                ->add('save', SubmitType::class, ['label' => 'Подтвердить'])
            ;
        }
    }