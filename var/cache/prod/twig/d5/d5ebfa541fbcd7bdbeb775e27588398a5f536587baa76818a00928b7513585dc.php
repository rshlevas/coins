<?php

/* @FOSUser/Registration/check_email.html.twig */
class __TwigTemplate_e7293f6d220953231cd5c5fdf82fae5a15c89d4db7cb4f295102e103232b135c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("@FOSUser/header.html.twig", "@FOSUser/Registration/check_email.html.twig", 1)->display($context);
        // line 2
        echo "
";
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 24
        echo "
";
        // line 25
        $this->displayBlock('footer', $context, $blocks);
    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 6
        echo "    <section>
    <div class=\"container\">
        <div class=\"row\">
            
                <br>
                <br>
                <br>
                <h4><center>Аккаунт успешно создан! На Bаш email (";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "email", array()), "html", null, true);
        echo ") отправлено письмо с инструкциями!
                    Благодарим за регистрацию!</center></h4>
                <br>
                <br>
                <br>
                <br>

        </div>
    </div>
</section>
";
    }

    // line 25
    public function block_footer($context, array $blocks = array())
    {
        // line 26
        $this->loadTemplate("@FOSUser/footer.html.twig", "@FOSUser/Registration/check_email.html.twig", 26)->display($context);
    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 26,  65 => 25,  50 => 13,  41 => 6,  38 => 5,  34 => 25,  31 => 24,  29 => 5,  26 => 4,  23 => 2,  21 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Registration/check_email.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Registration\\check_email.html.twig");
    }
}
