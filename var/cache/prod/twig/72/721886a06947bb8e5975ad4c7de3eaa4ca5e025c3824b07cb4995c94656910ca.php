<?php

/* cart/checkout_mistake.html.twig */
class __TwigTemplate_caa23b256e6f367be08e7ca26badbe1582168097e785945a6630a0dd8167558d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("header.html.twig", "cart/checkout_mistake.html.twig", 1)->display($context);
        // line 2
        echo "
<section>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-4 col-sm-offset-4 padding-right\">
                <div class=\"signup-form\"><!--sign up form-->
                    <h2>Простите, но сервис временно недоступен</h2>
                    <h5>Вы можете оформить заказ по телефону либо отправить намо <a href=\"";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("contacts");
        echo "\">сообщение</a></h5>
            </div>
        </div>
    </div>
</section>
                    
";
        // line 15
        $this->loadTemplate("footer.html.twig", "cart/checkout_mistake.html.twig", 15)->display($context);
    }

    public function getTemplateName()
    {
        return "cart/checkout_mistake.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 15,  30 => 9,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "cart/checkout_mistake.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\cart\\checkout_mistake.html.twig");
    }
}
