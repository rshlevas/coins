<?php

/* :site:contacts_success.html.twig */
class __TwigTemplate_1e29088cbfa8b01d442e8c15291254d27edd0cd7ebdd49e303dafccfc7166ce1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo twig_include($this->env, $context, "header.html.twig");
        echo "

<section>
    <div class=\"container\">
        <div class=\"row\">
            <h2>Ваше собщение отправленно! Наш менеджер свяжется с Вами</h2>
        </div>
    </div>
</section>
               
";
        // line 11
        echo twig_include($this->env, $context, "footer.html.twig");
    }

    public function getTemplateName()
    {
        return ":site:contacts_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 11,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":site:contacts_success.html.twig", "D:\\wamp64\\www\\coins\\app/Resources\\views/site/contacts_success.html.twig");
    }
}
