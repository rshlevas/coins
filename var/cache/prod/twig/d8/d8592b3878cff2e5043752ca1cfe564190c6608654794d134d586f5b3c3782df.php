<?php

/* :category:main.html.twig */
class __TwigTemplate_f71e9e665d69380082d520d68773f36daf94e12422ee8edfc383ac81d3011174 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo twig_include($this->env, $context, "header.html.twig");
        echo "

<section>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-3\">
                <div class=\"left-sidebar\">
                    <div class=\"panel-group category-products\">
                        <h2>Регионы</h2>
                            ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["regions"]) ? $context["regions"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["region"]) {
            // line 11
            echo "                                <div class=\"panel panel-default\">
                                    <div class=\"panel-heading\">
                                        <h4 class=\"panel-title\">
                                            <a href=\"/";
            // line 14
            echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : null), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["regions_for_route"]) ? $context["regions_for_route"] : null), $this->getAttribute($context["region"], "id", array()), array(), "array"), "html", null, true);
            echo "\">                                                                                    
                                                   ";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["region"], "name", array()), "html", null, true);
            echo "
                                            </a>
                                        </h4>
                                    </div>
                                </div>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['region'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "                        <h2><a class=\"countries_touch\" href=\"#\">Страны</a></h2>
                            ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["countries"]) ? $context["countries"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
            // line 23
            echo "                                <div class=\"panel panel-default countries\" style=\"display:none\">
                                        <p class=\"panel-title-countries\">
                                            <a href=\"/";
            // line 25
            echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : null), "html", null, true);
            echo "/countries/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["country"], "id", array()), "html", null, true);
            echo "\">                                                                                    
                                                   ";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["country"], "name", array()), "html", null, true);
            echo "
                                            </a>
                                        </p>
                                </div>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "                    </div>
                </div>
            </div>

            <div class=\"col-sm-9 padding-right\">
                <div class=\"features_items\"><!--features_items-->
                    <h2 class=\"title text-center\">Последние товары</h2>

                    ";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 40
            echo "                        <div class=\"col-sm-4\">
                            <div class=\"product-image-wrapper\">
                                <div class=\"single-products\">
                                    <div class=\"productinfo text-center\">
                                        <img src=\"/images/product/";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo ".jpg\" alt=\"\" />
                                        <h2> ";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "price", array()), "html", null, true);
            echo "</h2>
                                        <p>
                                            <a href=\"/product/";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "\">
                                                ";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "country_name", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
            echo "
                                            </a>
                                        </p>
                                        <a href=\"#\" class=\"btn btn-default add-to-cart\" data-id=\"";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "\">
                                            <i class=\"fa fa-shopping-cart\"></i>В корзину
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        echo "                              

                </div><!--features_items-->
                
            </div>
        </div>
    </div>
</section>

<script>
    \$(document).ready(function(){
        \$(\".countries_touch\").click(function() {
            \$(\".panel.panel-default.countries\").slideToggle( \"fast\" );
            return false;
        });
    });
</script> 
                    
";
        // line 76
        echo twig_include($this->env, $context, "footer.html.twig");
    }

    public function getTemplateName()
    {
        return ":category:main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  161 => 76,  141 => 58,  127 => 51,  119 => 48,  115 => 47,  110 => 45,  106 => 44,  100 => 40,  96 => 39,  86 => 31,  75 => 26,  69 => 25,  65 => 23,  61 => 22,  58 => 21,  46 => 15,  40 => 14,  35 => 11,  31 => 10,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":category:main.html.twig", "D:\\wamp64\\www\\coins\\app/Resources\\views/category/main.html.twig");
    }
}
