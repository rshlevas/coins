<?php

/* site/contacts_success.html.twig */
class __TwigTemplate_fdd8ab55d6467123e5f376191617118cdb3dffc8e8a101cbd42c6ebce07da897 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo twig_include($this->env, $context, "header.html.twig");
        echo "

<section>
    <div class=\"container\">
        <div class=\"row\">
            <h2>Ваше собщение отправленно! Наш менеджер свяжется с Вами</h2>
        </div>
    </div>
</section>
               
";
        // line 11
        echo twig_include($this->env, $context, "footer.html.twig");
    }

    public function getTemplateName()
    {
        return "site/contacts_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 11,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "site/contacts_success.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\site\\contacts_success.html.twig");
    }
}
