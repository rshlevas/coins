<?php

/* country/index.html.twig */
class __TwigTemplate_d1b21444fc45a738a7cfa40c99c0bac1d2478463a789709f02cbecb86e07a588 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo twig_include($this->env, $context, "header.html.twig");
        echo "

<h1>Твой первый номер - ";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["number1"]) ? $context["number1"] : null), "html", null, true);
        echo "</h1>
</br>
<h2><a href='#'>Твой второй номер - ";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["number2"]) ? $context["number2"] : null), "html", null, true);
        echo "</a></h2>
</br>
<h3>Их сумма - ";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["summ"]) ? $context["summ"] : null), "html", null, true);
        echo "</h3>

";
        // line 9
        echo twig_include($this->env, $context, "footer.html.twig");
    }

    public function getTemplateName()
    {
        return "country/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 9,  34 => 7,  29 => 5,  24 => 3,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "country/index.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\country\\index.html.twig");
    }
}
