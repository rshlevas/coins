<?php

/* cart/checkout_success.html.twig */
class __TwigTemplate_3ad8537b5183d336334a2552274a8e8cf575f31f822b551fb3bf4ffd06997800 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("header.html.twig", "cart/checkout_success.html.twig", 1)->display($context);
        // line 2
        echo "
<section>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-4 col-sm-offset-4 padding-right\">
                <div class=\"signup-form\"><!--sign up form-->
                    <h2>Заказ успешно оформлен</h2>
                    <h5>Наш менеджер свяжется с вами!</h5>
                    <h5>Есть вопрос? <a href=\"";
        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("contacts");
        echo "\">Напишите</a> нам</h5>
            </div>
        </div>
    </div>
</section>
                    
";
        // line 16
        $this->loadTemplate("footer.html.twig", "cart/checkout_success.html.twig", 16)->display($context);
    }

    public function getTemplateName()
    {
        return "cart/checkout_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 16,  31 => 10,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "cart/checkout_success.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\cart\\checkout_success.html.twig");
    }
}
