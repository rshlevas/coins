<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_7709c44b4a3b05f23080fa1ba789fe1fe40467ae1c65876f17455ca34230be8e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("@FOSUser/header.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 1)->display($context);
        // line 2
        echo "
";
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 27
        echo "

";
        // line 29
        $this->displayBlock('footer', $context, $blocks);
    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 6
        echo "    <section>
    <div class=\"container\">
        <div class=\"row\">
            
                <br>
                <br>
                <br>
                <h3><center>Поздравляю ";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "html", null, true);
        echo ", Ваш аккаунт активирован</center></h3>
                ";
        // line 14
        if ((isset($context["targetUrl"]) ? $context["targetUrl"] : null)) {
            // line 15
            echo "                <p><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["targetUrl"]) ? $context["targetUrl"] : null), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
                ";
        }
        // line 17
        echo "                <br>
                <br>
                <br>
                <br>

        </div>
    </div>
</section>
    
";
    }

    // line 29
    public function block_footer($context, array $blocks = array())
    {
        // line 30
        $this->loadTemplate("@FOSUser/footer.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 30)->display($context);
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 30,  78 => 29,  65 => 17,  57 => 15,  55 => 14,  51 => 13,  42 => 6,  39 => 5,  35 => 29,  31 => 27,  29 => 5,  26 => 4,  23 => 2,  21 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Registration:confirmed.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Registration/confirmed.html.twig");
    }
}
