<?php

/* cart/no_products.html.twig */
class __TwigTemplate_d28bb2dedb46a885c810e07434058ad513a66d0ac920fbc95ab5fe0fc5319a9d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<br>
<h3>Корзина пуста</h3>
<br>
<a class=\"btn btn-default checkout\" href=\"/\"><i class=\"fa fa-shopping-cart\"></i> Вернуться к покупкам</a>";
    }

    public function getTemplateName()
    {
        return "cart/no_products.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "cart/no_products.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\cart\\no_products.html.twig");
    }
}
