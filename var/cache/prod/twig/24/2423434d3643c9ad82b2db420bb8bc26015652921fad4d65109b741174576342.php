<?php

/* @FOSUser/Registration/register.html.twig */
class __TwigTemplate_75e478b75d060a403efb1ccbdafd45a2dcf674a5fdc0f98418e707ad4ae32a4d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("@FOSUser/header.html.twig", "@FOSUser/Registration/register.html.twig", 1)->display($context);
        // line 2
        echo "<section>
    <div class=\"container\">
        <div class=\"row\">
            
            <div class=\"col-sm-4 col-sm-offset-4 padding-right\">
                ";
        // line 7
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 10
        echo "            
                <br>
                <p>Если вы уже зарегестрированы нажмите 
                   <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
        echo "\"> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("ТУТ", array(), "FOSUserBundle"), "html", null, true);
        echo "
                   </a> для входа</p>
                <br>
            </div>
        </div>
    </div>
</section>

";
        // line 21
        $this->displayBlock('footer', $context, $blocks);
    }

    // line 7
    public function block_fos_user_content($context, array $blocks = array())
    {
        // line 8
        echo "                ";
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "@FOSUser/Registration/register.html.twig", 8)->display($context);
        // line 9
        echo "                ";
    }

    // line 21
    public function block_footer($context, array $blocks = array())
    {
        // line 22
        $this->loadTemplate("@FOSUser/footer.html.twig", "@FOSUser/Registration/register.html.twig", 22)->display($context);
    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 22,  64 => 21,  60 => 9,  57 => 8,  54 => 7,  50 => 21,  37 => 13,  32 => 10,  30 => 7,  23 => 2,  21 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Registration/register.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Registration\\register.html.twig");
    }
}
