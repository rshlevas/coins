<?php

/* cart/view.html.twig */
class __TwigTemplate_51387bb3a95701e3a9803c24f0a6781c123f2994ec9217e1093090949d415267 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->loadTemplate("header.html.twig", "cart/view.html.twig", 1)->display($context);
        // line 2
        echo "
<section>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-9 padding-right\">
                <div class=\"features_items\"><!--features_items-->
                    <h2 class=\"title text-center\">Корзина</h2>
                    <div id=\"table_content\">
                        ";
        // line 10
        if ((isset($context["products"]) ? $context["products"] : null)) {
            // line 11
            echo "                        ";
            $this->loadTemplate("cart/products_table.html.twig", "cart/view.html.twig", 11)->display(array_merge($context, array("products" =>             // line 12
(isset($context["products"]) ? $context["products"] : null), "summ" =>             // line 13
(isset($context["summ"]) ? $context["summ"] : null), "count" =>             // line 14
(isset($context["count"]) ? $context["count"] : null))));
            // line 16
            echo "                        ";
        } else {
            // line 17
            echo "                        ";
            $this->loadTemplate("cart/no_products.html.twig", "cart/view.html.twig", 17)->display($context);
            echo "    
                        ";
        }
        // line 19
        echo "                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
                    
";
        // line 26
        $this->loadTemplate("footer.html.twig", "cart/view.html.twig", 26)->display($context);
    }

    public function getTemplateName()
    {
        return "cart/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 26,  48 => 19,  42 => 17,  39 => 16,  37 => 14,  36 => 13,  35 => 12,  33 => 11,  31 => 10,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "cart/view.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\cart\\view.html.twig");
    }
}
