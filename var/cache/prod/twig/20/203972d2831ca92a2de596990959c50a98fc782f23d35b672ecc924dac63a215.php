<?php

/* cart/products_table.html.twig */
class __TwigTemplate_b16253b888929915c46e299a5e7fa088bf6d149b79f5bec43099cd5159fdbdc5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "                        <p>Вы выбрали такие товары:</p>
                        <table class=\"table-bordered table-striped table\">
                            <tr>
                                <th>№</th>
                                <th>Название</th>
                                <th>Стомость, грн</th>
                                <th>Количество, шт</th>
                                <th>Удалить</th>
                            </tr>
                            ";
        // line 10
        $context["i"] = 1;
        // line 11
        echo "                            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 12
            echo "                                <tr>
                                    <td>";
            // line 13
            echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : null), "html", null, true);
            echo "</td>
                                    <td>
                                        <a href=\"/product/";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "\">
                                           ";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
            echo "
                                        </a>
                                    </td>
                                    <td>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "price", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["count"]) ? $context["count"] : null), $this->getAttribute($context["product"], "id", array()), array(), "array"), "html", null, true);
            echo "</td> 
                                    <td>
                                        <a href=\"/cart/delete/";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "\">
                                            <i class=\"fa fa-times\" data-id=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "\"></i>
                                        </a>
                                    </td>
                                </tr>
                                ";
            // line 27
            $context["i"] = ((isset($context["i"]) ? $context["i"] : null) + 1);
            // line 28
            echo "                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "                                <tr>
                                    <td colspan=\"4\">Общая стоимость, грн:</td>
                                    <td>";
        // line 31
        echo twig_escape_filter($this->env, (isset($context["summ"]) ? $context["summ"] : null), "html", null, true);
        echo "</td>
                                </tr>
                            
                        </table>
                        
                        <a class=\"btn btn-default checkout\" href=\"";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("cart_checkout");
        echo "\"><i class=\"fa fa-shopping-cart\"></i> Оформить заказ</a>
                        <a class=\"btn btn-default checkout\" href=\"/\"><i class=\"fa fa-shopping-cart\"></i> Вернуться к покупкам</a>

<script>
    \$(document).ready(function(){
        \$(\".fa.fa-times\").click(function () {
            var id = \$(this).attr(\"data-id\");
            \$.ajax({
                type: 'post',
                dataType: 'json',
                url: \"/cart/delete/\"+id ,
                success: function (data) {
                    \$(\"#table_content\").html(data.html);

                }
            });
            \$.ajax({
                type: 'post',
                url: '";
        // line 54
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("cart_count");
        echo "',
                success: function (data) {
                    if (data !== 0) {
                        \$(\"#cart-count\").html(data);
                    }
                    else {
                        \$(\"#cart-count\").html(\"\");
                    }    
                }
            });
            return false;
        });
    }); 
    
</script>";
    }

    public function getTemplateName()
    {
        return "cart/products_table.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 54,  95 => 36,  87 => 31,  83 => 29,  77 => 28,  75 => 27,  68 => 23,  64 => 22,  59 => 20,  55 => 19,  49 => 16,  45 => 15,  40 => 13,  37 => 12,  32 => 11,  30 => 10,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "cart/products_table.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\cart\\products_table.html.twig");
    }
}
