<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_84e6ab581deeaf6bafcf73335438265ac4614061c27dd5666d59ce81e331b333 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c545d7138e10ffe94c2be7258b2898b6c8cc3daf4eea168a1ee5634917f6d864 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c545d7138e10ffe94c2be7258b2898b6c8cc3daf4eea168a1ee5634917f6d864->enter($__internal_c545d7138e10ffe94c2be7258b2898b6c8cc3daf4eea168a1ee5634917f6d864_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => (isset($context["exception"]) ? $context["exception"] : null)));
        echo "
";
        
        $__internal_c545d7138e10ffe94c2be7258b2898b6c8cc3daf4eea168a1ee5634917f6d864->leave($__internal_c545d7138e10ffe94c2be7258b2898b6c8cc3daf4eea168a1ee5634917f6d864_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "TwigBundle:Exception:exception.atom.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/exception.atom.twig");
    }
}
