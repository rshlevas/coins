<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_5725c628bba163228e1c5d604e135fae2734076f05525b93d78b926bb4c5b95d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_42ef59e7db15ba724ce9ae73b9c74ce3de57ef3545bac8540e55effcd3e18bd6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_42ef59e7db15ba724ce9ae73b9c74ce3de57ef3545bac8540e55effcd3e18bd6->enter($__internal_42ef59e7db15ba724ce9ae73b9c74ce3de57ef3545bac8540e55effcd3e18bd6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_42ef59e7db15ba724ce9ae73b9c74ce3de57ef3545bac8540e55effcd3e18bd6->leave($__internal_42ef59e7db15ba724ce9ae73b9c74ce3de57ef3545bac8540e55effcd3e18bd6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/form_widget.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_widget.html.php");
    }
}
