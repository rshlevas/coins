<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_ab8037a62e34ed1bd5b89a7e5cd3cba8ff8eb5b2584cae0671c45b08ccc5ce60 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bc499ea8fb2b97cb70a1c7407f25e3237fd658133c08d42834cf4d5192e13399 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bc499ea8fb2b97cb70a1c7407f25e3237fd658133c08d42834cf4d5192e13399->enter($__internal_bc499ea8fb2b97cb70a1c7407f25e3237fd658133c08d42834cf4d5192e13399_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_bc499ea8fb2b97cb70a1c7407f25e3237fd658133c08d42834cf4d5192e13399->leave($__internal_bc499ea8fb2b97cb70a1c7407f25e3237fd658133c08d42834cf4d5192e13399_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/reset_widget.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\reset_widget.html.php");
    }
}
