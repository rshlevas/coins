<?php

/* :country:index.html.twig */
class __TwigTemplate_ffb3e1f983af9066e5dbf83e863a90d15a425b731d095f9016e8ea8e3d26fcfe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_38e9a0d090c78024497d979fd0618d9c43af1d56d1d2de4c7094f4da96a8a0c6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_38e9a0d090c78024497d979fd0618d9c43af1d56d1d2de4c7094f4da96a8a0c6->enter($__internal_38e9a0d090c78024497d979fd0618d9c43af1d56d1d2de4c7094f4da96a8a0c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":country:index.html.twig"));

        // line 1
        echo twig_include($this->env, $context, "header.html.twig");
        echo "

<h1>Твой первый номер - ";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["number1"]) ? $context["number1"] : null), "html", null, true);
        echo "</h1>
</br>
<h2><a href='#'>Твой второй номер - ";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["number2"]) ? $context["number2"] : null), "html", null, true);
        echo "</a></h2>
</br>
<h3>Их сумма - ";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["summ"]) ? $context["summ"] : null), "html", null, true);
        echo "</h3>

";
        // line 9
        echo twig_include($this->env, $context, "footer.html.twig");
        
        $__internal_38e9a0d090c78024497d979fd0618d9c43af1d56d1d2de4c7094f4da96a8a0c6->leave($__internal_38e9a0d090c78024497d979fd0618d9c43af1d56d1d2de4c7094f4da96a8a0c6_prof);

    }

    public function getTemplateName()
    {
        return ":country:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 9,  37 => 7,  32 => 5,  27 => 3,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":country:index.html.twig", "D:\\wamp64\\www\\coins\\app/Resources\\views/country/index.html.twig");
    }
}
