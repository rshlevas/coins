<?php

/* FOSUserBundle:Profile:show.html.twig */
class __TwigTemplate_ba71242dfe9d656003c94e255139407e88a8de05e9da4e9028fa00ec8712984f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1f568abfbacb207876681cb3196df897d1468cecf5608fab6edfdf8efb64c174 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1f568abfbacb207876681cb3196df897d1468cecf5608fab6edfdf8efb64c174->enter($__internal_1f568abfbacb207876681cb3196df897d1468cecf5608fab6edfdf8efb64c174_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:show.html.twig"));

        // line 1
        $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:show.html.twig", 1)->display($context);
        // line 2
        echo "
<p><a href=\"";
        // line 3
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_profile_edit");
        echo "\">Edit</a></p>
<br>
<p><a href=\"";
        // line 5
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_profile_history");
        echo "\">History</a></p>

";
        // line 7
        $this->displayBlock('fos_user_content', $context, $blocks);
        
        $__internal_1f568abfbacb207876681cb3196df897d1468cecf5608fab6edfdf8efb64c174->leave($__internal_1f568abfbacb207876681cb3196df897d1468cecf5608fab6edfdf8efb64c174_prof);

    }

    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_cb582c8190be1598a1f48d7fcf13e32efcb9d938d77ee0638edb3d0c32ca4ada = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cb582c8190be1598a1f48d7fcf13e32efcb9d938d77ee0638edb3d0c32ca4ada->enter($__internal_cb582c8190be1598a1f48d7fcf13e32efcb9d938d77ee0638edb3d0c32ca4ada_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 8
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "FOSUserBundle:Profile:show.html.twig", 8)->display($context);
        
        $__internal_cb582c8190be1598a1f48d7fcf13e32efcb9d938d77ee0638edb3d0c32ca4ada->leave($__internal_cb582c8190be1598a1f48d7fcf13e32efcb9d938d77ee0638edb3d0c32ca4ada_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 8,  38 => 7,  33 => 5,  28 => 3,  25 => 2,  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Profile:show.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Profile/show.html.twig");
    }
}
