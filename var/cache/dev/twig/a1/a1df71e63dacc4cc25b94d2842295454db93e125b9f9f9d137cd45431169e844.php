<?php

/* cart/checkout_success.html.twig */
class __TwigTemplate_bddf429a606158b30954fe05972762d2826e8483ffb42e8271c4a49370cb2cca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bada8dc133f4332aa25952939ba36fcced077d32f940f76de820d9a6d5d6a700 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bada8dc133f4332aa25952939ba36fcced077d32f940f76de820d9a6d5d6a700->enter($__internal_bada8dc133f4332aa25952939ba36fcced077d32f940f76de820d9a6d5d6a700_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "cart/checkout_success.html.twig"));

        // line 1
        $this->loadTemplate("header.html.twig", "cart/checkout_success.html.twig", 1)->display($context);
        // line 2
        echo "
<section>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-4 col-sm-offset-4 padding-right\">
                <div class=\"signup-form\"><!--sign up form-->
                    <h2>Заказ успешно оформлен</h2>
                    <h5>Наш менеджер свяжется с вами!</h5>
                    <h5>Есть вопрос? <a href=\"";
        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("contacts");
        echo "\">Напишите</a> нам</h5>
            </div>
        </div>
    </div>
</section>
                    
";
        // line 16
        $this->loadTemplate("footer.html.twig", "cart/checkout_success.html.twig", 16)->display($context);
        
        $__internal_bada8dc133f4332aa25952939ba36fcced077d32f940f76de820d9a6d5d6a700->leave($__internal_bada8dc133f4332aa25952939ba36fcced077d32f940f76de820d9a6d5d6a700_prof);

    }

    public function getTemplateName()
    {
        return "cart/checkout_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 16,  34 => 10,  24 => 2,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "cart/checkout_success.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\cart\\checkout_success.html.twig");
    }
}
