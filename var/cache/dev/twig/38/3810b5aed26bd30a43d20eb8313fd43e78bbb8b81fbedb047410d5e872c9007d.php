<?php

/* @FOSUser/footer.html.twig */
class __TwigTemplate_69d128e1e8b1086e9291ed50ed4ac48c5c1be901b9408bd27c1c2adb37c02a62 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3f837a331a33a5d294359dfc771ad6bca56b2a449ea68863ce4174f948a2b32a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3f837a331a33a5d294359dfc771ad6bca56b2a449ea68863ce4174f948a2b32a->enter($__internal_3f837a331a33a5d294359dfc771ad6bca56b2a449ea68863ce4174f948a2b32a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/footer.html.twig"));

        // line 2
        echo "

<footer id=\"footer\" class=\"page-footer\"><!--Footer-->
    <div class=\"footer-bottom\">
        <div class=\"container\">
            <div class=\"row\">
                <p class=\"pull-left\">Copyright © 2016</p>
                <p class=\"pull-right\"></p>
            </div>
        </div>
    </div>
</footer><!--/Footer-->





</body>
</html>";
        
        $__internal_3f837a331a33a5d294359dfc771ad6bca56b2a449ea68863ce4174f948a2b32a->leave($__internal_3f837a331a33a5d294359dfc771ad6bca56b2a449ea68863ce4174f948a2b32a_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/footer.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\footer.html.twig");
    }
}
