<?php

/* country/index.html.twig */
class __TwigTemplate_a863d82147a9fed4024426ba341faa253c4ed03e5560a59d0174e511c6477250 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6729ff170bea268fcac27e8874b8e3471387020242d166bc2e5c66edea1caae6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6729ff170bea268fcac27e8874b8e3471387020242d166bc2e5c66edea1caae6->enter($__internal_6729ff170bea268fcac27e8874b8e3471387020242d166bc2e5c66edea1caae6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "country/index.html.twig"));

        // line 1
        echo twig_include($this->env, $context, "header.html.twig");
        echo "

<h1>Твой первый номер - ";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["number1"]) ? $context["number1"] : null), "html", null, true);
        echo "</h1>
</br>
<h2><a href='#'>Твой второй номер - ";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["number2"]) ? $context["number2"] : null), "html", null, true);
        echo "</a></h2>
</br>
<h3>Их сумма - ";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["summ"]) ? $context["summ"] : null), "html", null, true);
        echo "</h3>

";
        // line 9
        echo twig_include($this->env, $context, "footer.html.twig");
        
        $__internal_6729ff170bea268fcac27e8874b8e3471387020242d166bc2e5c66edea1caae6->leave($__internal_6729ff170bea268fcac27e8874b8e3471387020242d166bc2e5c66edea1caae6_prof);

    }

    public function getTemplateName()
    {
        return "country/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 9,  37 => 7,  32 => 5,  27 => 3,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "country/index.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\country\\index.html.twig");
    }
}
