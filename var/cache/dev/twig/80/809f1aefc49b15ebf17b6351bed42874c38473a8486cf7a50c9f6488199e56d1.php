<?php

/* @Twig/Exception/exception.rdf.twig */
class __TwigTemplate_dec7c06c706c0511f5a8548afd8a406f834d48987f15ecee7ba531db36dfa856 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5bc2c5a358559ca5469ca79705195d2ce96979bfd3091b15b7b4db2c01d49f21 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5bc2c5a358559ca5469ca79705195d2ce96979bfd3091b15b7b4db2c01d49f21->enter($__internal_5bc2c5a358559ca5469ca79705195d2ce96979bfd3091b15b7b4db2c01d49f21_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => (isset($context["exception"]) ? $context["exception"] : null)));
        echo "
";
        
        $__internal_5bc2c5a358559ca5469ca79705195d2ce96979bfd3091b15b7b4db2c01d49f21->leave($__internal_5bc2c5a358559ca5469ca79705195d2ce96979bfd3091b15b7b4db2c01d49f21_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Twig/Exception/exception.rdf.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception.rdf.twig");
    }
}
