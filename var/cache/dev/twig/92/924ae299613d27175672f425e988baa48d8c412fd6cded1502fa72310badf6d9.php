<?php

/* category/region.html.twig */
class __TwigTemplate_47c00cbc31d60dfa5343683f6056d595864f635ad175534b7a5a7d6ad6cd3f2b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_274cddc5a080df644747d4833309e03cd18a77f7b08d91e8961386958b2762a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_274cddc5a080df644747d4833309e03cd18a77f7b08d91e8961386958b2762a5->enter($__internal_274cddc5a080df644747d4833309e03cd18a77f7b08d91e8961386958b2762a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "category/region.html.twig"));

        // line 1
        echo twig_include($this->env, $context, "header.html.twig");
        echo "

<section>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-3\">
                <div class=\"left-sidebar\">
                    <div class=\"panel-group category-products\">
                        <h2><a class=\"countries_touch\" href=\"#\">Другие страны из региона</a></h2>
                            ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["countries"]) ? $context["countries"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
            // line 11
            echo "                                <div class=\"panel panel-default countries\" style=\"display:none\">
                                        <p class=\"panel-title-countries\">
                                            <a href=\"/";
            // line 13
            echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : null), "html", null, true);
            echo "/countries/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["country"], "id", array()), "html", null, true);
            echo "\">                                                                                    
                                                   ";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute($context["country"], "name", array()), "html", null, true);
            echo "
                                            </a>
                                        </p>
                                </div>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "                        <h2>Регионы</h2>
                            ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["regions"]) ? $context["regions"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["region"]) {
            // line 21
            echo "                                <div class=\"panel panel-default\">
                                    <div class=\"panel-heading\">
                                        <h4 class=\"panel-title\">
                                            <a href=\"/";
            // line 24
            echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : null), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["regions_for_route"]) ? $context["regions_for_route"] : null), $this->getAttribute($context["region"], "id", array()), array(), "array"), "html", null, true);
            echo "\">                                                                                    
                                                   ";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute($context["region"], "name", array()), "html", null, true);
            echo "
                                            </a>
                                        </h4>
                                    </div>
                                </div>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['region'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "                    </div>
                </div>
            </div>

            <div class=\"col-sm-9 padding-right\">
                <div class=\"features_items\"><!--features_items-->
                    <h2 class=\"title text-center\">Последние товары</h2>

                    ";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 40
            echo "                        <div class=\"col-sm-4\">
                            <div class=\"product-image-wrapper\">
                                <div class=\"single-products\">
                                    <div class=\"productinfo text-center\">
                                        <img src=\"/images/product/";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo ".jpg\" alt=\"\" />
                                        <h2> ";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "price", array()), "html", null, true);
            echo "</h2>
                                        <p>
                                            <a href=\"/product/";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "\">
                                                ";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["product"], "country", array()), "name", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
            echo "
                                            </a>
                                        </p>
                                        <a href=\"#\" class=\"btn btn-default add-to-cart\" data-id=\"";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "\">
                                            <i class=\"fa fa-shopping-cart\"></i>В корзину
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        echo "                              

                </div><!--features_items-->
                
            </div>
        </div>
    </div>
</section>

<script>
    \$(document).ready(function(){
        \$(\".countries_touch\").click(function() {
            \$(\".panel.panel-default.countries\").slideToggle( \"fast\" );
            return false;
        });
    });
</script> 
                    
";
        // line 76
        echo twig_include($this->env, $context, "footer.html.twig");
        
        $__internal_274cddc5a080df644747d4833309e03cd18a77f7b08d91e8961386958b2762a5->leave($__internal_274cddc5a080df644747d4833309e03cd18a77f7b08d91e8961386958b2762a5_prof);

    }

    public function getTemplateName()
    {
        return "category/region.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  164 => 76,  144 => 58,  130 => 51,  122 => 48,  118 => 47,  113 => 45,  109 => 44,  103 => 40,  99 => 39,  89 => 31,  77 => 25,  71 => 24,  66 => 21,  62 => 20,  59 => 19,  48 => 14,  42 => 13,  38 => 11,  34 => 10,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "category/region.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\category\\region.html.twig");
    }
}
