<?php

/* FOSUserBundle:Registration:email.txt.twig */
class __TwigTemplate_604c68e303896a13ca38887ce643a80c28233b3acfac4902a5258330b4665a41 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7973f55bb824d74ff7316df5a5b4a947a84a192e20e796b767c1cc2e1e8a7ae1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7973f55bb824d74ff7316df5a5b4a947a84a192e20e796b767c1cc2e1e8a7ae1->enter($__internal_7973f55bb824d74ff7316df5a5b4a947a84a192e20e796b767c1cc2e1e8a7ae1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_7973f55bb824d74ff7316df5a5b4a947a84a192e20e796b767c1cc2e1e8a7ae1->leave($__internal_7973f55bb824d74ff7316df5a5b4a947a84a192e20e796b767c1cc2e1e8a7ae1_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_ca74527edace7a08e323f9a62efd3c069151836f95d721daff262d76b92615e8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ca74527edace7a08e323f9a62efd3c069151836f95d721daff262d76b92615e8->enter($__internal_ca74527edace7a08e323f9a62efd3c069151836f95d721daff262d76b92615e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : null)), "FOSUserBundle");
        
        $__internal_ca74527edace7a08e323f9a62efd3c069151836f95d721daff262d76b92615e8->leave($__internal_ca74527edace7a08e323f9a62efd3c069151836f95d721daff262d76b92615e8_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_00a25dc19a90e7ec1c786eb2382ca5e1ce8a74f7b25a00d38deb868624934a57 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_00a25dc19a90e7ec1c786eb2382ca5e1ce8a74f7b25a00d38deb868624934a57->enter($__internal_00a25dc19a90e7ec1c786eb2382ca5e1ce8a74f7b25a00d38deb868624934a57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : null)), "FOSUserBundle");
        echo "
";
        
        $__internal_00a25dc19a90e7ec1c786eb2382ca5e1ce8a74f7b25a00d38deb868624934a57->leave($__internal_00a25dc19a90e7ec1c786eb2382ca5e1ce8a74f7b25a00d38deb868624934a57_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_05ee0f1bd00161ea253d5765b5a726a2c7b3af6985742c47f3102ad76ca77cd0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_05ee0f1bd00161ea253d5765b5a726a2c7b3af6985742c47f3102ad76ca77cd0->enter($__internal_05ee0f1bd00161ea253d5765b5a726a2c7b3af6985742c47f3102ad76ca77cd0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_05ee0f1bd00161ea253d5765b5a726a2c7b3af6985742c47f3102ad76ca77cd0->leave($__internal_05ee0f1bd00161ea253d5765b5a726a2c7b3af6985742c47f3102ad76ca77cd0_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  67 => 13,  58 => 10,  52 => 8,  45 => 4,  39 => 2,  32 => 13,  30 => 8,  27 => 7,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Registration:email.txt.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Registration/email.txt.twig");
    }
}
