<?php

/* @FOSUser/Registration/confirmed.html.twig */
class __TwigTemplate_aa590384d21aac7d5cb8330b596d0880f43e512df0db5ee605c76d195a1f5574 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_edf6217d3e2800e5ac94c40543979434db47450b1300841523726bc4d9a43633 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_edf6217d3e2800e5ac94c40543979434db47450b1300841523726bc4d9a43633->enter($__internal_edf6217d3e2800e5ac94c40543979434db47450b1300841523726bc4d9a43633_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/confirmed.html.twig"));

        // line 1
        $this->loadTemplate("@FOSUser/header.html.twig", "@FOSUser/Registration/confirmed.html.twig", 1)->display($context);
        // line 2
        echo "
";
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 27
        echo "

";
        // line 29
        $this->displayBlock('footer', $context, $blocks);
        
        $__internal_edf6217d3e2800e5ac94c40543979434db47450b1300841523726bc4d9a43633->leave($__internal_edf6217d3e2800e5ac94c40543979434db47450b1300841523726bc4d9a43633_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_6b4f374d13d4d8f9f72c63562724d117d4740f91a8eac73838946febe3ea2bad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6b4f374d13d4d8f9f72c63562724d117d4740f91a8eac73838946febe3ea2bad->enter($__internal_6b4f374d13d4d8f9f72c63562724d117d4740f91a8eac73838946febe3ea2bad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <section>
    <div class=\"container\">
        <div class=\"row\">
            
                <br>
                <br>
                <br>
                <h3><center>Поздравляю ";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "html", null, true);
        echo ", Ваш аккаунт активирован</center></h3>
                ";
        // line 14
        if ((isset($context["targetUrl"]) ? $context["targetUrl"] : null)) {
            // line 15
            echo "                <p><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["targetUrl"]) ? $context["targetUrl"] : null), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
                ";
        }
        // line 17
        echo "                <br>
                <br>
                <br>
                <br>

        </div>
    </div>
</section>
    
";
        
        $__internal_6b4f374d13d4d8f9f72c63562724d117d4740f91a8eac73838946febe3ea2bad->leave($__internal_6b4f374d13d4d8f9f72c63562724d117d4740f91a8eac73838946febe3ea2bad_prof);

    }

    // line 29
    public function block_footer($context, array $blocks = array())
    {
        $__internal_502062e2ab2740d889b9c11e62c565daac617c70a127f5142338c47529b53c07 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_502062e2ab2740d889b9c11e62c565daac617c70a127f5142338c47529b53c07->enter($__internal_502062e2ab2740d889b9c11e62c565daac617c70a127f5142338c47529b53c07_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 30
        $this->loadTemplate("@FOSUser/footer.html.twig", "@FOSUser/Registration/confirmed.html.twig", 30)->display($context);
        
        $__internal_502062e2ab2740d889b9c11e62c565daac617c70a127f5142338c47529b53c07->leave($__internal_502062e2ab2740d889b9c11e62c565daac617c70a127f5142338c47529b53c07_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 30,  90 => 29,  74 => 17,  66 => 15,  64 => 14,  60 => 13,  51 => 6,  45 => 5,  38 => 29,  34 => 27,  32 => 5,  29 => 4,  26 => 2,  24 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Registration/confirmed.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Registration\\confirmed.html.twig");
    }
}
