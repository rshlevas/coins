<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_0de4fb358efcd82fa27c2a8b914fe0ebc082e17731eeffdf81e48d60c3aa02af extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fa40bb6d5b3d19440c9a732010503da67ffd52c868c321696dd0d035370c1c83 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fa40bb6d5b3d19440c9a732010503da67ffd52c868c321696dd0d035370c1c83->enter($__internal_fa40bb6d5b3d19440c9a732010503da67ffd52c868c321696dd0d035370c1c83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_fa40bb6d5b3d19440c9a732010503da67ffd52c868c321696dd0d035370c1c83->leave($__internal_fa40bb6d5b3d19440c9a732010503da67ffd52c868c321696dd0d035370c1c83_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/container_attributes.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\container_attributes.html.php");
    }
}
