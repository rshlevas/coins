<?php

/* @FOSUser/Resetting/request.html.twig */
class __TwigTemplate_7ead01fa15eed05767f62f3d9e555cad0d47474448a577fa4eed476d96cfd6f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Resetting/request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_11ccae79eb088dc77d758821738eb8a33b7163cff46ff52baa00481c6f87fbef = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_11ccae79eb088dc77d758821738eb8a33b7163cff46ff52baa00481c6f87fbef->enter($__internal_11ccae79eb088dc77d758821738eb8a33b7163cff46ff52baa00481c6f87fbef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_11ccae79eb088dc77d758821738eb8a33b7163cff46ff52baa00481c6f87fbef->leave($__internal_11ccae79eb088dc77d758821738eb8a33b7163cff46ff52baa00481c6f87fbef_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_50506fda1c4fbcd8e2ff913b034430b54fd68f6b749f88f8ee69d53bfb94acac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_50506fda1c4fbcd8e2ff913b034430b54fd68f6b749f88f8ee69d53bfb94acac->enter($__internal_50506fda1c4fbcd8e2ff913b034430b54fd68f6b749f88f8ee69d53bfb94acac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/request_content.html.twig", "@FOSUser/Resetting/request.html.twig", 4)->display($context);
        
        $__internal_50506fda1c4fbcd8e2ff913b034430b54fd68f6b749f88f8ee69d53bfb94acac->leave($__internal_50506fda1c4fbcd8e2ff913b034430b54fd68f6b749f88f8ee69d53bfb94acac_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Resetting/request.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Resetting\\request.html.twig");
    }
}
