<?php

/* cart/view.html.twig */
class __TwigTemplate_52b8ca42c273b951f411c478f969d87f0ea97b19287d8ffbfdc263994b59909f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0212c28e1a213fdb4bc21e2743c0baf74420b17be25dbf29e0f1ecd32a99897e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0212c28e1a213fdb4bc21e2743c0baf74420b17be25dbf29e0f1ecd32a99897e->enter($__internal_0212c28e1a213fdb4bc21e2743c0baf74420b17be25dbf29e0f1ecd32a99897e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "cart/view.html.twig"));

        $__internal_ff8c6f23cf7a1e193e77b66609408506a401825142c963c95bd705945a20d114 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ff8c6f23cf7a1e193e77b66609408506a401825142c963c95bd705945a20d114->enter($__internal_ff8c6f23cf7a1e193e77b66609408506a401825142c963c95bd705945a20d114_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "cart/view.html.twig"));

        // line 1
        $this->loadTemplate("header.html.twig", "cart/view.html.twig", 1)->display($context);
        // line 2
        echo "
<section>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-9 padding-right\">
                <div class=\"features_items\"><!--features_items-->
                    <h2 class=\"title text-center\">История </h2>
                    <div id=\"table_content\">
                        ";
        // line 10
        if ((isset($context["products"]) ? $context["products"] : $this->getContext($context, "products"))) {
            // line 11
            echo "                        ";
            $this->loadTemplate("cart/products_table.html.twig", "cart/view.html.twig", 11)->display(array_merge($context, array("products" =>             // line 12
(isset($context["products"]) ? $context["products"] : $this->getContext($context, "products")), "summ" =>             // line 13
(isset($context["summ"]) ? $context["summ"] : $this->getContext($context, "summ")), "count" =>             // line 14
(isset($context["count"]) ? $context["count"] : $this->getContext($context, "count")))));
            // line 16
            echo "                        ";
        } else {
            // line 17
            echo "                        ";
            $this->loadTemplate("cart/no_products.html.twig", "cart/view.html.twig", 17)->display($context);
            echo "    
                        ";
        }
        // line 19
        echo "                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
                    
";
        // line 26
        $this->loadTemplate("footer.html.twig", "cart/view.html.twig", 26)->display($context);
        
        $__internal_0212c28e1a213fdb4bc21e2743c0baf74420b17be25dbf29e0f1ecd32a99897e->leave($__internal_0212c28e1a213fdb4bc21e2743c0baf74420b17be25dbf29e0f1ecd32a99897e_prof);

        
        $__internal_ff8c6f23cf7a1e193e77b66609408506a401825142c963c95bd705945a20d114->leave($__internal_ff8c6f23cf7a1e193e77b66609408506a401825142c963c95bd705945a20d114_prof);

    }

    public function getTemplateName()
    {
        return "cart/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 26,  54 => 19,  48 => 17,  45 => 16,  43 => 14,  42 => 13,  41 => 12,  39 => 11,  37 => 10,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include ('header.html.twig') %}

<section>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-9 padding-right\">
                <div class=\"features_items\"><!--features_items-->
                    <h2 class=\"title text-center\">История </h2>
                    <div id=\"table_content\">
                        {% if products %}
                        {% include ('cart/products_table.html.twig') with {
                            'products' : products,
                            'summ' : summ,
                            'count' : count
                        }%}
                        {% else %}
                        {% include ('cart/no_products.html.twig') %}    
                        {% endif %}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
                    
{% include ('footer.html.twig') %}
", "cart/view.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\cart\\view.html.twig");
    }
}
