<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_8790a8e03219807df2a7bd9679294dc74883d0db32e93263b43b71e6a0cdd3ad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_81b9f7b263d9269544161c414f2eaf733f396b9e8ae92f93f1d7097f4676893b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_81b9f7b263d9269544161c414f2eaf733f396b9e8ae92f93f1d7097f4676893b->enter($__internal_81b9f7b263d9269544161c414f2eaf733f396b9e8ae92f93f1d7097f4676893b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        // line 1
        $this->loadTemplate("@FOSUser/header.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 1)->display($context);
        // line 2
        echo "
";
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 27
        echo "

";
        // line 29
        $this->displayBlock('footer', $context, $blocks);
        
        $__internal_81b9f7b263d9269544161c414f2eaf733f396b9e8ae92f93f1d7097f4676893b->leave($__internal_81b9f7b263d9269544161c414f2eaf733f396b9e8ae92f93f1d7097f4676893b_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_575bfbb6e850c3b8d12044f55fb19b0d0afdf8e78e43320fca2cf4fdbacec757 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_575bfbb6e850c3b8d12044f55fb19b0d0afdf8e78e43320fca2cf4fdbacec757->enter($__internal_575bfbb6e850c3b8d12044f55fb19b0d0afdf8e78e43320fca2cf4fdbacec757_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <section>
    <div class=\"container\">
        <div class=\"row\">
            
                <br>
                <br>
                <br>
                <h3><center>Поздравляю ";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "html", null, true);
        echo ", Ваш аккаунт активирован</center></h3>
                ";
        // line 14
        if ((isset($context["targetUrl"]) ? $context["targetUrl"] : null)) {
            // line 15
            echo "                <p><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["targetUrl"]) ? $context["targetUrl"] : null), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
                ";
        }
        // line 17
        echo "                <br>
                <br>
                <br>
                <br>

        </div>
    </div>
</section>
    
";
        
        $__internal_575bfbb6e850c3b8d12044f55fb19b0d0afdf8e78e43320fca2cf4fdbacec757->leave($__internal_575bfbb6e850c3b8d12044f55fb19b0d0afdf8e78e43320fca2cf4fdbacec757_prof);

    }

    // line 29
    public function block_footer($context, array $blocks = array())
    {
        $__internal_1a77e264efe9bec0bb8ea67d3c2d83d83b622ab8876ce36325987312a0c4abcb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1a77e264efe9bec0bb8ea67d3c2d83d83b622ab8876ce36325987312a0c4abcb->enter($__internal_1a77e264efe9bec0bb8ea67d3c2d83d83b622ab8876ce36325987312a0c4abcb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 30
        $this->loadTemplate("@FOSUser/footer.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 30)->display($context);
        
        $__internal_1a77e264efe9bec0bb8ea67d3c2d83d83b622ab8876ce36325987312a0c4abcb->leave($__internal_1a77e264efe9bec0bb8ea67d3c2d83d83b622ab8876ce36325987312a0c4abcb_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 30,  90 => 29,  74 => 17,  66 => 15,  64 => 14,  60 => 13,  51 => 6,  45 => 5,  38 => 29,  34 => 27,  32 => 5,  29 => 4,  26 => 2,  24 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Registration:confirmed.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Registration/confirmed.html.twig");
    }
}
