<?php

/* @WebProfiler/Profiler/open.html.twig */
class __TwigTemplate_cb916bdfc177eef411bafd1b1214598b449ed72350a2bcd90a4328ae2c1bb044 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "@WebProfiler/Profiler/open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ee9df56e698c4e598676adf122dc2c30e950a5d2eee1b2a9f657f537f7ac72ad = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ee9df56e698c4e598676adf122dc2c30e950a5d2eee1b2a9f657f537f7ac72ad->enter($__internal_ee9df56e698c4e598676adf122dc2c30e950a5d2eee1b2a9f657f537f7ac72ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ee9df56e698c4e598676adf122dc2c30e950a5d2eee1b2a9f657f537f7ac72ad->leave($__internal_ee9df56e698c4e598676adf122dc2c30e950a5d2eee1b2a9f657f537f7ac72ad_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_5c79e9353cddaa13914f4370daf4c7a5f546f925c57b5127f6ae13d134497e4e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5c79e9353cddaa13914f4370daf4c7a5f546f925c57b5127f6ae13d134497e4e->enter($__internal_5c79e9353cddaa13914f4370daf4c7a5f546f925c57b5127f6ae13d134497e4e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_5c79e9353cddaa13914f4370daf4c7a5f546f925c57b5127f6ae13d134497e4e->leave($__internal_5c79e9353cddaa13914f4370daf4c7a5f546f925c57b5127f6ae13d134497e4e_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_d01d7d9da581940464a623e9d701c94d9f5914c5018b45b340f4da62df1d2c8e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d01d7d9da581940464a623e9d701c94d9f5914c5018b45b340f4da62df1d2c8e->enter($__internal_d01d7d9da581940464a623e9d701c94d9f5914c5018b45b340f4da62df1d2c8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["file"]) ? $context["file"] : null), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, (isset($context["line"]) ? $context["line"] : null), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt((isset($context["filename"]) ? $context["filename"] : null), (isset($context["line"]) ? $context["line"] : null),  -1);
        echo "
</div>
";
        
        $__internal_d01d7d9da581940464a623e9d701c94d9f5914c5018b45b340f4da62df1d2c8e->leave($__internal_d01d7d9da581940464a623e9d701c94d9f5914c5018b45b340f4da62df1d2c8e_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 15,  69 => 12,  63 => 11,  60 => 10,  54 => 9,  44 => 5,  41 => 4,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@WebProfiler/Profiler/open.html.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\open.html.twig");
    }
}
