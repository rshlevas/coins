<?php

/* TwigBundle::layout.html.twig */
class __TwigTemplate_c1067e15e6c3679a97f1850de645f722affaa9753d3729ddb282d75406e2e836 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_51324656fdded7c6c57af553dbb55103208fed1ea294c538a5ca67ce625e7e85 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_51324656fdded7c6c57af553dbb55103208fed1ea294c538a5ca67ce625e7e85->enter($__internal_51324656fdded7c6c57af553dbb55103208fed1ea294c538a5ca67ce625e7e85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle::layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_51324656fdded7c6c57af553dbb55103208fed1ea294c538a5ca67ce625e7e85->leave($__internal_51324656fdded7c6c57af553dbb55103208fed1ea294c538a5ca67ce625e7e85_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_87efb35a0538b8dd35f3c811921ea307c11560601114f31c1641011ce54ea908 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_87efb35a0538b8dd35f3c811921ea307c11560601114f31c1641011ce54ea908->enter($__internal_87efb35a0538b8dd35f3c811921ea307c11560601114f31c1641011ce54ea908_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_87efb35a0538b8dd35f3c811921ea307c11560601114f31c1641011ce54ea908->leave($__internal_87efb35a0538b8dd35f3c811921ea307c11560601114f31c1641011ce54ea908_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_261ce763083b205761d7fc637dc0500a021846e7701241a2feffad009a70c06d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_261ce763083b205761d7fc637dc0500a021846e7701241a2feffad009a70c06d->enter($__internal_261ce763083b205761d7fc637dc0500a021846e7701241a2feffad009a70c06d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_261ce763083b205761d7fc637dc0500a021846e7701241a2feffad009a70c06d->leave($__internal_261ce763083b205761d7fc637dc0500a021846e7701241a2feffad009a70c06d_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_90a28cf261450b80dc5476b6f7590d22599bbf72a66f823689efb1cfa39f573a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_90a28cf261450b80dc5476b6f7590d22599bbf72a66f823689efb1cfa39f573a->enter($__internal_90a28cf261450b80dc5476b6f7590d22599bbf72a66f823689efb1cfa39f573a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_90a28cf261450b80dc5476b6f7590d22599bbf72a66f823689efb1cfa39f573a->leave($__internal_90a28cf261450b80dc5476b6f7590d22599bbf72a66f823689efb1cfa39f573a_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 33,  108 => 10,  97 => 7,  85 => 34,  83 => 33,  73 => 26,  63 => 19,  56 => 15,  50 => 11,  48 => 10,  44 => 9,  40 => 8,  36 => 7,  30 => 4,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "TwigBundle::layout.html.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/layout.html.twig");
    }
}
