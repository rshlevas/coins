<?php

/* @FOSUser/Resetting/reset.html.twig */
class __TwigTemplate_42ddd212a69255491ed48a9ac1e194de1ec6c878ab7aa066cfc07e7539006d0e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Resetting/reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9d0990006c43a709846b0e33d35d88310e41bdff6b7f0630c6733c12d95e6ac6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9d0990006c43a709846b0e33d35d88310e41bdff6b7f0630c6733c12d95e6ac6->enter($__internal_9d0990006c43a709846b0e33d35d88310e41bdff6b7f0630c6733c12d95e6ac6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9d0990006c43a709846b0e33d35d88310e41bdff6b7f0630c6733c12d95e6ac6->leave($__internal_9d0990006c43a709846b0e33d35d88310e41bdff6b7f0630c6733c12d95e6ac6_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_3a835d5df72ed4e1d8a537dc8f196c825af294dd9856d265e7ed675fdd900dd0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3a835d5df72ed4e1d8a537dc8f196c825af294dd9856d265e7ed675fdd900dd0->enter($__internal_3a835d5df72ed4e1d8a537dc8f196c825af294dd9856d265e7ed675fdd900dd0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/reset_content.html.twig", "@FOSUser/Resetting/reset.html.twig", 4)->display($context);
        
        $__internal_3a835d5df72ed4e1d8a537dc8f196c825af294dd9856d265e7ed675fdd900dd0->leave($__internal_3a835d5df72ed4e1d8a537dc8f196c825af294dd9856d265e7ed675fdd900dd0_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Resetting/reset.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Resetting\\reset.html.twig");
    }
}
