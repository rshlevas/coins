<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_957f5fbdc9fcd66214e8af0b25c686653cfffb9388c86e66fd99a941e978ae30 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_198b39eef59a43a3feb888f025d216f684c2c0b7d7150f56ec016a98f0e6d636 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_198b39eef59a43a3feb888f025d216f684c2c0b7d7150f56ec016a98f0e6d636->enter($__internal_198b39eef59a43a3feb888f025d216f684c2c0b7d7150f56ec016a98f0e6d636_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_198b39eef59a43a3feb888f025d216f684c2c0b7d7150f56ec016a98f0e6d636->leave($__internal_198b39eef59a43a3feb888f025d216f684c2c0b7d7150f56ec016a98f0e6d636_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/password_widget.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\password_widget.html.php");
    }
}
