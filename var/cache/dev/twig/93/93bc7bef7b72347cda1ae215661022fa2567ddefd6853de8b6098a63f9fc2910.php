<?php

/* @WebProfiler/Icon/cache.svg */
class __TwigTemplate_248dfd4bf9c9757b92bbbef639f675966cfdd124e29b658b4580b87788765ee7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cc552107f29fd5119667692b4e28e5d7918c74956fc8562f6fc84ca1014265db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cc552107f29fd5119667692b4e28e5d7918c74956fc8562f6fc84ca1014265db->enter($__internal_cc552107f29fd5119667692b4e28e5d7918c74956fc8562f6fc84ca1014265db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/cache.svg"));

        // line 1
        echo "<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 24 24\">
\t<path fill=\"#AAA\" d=\"M2.26 6.09l9.06-4.67a1.49 1.49 0 0 1 1.37 0l9.06 4.67a1.49 1.49 0 0 1 0 2.65l-9.06 4.67a1.49 1.49 0 0 1-1.37 0L2.26 8.74a1.49 1.49 0 0 1 0-2.65zM20.55 11L12 15.39 3.45 11a1.36 1.36 0 0 0-1.25 2.42l9.17 4.73a1.36 1.36 0 0 0 1.25 0l9.17-4.73A1.36 1.36 0 0 0 20.55 11zm0 4.47L12 19.86l-8.55-4.41a1.36 1.36 0 0 0-1.25 2.42l9.17 4.73a1.36 1.36 0 0 0 1.25 0l9.17-4.73a1.36 1.36 0 0 0-1.25-2.42z\"/>
</svg>
";
        
        $__internal_cc552107f29fd5119667692b4e28e5d7918c74956fc8562f6fc84ca1014265db->leave($__internal_cc552107f29fd5119667692b4e28e5d7918c74956fc8562f6fc84ca1014265db_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/cache.svg";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@WebProfiler/Icon/cache.svg", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Icon\\cache.svg");
    }
}
