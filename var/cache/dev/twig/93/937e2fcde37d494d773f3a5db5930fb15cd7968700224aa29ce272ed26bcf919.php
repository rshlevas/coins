<?php

/* @FOSUser/Profile/show.html.twig */
class __TwigTemplate_2e5c2767f20e9094ea1414c0812e8ef19393c0b9ef418c0b21dc130ad2cce185 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_16496ae48a5c5f9a77ebff77bbca57ac6c3f81aceaa644a3d34e6ffdeec67af4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_16496ae48a5c5f9a77ebff77bbca57ac6c3f81aceaa644a3d34e6ffdeec67af4->enter($__internal_16496ae48a5c5f9a77ebff77bbca57ac6c3f81aceaa644a3d34e6ffdeec67af4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show.html.twig"));

        // line 1
        $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Profile/show.html.twig", 1)->display($context);
        // line 2
        echo "
<p><a href=\"";
        // line 3
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_profile_edit");
        echo "\">Edit</a></p>
<br>
<p><a href=\"";
        // line 5
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_profile_history");
        echo "\">History</a></p>

";
        // line 7
        $this->displayBlock('fos_user_content', $context, $blocks);
        
        $__internal_16496ae48a5c5f9a77ebff77bbca57ac6c3f81aceaa644a3d34e6ffdeec67af4->leave($__internal_16496ae48a5c5f9a77ebff77bbca57ac6c3f81aceaa644a3d34e6ffdeec67af4_prof);

    }

    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_f57908820922be5b6a68ee8737e8f5ac5dfef173382316bc1bce2e8509da496b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f57908820922be5b6a68ee8737e8f5ac5dfef173382316bc1bce2e8509da496b->enter($__internal_f57908820922be5b6a68ee8737e8f5ac5dfef173382316bc1bce2e8509da496b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 8
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "@FOSUser/Profile/show.html.twig", 8)->display($context);
        
        $__internal_f57908820922be5b6a68ee8737e8f5ac5dfef173382316bc1bce2e8509da496b->leave($__internal_f57908820922be5b6a68ee8737e8f5ac5dfef173382316bc1bce2e8509da496b_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 8,  38 => 7,  33 => 5,  28 => 3,  25 => 2,  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Profile/show.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Profile\\show.html.twig");
    }
}
