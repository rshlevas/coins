<?php

/* main/index.html.twig */
class __TwigTemplate_6c273fcabfb36e6712321066e8c7505afb5af40c6d2dec4e1e39165ba0723079 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_456b9ff5cc62afe61d2628973ee87939a58acd678da472c4f03ddd2fb9a9532b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_456b9ff5cc62afe61d2628973ee87939a58acd678da472c4f03ddd2fb9a9532b->enter($__internal_456b9ff5cc62afe61d2628973ee87939a58acd678da472c4f03ddd2fb9a9532b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "main/index.html.twig"));

        // line 1
        echo twig_include($this->env, $context, "header.html.twig");
        echo "

<h1>Твой первый номер - ";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["number1"]) ? $context["number1"] : null), "html", null, true);
        echo "</h1>
</br>
<h2><a href='#'>Твой второй номер - ";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["number2"]) ? $context["number2"] : null), "html", null, true);
        echo "</a></h2>
</br>
<h3>Их сумма - ";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["summ"]) ? $context["summ"] : null), "html", null, true);
        echo "</h3>

";
        // line 9
        echo twig_include($this->env, $context, "footer.html.twig");
        
        $__internal_456b9ff5cc62afe61d2628973ee87939a58acd678da472c4f03ddd2fb9a9532b->leave($__internal_456b9ff5cc62afe61d2628973ee87939a58acd678da472c4f03ddd2fb9a9532b_prof);

    }

    public function getTemplateName()
    {
        return "main/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 9,  37 => 7,  32 => 5,  27 => 3,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "main/index.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\main\\index.html.twig");
    }
}
