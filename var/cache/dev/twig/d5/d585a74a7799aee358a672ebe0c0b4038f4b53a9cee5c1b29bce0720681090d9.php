<?php

/* WebProfilerBundle:Collector:exception.html.twig */
class __TwigTemplate_bede31aae401936286c421ad6bd743c48108f1eb20ae0083685d1d154b30ea1a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c02dd74742bf9b4f6fd7cb610d68266bc7a219e97787b307849118a4c477ecb5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c02dd74742bf9b4f6fd7cb610d68266bc7a219e97787b307849118a4c477ecb5->enter($__internal_c02dd74742bf9b4f6fd7cb610d68266bc7a219e97787b307849118a4c477ecb5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c02dd74742bf9b4f6fd7cb610d68266bc7a219e97787b307849118a4c477ecb5->leave($__internal_c02dd74742bf9b4f6fd7cb610d68266bc7a219e97787b307849118a4c477ecb5_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_69113d7281d019963c312f2bc90bc185e59a2728cd9ab0369f739f486805820f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_69113d7281d019963c312f2bc90bc185e59a2728cd9ab0369f739f486805820f->enter($__internal_69113d7281d019963c312f2bc90bc185e59a2728cd9ab0369f739f486805820f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : null), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) ? $context["token"] : null))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_69113d7281d019963c312f2bc90bc185e59a2728cd9ab0369f739f486805820f->leave($__internal_69113d7281d019963c312f2bc90bc185e59a2728cd9ab0369f739f486805820f_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_6c8611e9c030eb3585a11c0be1a4298f44facbbaaf150033650b4b3ccbcc91c5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6c8611e9c030eb3585a11c0be1a4298f44facbbaaf150033650b4b3ccbcc91c5->enter($__internal_6c8611e9c030eb3585a11c0be1a4298f44facbbaaf150033650b4b3ccbcc91c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : null), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : null), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_6c8611e9c030eb3585a11c0be1a4298f44facbbaaf150033650b4b3ccbcc91c5->leave($__internal_6c8611e9c030eb3585a11c0be1a4298f44facbbaaf150033650b4b3ccbcc91c5_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_7abc13d9357680668f69c1e792cd5f6604618e07b8f2c11057425a73938a571c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7abc13d9357680668f69c1e792cd5f6604618e07b8f2c11057425a73938a571c->enter($__internal_7abc13d9357680668f69c1e792cd5f6604618e07b8f2c11057425a73938a571c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute((isset($context["collector"]) ? $context["collector"] : null), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) ? $context["token"] : null))));
            echo "
        </div>
    ";
        }
        
        $__internal_7abc13d9357680668f69c1e792cd5f6604618e07b8f2c11057425a73938a571c->leave($__internal_7abc13d9357680668f69c1e792cd5f6604618e07b8f2c11057425a73938a571c_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 33,  114 => 32,  108 => 28,  106 => 27,  102 => 25,  96 => 24,  88 => 21,  82 => 17,  80 => 16,  75 => 14,  70 => 13,  64 => 12,  54 => 9,  48 => 6,  45 => 5,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "WebProfilerBundle:Collector:exception.html.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
