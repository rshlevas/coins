<?php

/* :cart:products_table.html.twig */
class __TwigTemplate_3eb138fd1ef746adc16dcdaedc0ff0d9156c9be739990d9a091296916ec71693 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_78a2aabc35d27341ca501498ca140bf313d7056c81dd1573af5c9863efb60190 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_78a2aabc35d27341ca501498ca140bf313d7056c81dd1573af5c9863efb60190->enter($__internal_78a2aabc35d27341ca501498ca140bf313d7056c81dd1573af5c9863efb60190_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":cart:products_table.html.twig"));

        // line 1
        echo "                        <p>Вы выбрали такие товары:</p>
                        <table class=\"table-bordered table-striped table\">
                            <tr>
                                <th>№</th>
                                <th>Название</th>
                                <th>Стомость, грн</th>
                                <th>Количество, шт</th>
                                <th>Удалить</th>
                            </tr>
                            ";
        // line 10
        $context["i"] = 1;
        // line 11
        echo "                            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 12
            echo "                                <tr>
                                    <td>";
            // line 13
            echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : null), "html", null, true);
            echo "</td>
                                    <td>
                                        <a href=\"/product/";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "\">
                                           ";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
            echo "
                                        </a>
                                    </td>
                                    <td>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "price", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["count"]) ? $context["count"] : null), $this->getAttribute($context["product"], "id", array()), array(), "array"), "html", null, true);
            echo "</td> 
                                    <td>
                                        <a href=\"/cart/delete/";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "\">
                                            <i class=\"fa fa-times\" data-id=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "\"></i>
                                        </a>
                                    </td>
                                </tr>
                                ";
            // line 27
            $context["i"] = ((isset($context["i"]) ? $context["i"] : null) + 1);
            // line 28
            echo "                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "                                <tr>
                                    <td colspan=\"4\">Общая стоимость, грн:</td>
                                    <td>";
        // line 31
        echo twig_escape_filter($this->env, (isset($context["summ"]) ? $context["summ"] : null), "html", null, true);
        echo "</td>
                                </tr>
                            
                        </table>
                        
                        <a class=\"btn btn-default checkout\" href=\"";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("cart_checkout");
        echo "\"><i class=\"fa fa-shopping-cart\"></i> Оформить заказ</a>
                        <a class=\"btn btn-default checkout\" href=\"/\"><i class=\"fa fa-shopping-cart\"></i> Вернуться к покупкам</a>

<script>
    \$(document).ready(function(){
        \$(\".fa.fa-times\").click(function () {
            var id = \$(this).attr(\"data-id\");
            \$.ajax({
                type: 'post',
                dataType: 'json',
                url: \"/cart/delete/\"+id ,
                success: function (data) {
                    \$(\"#table_content\").html(data.html);

                }
            });
            \$.ajax({
                type: 'post',
                url: '";
        // line 54
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("cart_count");
        echo "',
                success: function (data) {
                    if (data !== 0) {
                        \$(\"#cart-count\").html(data);
                    }
                    else {
                        \$(\"#cart-count\").html(\"\");
                    }    
                }
            });
            return false;
        });
    }); 
    
</script>";
        
        $__internal_78a2aabc35d27341ca501498ca140bf313d7056c81dd1573af5c9863efb60190->leave($__internal_78a2aabc35d27341ca501498ca140bf313d7056c81dd1573af5c9863efb60190_prof);

    }

    public function getTemplateName()
    {
        return ":cart:products_table.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 54,  98 => 36,  90 => 31,  86 => 29,  80 => 28,  78 => 27,  71 => 23,  67 => 22,  62 => 20,  58 => 19,  52 => 16,  48 => 15,  43 => 13,  40 => 12,  35 => 11,  33 => 10,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":cart:products_table.html.twig", "D:\\wamp64\\www\\coins\\app/Resources\\views/cart/products_table.html.twig");
    }
}
