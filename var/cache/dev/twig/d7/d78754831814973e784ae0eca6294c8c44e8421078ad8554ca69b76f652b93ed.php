<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_5bf689ec2aabda420f9e09e9a3799980ae970600f6d78ab6115aff2c70b7b287 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3cd1cb4d3c78a044593c74f53fde6b24ef56d9686a43d915cbb85f075bdb085f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3cd1cb4d3c78a044593c74f53fde6b24ef56d9686a43d915cbb85f075bdb085f->enter($__internal_3cd1cb4d3c78a044593c74f53fde6b24ef56d9686a43d915cbb85f075bdb085f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_3cd1cb4d3c78a044593c74f53fde6b24ef56d9686a43d915cbb85f075bdb085f->leave($__internal_3cd1cb4d3c78a044593c74f53fde6b24ef56d9686a43d915cbb85f075bdb085f_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_0329cf33672c4d2d1679363da1365f1de081dd9a6d0648c372496327d656568a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0329cf33672c4d2d1679363da1365f1de081dd9a6d0648c372496327d656568a->enter($__internal_0329cf33672c4d2d1679363da1365f1de081dd9a6d0648c372496327d656568a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array())), "FOSUserBundle");
        
        $__internal_0329cf33672c4d2d1679363da1365f1de081dd9a6d0648c372496327d656568a->leave($__internal_0329cf33672c4d2d1679363da1365f1de081dd9a6d0648c372496327d656568a_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_2df8ffd9b2b9245e378cb3a41edec22b9564736bd7218ced29ce7bd42d88fd69 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2df8ffd9b2b9245e378cb3a41edec22b9564736bd7218ced29ce7bd42d88fd69->enter($__internal_2df8ffd9b2b9245e378cb3a41edec22b9564736bd7218ced29ce7bd42d88fd69_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : null)), "FOSUserBundle");
        echo "
";
        
        $__internal_2df8ffd9b2b9245e378cb3a41edec22b9564736bd7218ced29ce7bd42d88fd69->leave($__internal_2df8ffd9b2b9245e378cb3a41edec22b9564736bd7218ced29ce7bd42d88fd69_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_a3f968b25a1b4b9f7b4530d6fd973bf69e1550934a244ca7d279d8424bdf520d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a3f968b25a1b4b9f7b4530d6fd973bf69e1550934a244ca7d279d8424bdf520d->enter($__internal_a3f968b25a1b4b9f7b4530d6fd973bf69e1550934a244ca7d279d8424bdf520d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_a3f968b25a1b4b9f7b4530d6fd973bf69e1550934a244ca7d279d8424bdf520d->leave($__internal_a3f968b25a1b4b9f7b4530d6fd973bf69e1550934a244ca7d279d8424bdf520d_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  67 => 13,  58 => 10,  52 => 8,  45 => 4,  39 => 2,  32 => 13,  30 => 8,  27 => 7,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Resetting:email.txt.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Resetting/email.txt.twig");
    }
}
