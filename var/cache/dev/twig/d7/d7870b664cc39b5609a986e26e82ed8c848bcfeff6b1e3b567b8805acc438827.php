<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_09236d53f938956137e9b52da272f8429e99bba73bce750f01f7f8ba3fae29ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_631ef14119b5259b48807bf9adebfc5c68411fe0c5db092f35f1bc4e2edf6b9f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_631ef14119b5259b48807bf9adebfc5c68411fe0c5db092f35f1bc4e2edf6b9f->enter($__internal_631ef14119b5259b48807bf9adebfc5c68411fe0c5db092f35f1bc4e2edf6b9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_631ef14119b5259b48807bf9adebfc5c68411fe0c5db092f35f1bc4e2edf6b9f->leave($__internal_631ef14119b5259b48807bf9adebfc5c68411fe0c5db092f35f1bc4e2edf6b9f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/form_rest.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_rest.html.php");
    }
}
