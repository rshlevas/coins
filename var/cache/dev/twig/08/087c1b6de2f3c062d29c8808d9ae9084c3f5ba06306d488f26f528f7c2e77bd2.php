<?php

/* product/view.html.twig */
class __TwigTemplate_88f6bbd37fc2a1fea88528655f2c48122ab4da8fa98f59b5392d855a0e24ab26 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_225cb0674e71d78c245068854d035f9ce6e94062ee83f98c649931d8d7f25313 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_225cb0674e71d78c245068854d035f9ce6e94062ee83f98c649931d8d7f25313->enter($__internal_225cb0674e71d78c245068854d035f9ce6e94062ee83f98c649931d8d7f25313_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "product/view.html.twig"));

        // line 1
        echo twig_include($this->env, $context, "header.html.twig");
        echo "

<section>
<div class=\"container\">
<div class=\"row\">
    <div class=\"breadcrumbs\">
        <ol class=\"breadcrumb\">
            <li>Каталог</li>
            <li><a href=\"/";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["type"]) ? $context["type"] : null), "route", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["type"]) ? $context["type"] : null), "name", array()), "html", null, true);
        echo "</a></li>
            <li><a href=\"/";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["type"]) ? $context["type"] : null), "route", array()), "html", null, true);
        echo "/";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["region"]) ? $context["region"] : null), "route", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["region"]) ? $context["region"] : null), "name", array()), "html", null, true);
        echo "</a></li>
            <li><a href=\"/country/";
        // line 11
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "country", array()), "id", array()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "country", array()), "name", array()), "html", null, true);
        echo "</a></li>
            <li class=\"active\">";
        // line 12
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "name", array()), "html", null, true);
        echo "</li>
        </ol>
    </div>
    <div class=\"col-sm-3\">
        <div class=\"left-sidebar\">
            <h2>Оплата</h2>
            <div class=\"panel-group category-products\">
                    <div class=\"panel panel-default\">
                        <div class=\"panel-heading\">
                            <h4 class=\"panel-title\">
                                
                            </h4>
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class=\"col-sm-9 padding-right\">
        <div class=\"product-details\"><!--product-details-->
            <div class=\"row\">
                <div class=\"col-sm-5\">
                    <div class=\"view-product\">
                        <img src=\"/images/product/";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "id", array()), "html", null, true);
        echo ".jpg\" alt=\"\" />
                    </div>
                </div>
                <div class=\"col-sm-7\">
                    <div class=\"product-information\"><!--/product-information-->
                        <h2> ";
        // line 40
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "name", array()), "html", null, true);
        echo "</h2>
                        <span>
                            <span> ";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "price", array()), "html", null, true);
        echo " </span>
                            <a href=\"#\" data-id=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "id", array()), "html", null, true);
        echo "\"
                               class=\"btn btn-default add-to-cart\">
                                <i class=\"fa fa-shopping-cart\"></i>В корзину
                            </a>
                        </span>
                        <p><b>Год:</b> ";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "year", array()), "html", null, true);
        echo "</p> 
                        <p><b>Наличие:</b> 
                            ";
        // line 50
        if (($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "count", array()) == 0)) {
            // line 51
            echo "                                Нет в наличии
                            ";
        } else {
            // line 53
            echo "                                Доступно ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "count", array()), "html", null, true);
            echo "
                            ";
        }
        // line 55
        echo "                        </p>
                        <p><b>Cтрана:</b> ";
        // line 56
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["product"]) ? $context["product"] : null), "country", array()), "name", array()), "html", null, true);
        echo "</p>
                    </div><!--/product-information-->
                </div>
            </div>
            <div class=\"row\">                                
                <div class=\"col-sm-12\">
                    <br/>
                    <h5>Описание товара</h5>
                    ";
        // line 64
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "description", array()), "html", null, true);
        echo "
                </div>
            </div>
        </div><!--/product-details-->
        <div class=\"recommended_items\"><!--recommended_items-->
            <h2 class=\"title text-center\">Похожие товары</h2>

            <div class=\"cycle-slideshow\" 
                 data-cycle-fx=carousel
                 data-cycle-timeout=5000
                 data-cycle-carousel-visible=3
                 data-cycle-carousel-fluid=true
                 data-cycle-slides=\"div.item\"
                 data-cycle-prev=\"#prev\"
                 data-cycle-next=\"#next\"
                 >                        
                ";
        // line 80
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["other_products"]) ? $context["other_products"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["product_item"]) {
            // line 81
            echo "                    <div class=\"item\">
                        <div class=\"product-image-wrapper\">
                            <div class=\"single-products\">
                                <div class=\"productinfo text-center\">
                                    <img 
                                        src=\"/images/product/";
            // line 86
            echo twig_escape_filter($this->env, $this->getAttribute($context["product_item"], "id", array()), "html", null, true);
            echo ".jpg\" 
                                        alt=\"\" 
                                        height=\"100px\"
                                        width=\"50px\"
                                    />
                                    <h2>";
            // line 91
            echo twig_escape_filter($this->env, $this->getAttribute($context["product_item"], "price", array()), "html", null, true);
            echo "</h2>
                                    <a href=\"/product/";
            // line 92
            echo twig_escape_filter($this->env, $this->getAttribute($context["product_item"], "id", array()), "html", null, true);
            echo "\">
                                        ";
            // line 93
            echo twig_escape_filter($this->env, $this->getAttribute($context["product_item"], "name", array()), "html", null, true);
            echo "
                                    </a>
                                    <br/><br/>
                                    <a href=\"#\" class=\"btn btn-default add-to-cart\" data-id=\"";
            // line 96
            echo twig_escape_filter($this->env, $this->getAttribute($context["product_item"], "id", array()), "html", null, true);
            echo "\"><i class=\"fa fa-shopping-cart\"></i>В корзину</a>
                                </div>
                            </div>
                        </div>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product_item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 102
        echo "            </div>

            <a class=\"left recommended-item-control\" id=\"prev\" href=\"#recommended-item-carousel\" data-slide=\"prev\">
                <i class=\"fa fa-angle-left\"></i>
            </a>
            <a class=\"right recommended-item-control\" id=\"next\"  href=\"#recommended-item-carousel\" data-slide=\"next\">
                <i class=\"fa fa-angle-right\"></i>
            </a>

        </div>
    </div>
</div>
</div>
</section>


                    
";
        // line 119
        echo twig_include($this->env, $context, "footer.html.twig");
        
        $__internal_225cb0674e71d78c245068854d035f9ce6e94062ee83f98c649931d8d7f25313->leave($__internal_225cb0674e71d78c245068854d035f9ce6e94062ee83f98c649931d8d7f25313_prof);

    }

    public function getTemplateName()
    {
        return "product/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  218 => 119,  199 => 102,  187 => 96,  181 => 93,  177 => 92,  173 => 91,  165 => 86,  158 => 81,  154 => 80,  135 => 64,  124 => 56,  121 => 55,  115 => 53,  111 => 51,  109 => 50,  104 => 48,  96 => 43,  92 => 42,  87 => 40,  79 => 35,  53 => 12,  47 => 11,  39 => 10,  33 => 9,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "product/view.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\product\\view.html.twig");
    }
}
