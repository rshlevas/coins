<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_2d20b207b6fd8e07822835766076e645a3ddb8de5cca04ad2d5d8d18c5b3c032 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c50c89560db1cf6cdab2fe230099a6993be9d6a365fd379a1bdad96624719375 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c50c89560db1cf6cdab2fe230099a6993be9d6a365fd379a1bdad96624719375->enter($__internal_c50c89560db1cf6cdab2fe230099a6993be9d6a365fd379a1bdad96624719375_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c50c89560db1cf6cdab2fe230099a6993be9d6a365fd379a1bdad96624719375->leave($__internal_c50c89560db1cf6cdab2fe230099a6993be9d6a365fd379a1bdad96624719375_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_732ffd7c6b77941aeaacc1ff3dc18bcbc9b8665b5f40b994275b6408302e9163 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_732ffd7c6b77941aeaacc1ff3dc18bcbc9b8665b5f40b994275b6408302e9163->enter($__internal_732ffd7c6b77941aeaacc1ff3dc18bcbc9b8665b5f40b994275b6408302e9163_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_732ffd7c6b77941aeaacc1ff3dc18bcbc9b8665b5f40b994275b6408302e9163->leave($__internal_732ffd7c6b77941aeaacc1ff3dc18bcbc9b8665b5f40b994275b6408302e9163_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_c29a7736af0f05bc213146488ce8d3c7beeedf54155316dd3c705227a8fee66e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c29a7736af0f05bc213146488ce8d3c7beeedf54155316dd3c705227a8fee66e->enter($__internal_c29a7736af0f05bc213146488ce8d3c7beeedf54155316dd3c705227a8fee66e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : null), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_c29a7736af0f05bc213146488ce8d3c7beeedf54155316dd3c705227a8fee66e->leave($__internal_c29a7736af0f05bc213146488ce8d3c7beeedf54155316dd3c705227a8fee66e_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
