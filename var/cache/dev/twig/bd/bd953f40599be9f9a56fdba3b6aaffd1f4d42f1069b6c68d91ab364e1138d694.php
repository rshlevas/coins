<?php

/* FOSUserBundle:Security:login.html.twig */
class __TwigTemplate_c6e05061116964ab1fe291b481cce17ef61b6da0f4318a89a96909622e5963f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f61913bfe3a446afb266eb30c0036d5ba9b88d0636a552efa56cf9c699402f74 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f61913bfe3a446afb266eb30c0036d5ba9b88d0636a552efa56cf9c699402f74->enter($__internal_f61913bfe3a446afb266eb30c0036d5ba9b88d0636a552efa56cf9c699402f74_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Security:login.html.twig"));

        // line 1
        $this->loadTemplate("@FOSUser/header.html.twig", "FOSUserBundle:Security:login.html.twig", 1)->display($context);
        // line 2
        echo "<section>
    <div class=\"container\">
        <div class=\"row\">
            
            <div class=\"col-sm-4 col-sm-offset-4 padding-right\">
                ";
        // line 7
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 10
        echo "            
                <br>
                <p>Забыли пароль? Нажмите 
                   <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_resetting_request");
        echo "\"> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("ТУТ", array(), "FOSUserBundle"), "html", null, true);
        echo "
                   </a></p>
                <br>
            </div>
        </div>
    </div>
</section>

";
        // line 21
        $this->displayBlock('footer', $context, $blocks);
        // line 24
        echo "
";
        
        $__internal_f61913bfe3a446afb266eb30c0036d5ba9b88d0636a552efa56cf9c699402f74->leave($__internal_f61913bfe3a446afb266eb30c0036d5ba9b88d0636a552efa56cf9c699402f74_prof);

    }

    // line 7
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_743119f7610a1a9cfedc9cd429c4fabd1929f59c19eed7e23d3b4096de087c1b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_743119f7610a1a9cfedc9cd429c4fabd1929f59c19eed7e23d3b4096de087c1b->enter($__internal_743119f7610a1a9cfedc9cd429c4fabd1929f59c19eed7e23d3b4096de087c1b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 8
        echo "                ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
                ";
        
        $__internal_743119f7610a1a9cfedc9cd429c4fabd1929f59c19eed7e23d3b4096de087c1b->leave($__internal_743119f7610a1a9cfedc9cd429c4fabd1929f59c19eed7e23d3b4096de087c1b_prof);

    }

    // line 21
    public function block_footer($context, array $blocks = array())
    {
        $__internal_b79bae890ed392c40cc0cb41bbf805cd2bb8412364bb93984cefe1377c380bfd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b79bae890ed392c40cc0cb41bbf805cd2bb8412364bb93984cefe1377c380bfd->enter($__internal_b79bae890ed392c40cc0cb41bbf805cd2bb8412364bb93984cefe1377c380bfd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 22
        $this->loadTemplate("@FOSUser/footer.html.twig", "FOSUserBundle:Security:login.html.twig", 22)->display($context);
        
        $__internal_b79bae890ed392c40cc0cb41bbf805cd2bb8412364bb93984cefe1377c380bfd->leave($__internal_b79bae890ed392c40cc0cb41bbf805cd2bb8412364bb93984cefe1377c380bfd_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Security:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 22,  79 => 21,  69 => 8,  63 => 7,  55 => 24,  53 => 21,  40 => 13,  35 => 10,  33 => 7,  26 => 2,  24 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Security:login.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Security/login.html.twig");
    }
}
