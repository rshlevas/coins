<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_ce71be941e237a269cc0ea77c0623a09f0e745307e74f38bc17ad2ab87ed996a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_72a739b91ef1806d4c64908af62d8db74651580baa49048f2e375ca56eb9a089 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_72a739b91ef1806d4c64908af62d8db74651580baa49048f2e375ca56eb9a089->enter($__internal_72a739b91ef1806d4c64908af62d8db74651580baa49048f2e375ca56eb9a089_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_72a739b91ef1806d4c64908af62d8db74651580baa49048f2e375ca56eb9a089->leave($__internal_72a739b91ef1806d4c64908af62d8db74651580baa49048f2e375ca56eb9a089_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/textarea_widget.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\textarea_widget.html.php");
    }
}
