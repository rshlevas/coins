<?php

/* FOSUserBundle::footer.html.twig */
class __TwigTemplate_58d25a7e8f338c3be3f3cd63a157e496f5f4d29070cc13a6b15e9ca2bc29e805 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5d073d1d83b527e4a847d8c8a7bc865b0e38beb691356477ff5179bc35672512 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d073d1d83b527e4a847d8c8a7bc865b0e38beb691356477ff5179bc35672512->enter($__internal_5d073d1d83b527e4a847d8c8a7bc865b0e38beb691356477ff5179bc35672512_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle::footer.html.twig"));

        // line 2
        echo "

<footer id=\"footer\" class=\"page-footer\"><!--Footer-->
    <div class=\"footer-bottom\">
        <div class=\"container\">
            <div class=\"row\">
                <p class=\"pull-left\">Copyright © 2016</p>
                <p class=\"pull-right\"></p>
            </div>
        </div>
    </div>
</footer><!--/Footer-->





</body>
</html>";
        
        $__internal_5d073d1d83b527e4a847d8c8a7bc865b0e38beb691356477ff5179bc35672512->leave($__internal_5d073d1d83b527e4a847d8c8a7bc865b0e38beb691356477ff5179bc35672512_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle::footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle::footer.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle/Resources/views/footer.html.twig");
    }
}
