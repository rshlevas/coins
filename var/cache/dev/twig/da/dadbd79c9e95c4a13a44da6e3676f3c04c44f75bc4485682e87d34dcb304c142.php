<?php

/* FOSUserBundle:Resetting:request.html.twig */
class __TwigTemplate_026e64f5315a93da28e9ff5f4229b547ebd63e80920f2562f40362abe0ec5001 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:request.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a72495d6cdd4c7c4ea3a42bfee02f7f058f222a38e9bcecfdcc3ebe238173190 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a72495d6cdd4c7c4ea3a42bfee02f7f058f222a38e9bcecfdcc3ebe238173190->enter($__internal_a72495d6cdd4c7c4ea3a42bfee02f7f058f222a38e9bcecfdcc3ebe238173190_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:request.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a72495d6cdd4c7c4ea3a42bfee02f7f058f222a38e9bcecfdcc3ebe238173190->leave($__internal_a72495d6cdd4c7c4ea3a42bfee02f7f058f222a38e9bcecfdcc3ebe238173190_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_c841581909ce13a699e4c341fa11b399df8752b90fad56d04c8467c423f5a49b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c841581909ce13a699e4c341fa11b399df8752b90fad56d04c8467c423f5a49b->enter($__internal_c841581909ce13a699e4c341fa11b399df8752b90fad56d04c8467c423f5a49b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/request_content.html.twig", "FOSUserBundle:Resetting:request.html.twig", 4)->display($context);
        
        $__internal_c841581909ce13a699e4c341fa11b399df8752b90fad56d04c8467c423f5a49b->leave($__internal_c841581909ce13a699e4c341fa11b399df8752b90fad56d04c8467c423f5a49b_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Resetting:request.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Resetting/request.html.twig");
    }
}
