<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_1967fb402600f2e152b572a88d751533931da45d8fa82dd957cfdbf53e913625 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3157ebe8e104634b3f3ba85ab8c9071cc4ae50d303204f0f630ee33b46292775 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3157ebe8e104634b3f3ba85ab8c9071cc4ae50d303204f0f630ee33b46292775->enter($__internal_3157ebe8e104634b3f3ba85ab8c9071cc4ae50d303204f0f630ee33b46292775_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_3157ebe8e104634b3f3ba85ab8c9071cc4ae50d303204f0f630ee33b46292775->leave($__internal_3157ebe8e104634b3f3ba85ab8c9071cc4ae50d303204f0f630ee33b46292775_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/form_enctype.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_enctype.html.php");
    }
}
