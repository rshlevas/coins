<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_3f6fdf563d18cb3711f51d3eb3b30702b9f7704a48f0bb404a91f758801eb036 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_502e9b586ffe1981b7dccf51eb3cb3630e41161c710feae0d20946e66e32e7d8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_502e9b586ffe1981b7dccf51eb3cb3630e41161c710feae0d20946e66e32e7d8->enter($__internal_502e9b586ffe1981b7dccf51eb3cb3630e41161c710feae0d20946e66e32e7d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : null), "message" => (isset($context["status_text"]) ? $context["status_text"] : null), "exception" => $this->getAttribute((isset($context["exception"]) ? $context["exception"] : null), "toarray", array()))));
        echo "
";
        
        $__internal_502e9b586ffe1981b7dccf51eb3cb3630e41161c710feae0d20946e66e32e7d8->leave($__internal_502e9b586ffe1981b7dccf51eb3cb3630e41161c710feae0d20946e66e32e7d8_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "TwigBundle:Exception:exception.json.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/exception.json.twig");
    }
}
