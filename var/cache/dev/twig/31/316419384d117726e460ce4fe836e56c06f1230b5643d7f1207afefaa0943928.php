<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_e202dc7f60baf1f63f771b87b998d2b108a63c8a9a60835a7dfba0ac10c8b2ca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5ba17572266339ad15d941e85032fc091beff5c4483528ce98beafcdecd8ed6b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5ba17572266339ad15d941e85032fc091beff5c4483528ce98beafcdecd8ed6b->enter($__internal_5ba17572266339ad15d941e85032fc091beff5c4483528ce98beafcdecd8ed6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5ba17572266339ad15d941e85032fc091beff5c4483528ce98beafcdecd8ed6b->leave($__internal_5ba17572266339ad15d941e85032fc091beff5c4483528ce98beafcdecd8ed6b_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_b694bbcfdf0fc7ddbab973566204068f816bfa33f90c50ec256d57f0e4a0ddbd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b694bbcfdf0fc7ddbab973566204068f816bfa33f90c50ec256d57f0e4a0ddbd->enter($__internal_b694bbcfdf0fc7ddbab973566204068f816bfa33f90c50ec256d57f0e4a0ddbd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_b694bbcfdf0fc7ddbab973566204068f816bfa33f90c50ec256d57f0e4a0ddbd->leave($__internal_b694bbcfdf0fc7ddbab973566204068f816bfa33f90c50ec256d57f0e4a0ddbd_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_f21ef9f4087abf101f38ea48d7bbff8fd72791ecdb0b88b560d663ef714839bb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f21ef9f4087abf101f38ea48d7bbff8fd72791ecdb0b88b560d663ef714839bb->enter($__internal_f21ef9f4087abf101f38ea48d7bbff8fd72791ecdb0b88b560d663ef714839bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_f21ef9f4087abf101f38ea48d7bbff8fd72791ecdb0b88b560d663ef714839bb->leave($__internal_f21ef9f4087abf101f38ea48d7bbff8fd72791ecdb0b88b560d663ef714839bb_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_96c175d4855b49684cc5f07c3c769fa294e561c664f40d5a7d2e95e9abfbb181 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_96c175d4855b49684cc5f07c3c769fa294e561c664f40d5a7d2e95e9abfbb181->enter($__internal_96c175d4855b49684cc5f07c3c769fa294e561c664f40d5a7d2e95e9abfbb181_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : null))));
        echo "
";
        
        $__internal_96c175d4855b49684cc5f07c3c769fa294e561c664f40d5a7d2e95e9abfbb181->leave($__internal_96c175d4855b49684cc5f07c3c769fa294e561c664f40d5a7d2e95e9abfbb181_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@WebProfiler/Collector/router.html.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
