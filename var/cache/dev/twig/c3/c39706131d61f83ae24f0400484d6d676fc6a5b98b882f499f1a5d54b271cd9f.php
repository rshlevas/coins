<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_98cd0ea4d6d27b39316cc85b2dc9c52335080d8dca232c82eda23224e47e1968 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_820dae04b8b22f2fd27d2cbf8eac380e3c065472eb0607af19453f44dfe18687 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_820dae04b8b22f2fd27d2cbf8eac380e3c065472eb0607af19453f44dfe18687->enter($__internal_820dae04b8b22f2fd27d2cbf8eac380e3c065472eb0607af19453f44dfe18687_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_820dae04b8b22f2fd27d2cbf8eac380e3c065472eb0607af19453f44dfe18687->leave($__internal_820dae04b8b22f2fd27d2cbf8eac380e3c065472eb0607af19453f44dfe18687_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_392ef0380ac52aa4e1d9a72d9686524c85d0273171c3737025e4c665fb685b63 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_392ef0380ac52aa4e1d9a72d9686524c85d0273171c3737025e4c665fb685b63->enter($__internal_392ef0380ac52aa4e1d9a72d9686524c85d0273171c3737025e4c665fb685b63_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : null), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) ? $context["token"] : null))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_392ef0380ac52aa4e1d9a72d9686524c85d0273171c3737025e4c665fb685b63->leave($__internal_392ef0380ac52aa4e1d9a72d9686524c85d0273171c3737025e4c665fb685b63_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_754c344d3d8ea11370d1a4d1e97db318674de37497671257231ed12600bed02f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_754c344d3d8ea11370d1a4d1e97db318674de37497671257231ed12600bed02f->enter($__internal_754c344d3d8ea11370d1a4d1e97db318674de37497671257231ed12600bed02f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute((isset($context["collector"]) ? $context["collector"] : null), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute((isset($context["collector"]) ? $context["collector"] : null), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_754c344d3d8ea11370d1a4d1e97db318674de37497671257231ed12600bed02f->leave($__internal_754c344d3d8ea11370d1a4d1e97db318674de37497671257231ed12600bed02f_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_11b57a3628e79eedf9701244b1850b79d137428b0cd69d13cab6c87cf86592fc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_11b57a3628e79eedf9701244b1850b79d137428b0cd69d13cab6c87cf86592fc->enter($__internal_11b57a3628e79eedf9701244b1850b79d137428b0cd69d13cab6c87cf86592fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute((isset($context["collector"]) ? $context["collector"] : null), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) ? $context["token"] : null))));
            echo "
        </div>
    ";
        }
        
        $__internal_11b57a3628e79eedf9701244b1850b79d137428b0cd69d13cab6c87cf86592fc->leave($__internal_11b57a3628e79eedf9701244b1850b79d137428b0cd69d13cab6c87cf86592fc_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  117 => 33,  114 => 32,  108 => 28,  106 => 27,  102 => 25,  96 => 24,  88 => 21,  82 => 17,  80 => 16,  75 => 14,  70 => 13,  64 => 12,  54 => 9,  48 => 6,  45 => 5,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@WebProfiler/Collector/exception.html.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\exception.html.twig");
    }
}
