<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_d5284b4ed5db4173130b79a48326bb6c16c9519298de3a7f80a5cadf527929d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a741a0ef2d0e68bf0d6cd42714881037573bc0f3b6bef82d3a25928b9e658f60 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a741a0ef2d0e68bf0d6cd42714881037573bc0f3b6bef82d3a25928b9e658f60->enter($__internal_a741a0ef2d0e68bf0d6cd42714881037573bc0f3b6bef82d3a25928b9e658f60_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_a741a0ef2d0e68bf0d6cd42714881037573bc0f3b6bef82d3a25928b9e658f60->leave($__internal_a741a0ef2d0e68bf0d6cd42714881037573bc0f3b6bef82d3a25928b9e658f60_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/radio_widget.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\radio_widget.html.php");
    }
}
