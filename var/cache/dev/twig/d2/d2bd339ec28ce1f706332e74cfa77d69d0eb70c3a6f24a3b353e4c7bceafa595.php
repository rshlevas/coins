<?php

/* FOSUserBundle:Group:show.html.twig */
class __TwigTemplate_16b00bca18350d872ff153d3c543c4cf665d0bc2ce9ea8eec70dc369a2ae7ba1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2a7119a0451ebc048508dd3caf0d6f05ddd542cc71f319eb60b60f46b19fac8f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a7119a0451ebc048508dd3caf0d6f05ddd542cc71f319eb60b60f46b19fac8f->enter($__internal_2a7119a0451ebc048508dd3caf0d6f05ddd542cc71f319eb60b60f46b19fac8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2a7119a0451ebc048508dd3caf0d6f05ddd542cc71f319eb60b60f46b19fac8f->leave($__internal_2a7119a0451ebc048508dd3caf0d6f05ddd542cc71f319eb60b60f46b19fac8f_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_7166555e1b30693bc333cdfcfc8f4c790b8a1f269886ac36e4680f7935b930b2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7166555e1b30693bc333cdfcfc8f4c790b8a1f269886ac36e4680f7935b930b2->enter($__internal_7166555e1b30693bc333cdfcfc8f4c790b8a1f269886ac36e4680f7935b930b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/show_content.html.twig", "FOSUserBundle:Group:show.html.twig", 4)->display($context);
        
        $__internal_7166555e1b30693bc333cdfcfc8f4c790b8a1f269886ac36e4680f7935b930b2->leave($__internal_7166555e1b30693bc333cdfcfc8f4c790b8a1f269886ac36e4680f7935b930b2_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Group:show.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Group/show.html.twig");
    }
}
