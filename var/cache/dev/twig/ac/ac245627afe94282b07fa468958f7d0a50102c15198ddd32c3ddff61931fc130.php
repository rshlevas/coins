<?php

/* cart/products_table.html.twig */
class __TwigTemplate_1ee9e4ca215146dbfd8ffebad3f29207bba71c3fe61f275527764478ed08930c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0af7da86305231cb2bbe99f3182c89309dde79f634288d5c663f99f7fb8360ac = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0af7da86305231cb2bbe99f3182c89309dde79f634288d5c663f99f7fb8360ac->enter($__internal_0af7da86305231cb2bbe99f3182c89309dde79f634288d5c663f99f7fb8360ac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "cart/products_table.html.twig"));

        $__internal_f8823d8e1b9d3adcc76ad66fd149960e8aca761db29e2658b9332d9d04ef6292 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f8823d8e1b9d3adcc76ad66fd149960e8aca761db29e2658b9332d9d04ef6292->enter($__internal_f8823d8e1b9d3adcc76ad66fd149960e8aca761db29e2658b9332d9d04ef6292_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "cart/products_table.html.twig"));

        // line 1
        echo "                        <p>Вы выбрали такие товары:</p>
                        <table class=\"table-bordered table-striped table\">
                            <tr>
                                <th>№</th>
                                <th>Название</th>
                                <th>Стомость, грн</th>
                                <th>Количество, шт</th>
                                <th>Удалить</th>
                            </tr>
                            ";
        // line 10
        $context["i"] = 1;
        // line 11
        echo "                            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : $this->getContext($context, "products")));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 12
            echo "                                <tr>
                                    <td>";
            // line 13
            echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")), "html", null, true);
            echo "</td>
                                    <td>
                                        <a href=\"/product/";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "\">
                                           ";
            // line 16
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
            echo "
                                        </a>
                                    </td>
                                    <td>";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "price", array()), "html", null, true);
            echo "</td>
                                    <td>";
            // line 20
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["count"]) ? $context["count"] : $this->getContext($context, "count")), $this->getAttribute($context["product"], "id", array()), array(), "array"), "html", null, true);
            echo "</td> 
                                    <td>
                                        <a href=\"/cart/delete/";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "\">
                                            <i class=\"fa fa-times\" data-id=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "\"></i>
                                        </a>
                                    </td>
                                </tr>
                                ";
            // line 27
            $context["i"] = ((isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")) + 1);
            // line 28
            echo "                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "                                <tr>
                                    <td colspan=\"4\">Общая стоимость, грн:</td>
                                    <td>";
        // line 31
        echo twig_escape_filter($this->env, (isset($context["summ"]) ? $context["summ"] : $this->getContext($context, "summ")), "html", null, true);
        echo "</td>
                                </tr>
                            
                        </table>
                        
                        <a class=\"btn btn-default checkout\" href=\"";
        // line 36
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("cart_checkout");
        echo "\"><i class=\"fa fa-shopping-cart\"></i> Оформить заказ</a>
                        <a class=\"btn btn-default checkout\" href=\"/\"><i class=\"fa fa-shopping-cart\"></i> Вернуться к покупкам</a>

<script>
    \$(document).ready(function(){
        \$(\".fa.fa-times\").click(function () {
            var id = \$(this).attr(\"data-id\");
            \$.ajax({
                type: 'post',
                dataType: 'json',
                url: \"/cart/delete/\"+id ,
                success: function (data) {
                    \$(\"#table_content\").html(data.html);

                }
            });
            \$.ajax({
                type: 'post',
                url: '";
        // line 54
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("cart_count");
        echo "',
                success: function (data) {
                    if (data !== 0) {
                        \$(\"#cart-count\").html(data);
                    }
                    else {
                        \$(\"#cart-count\").html(\"\");
                    }    
                }
            });
            return false;
        });
    }); 
    
</script>";
        
        $__internal_0af7da86305231cb2bbe99f3182c89309dde79f634288d5c663f99f7fb8360ac->leave($__internal_0af7da86305231cb2bbe99f3182c89309dde79f634288d5c663f99f7fb8360ac_prof);

        
        $__internal_f8823d8e1b9d3adcc76ad66fd149960e8aca761db29e2658b9332d9d04ef6292->leave($__internal_f8823d8e1b9d3adcc76ad66fd149960e8aca761db29e2658b9332d9d04ef6292_prof);

    }

    public function getTemplateName()
    {
        return "cart/products_table.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 54,  101 => 36,  93 => 31,  89 => 29,  83 => 28,  81 => 27,  74 => 23,  70 => 22,  65 => 20,  61 => 19,  55 => 16,  51 => 15,  46 => 13,  43 => 12,  38 => 11,  36 => 10,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("                        <p>Вы выбрали такие товары:</p>
                        <table class=\"table-bordered table-striped table\">
                            <tr>
                                <th>№</th>
                                <th>Название</th>
                                <th>Стомость, грн</th>
                                <th>Количество, шт</th>
                                <th>Удалить</th>
                            </tr>
                            {%set i = 1 %}
                            {% for product in products %}
                                <tr>
                                    <td>{{ i }}</td>
                                    <td>
                                        <a href=\"/product/{{ product.id }}\">
                                           {{ product.name }}
                                        </a>
                                    </td>
                                    <td>{{ product.price }}</td>
                                    <td>{{ count[product.id] }}</td> 
                                    <td>
                                        <a href=\"/cart/delete/{{ product.id }}\">
                                            <i class=\"fa fa-times\" data-id=\"{{ product.id }}\"></i>
                                        </a>
                                    </td>
                                </tr>
                                {% set i = i + 1 %}
                            {% endfor %}
                                <tr>
                                    <td colspan=\"4\">Общая стоимость, грн:</td>
                                    <td>{{ summ }}</td>
                                </tr>
                            
                        </table>
                        
                        <a class=\"btn btn-default checkout\" href=\"{{ path('cart_checkout') }}\"><i class=\"fa fa-shopping-cart\"></i> Оформить заказ</a>
                        <a class=\"btn btn-default checkout\" href=\"/\"><i class=\"fa fa-shopping-cart\"></i> Вернуться к покупкам</a>

<script>
    \$(document).ready(function(){
        \$(\".fa.fa-times\").click(function () {
            var id = \$(this).attr(\"data-id\");
            \$.ajax({
                type: 'post',
                dataType: 'json',
                url: \"/cart/delete/\"+id ,
                success: function (data) {
                    \$(\"#table_content\").html(data.html);

                }
            });
            \$.ajax({
                type: 'post',
                url: '{{ path('cart_count') }}',
                success: function (data) {
                    if (data !== 0) {
                        \$(\"#cart-count\").html(data);
                    }
                    else {
                        \$(\"#cart-count\").html(\"\");
                    }    
                }
            });
            return false;
        });
    }); 
    
</script>", "cart/products_table.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\cart\\products_table.html.twig");
    }
}
