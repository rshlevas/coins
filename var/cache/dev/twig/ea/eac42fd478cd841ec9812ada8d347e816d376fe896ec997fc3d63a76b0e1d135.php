<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_a75d4022ae7df99d948d7db043fd951428d3207238c95f8bc8ee72934413cbb7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_09a17cd1576d2c98274359ef13a224abfb815da26d8aae8f770ae172be76e4d2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_09a17cd1576d2c98274359ef13a224abfb815da26d8aae8f770ae172be76e4d2->enter($__internal_09a17cd1576d2c98274359ef13a224abfb815da26d8aae8f770ae172be76e4d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_09a17cd1576d2c98274359ef13a224abfb815da26d8aae8f770ae172be76e4d2->leave($__internal_09a17cd1576d2c98274359ef13a224abfb815da26d8aae8f770ae172be76e4d2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/FormTable/button_row.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\FormTable\\button_row.html.php");
    }
}
