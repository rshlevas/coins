<?php

/* cart/checkout_mistake.html.twig */
class __TwigTemplate_a9cd8482acbd314c280decd25a3c243657a28fe3099e40f945c98c90a3bbddb1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c9d80814c0a2d6245df68a6c607a60f4f735959d06ad422fbf75f08cf1de608c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c9d80814c0a2d6245df68a6c607a60f4f735959d06ad422fbf75f08cf1de608c->enter($__internal_c9d80814c0a2d6245df68a6c607a60f4f735959d06ad422fbf75f08cf1de608c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "cart/checkout_mistake.html.twig"));

        // line 1
        $this->loadTemplate("header.html.twig", "cart/checkout_mistake.html.twig", 1)->display($context);
        // line 2
        echo "
<section>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-4 col-sm-offset-4 padding-right\">
                <div class=\"signup-form\"><!--sign up form-->
                    <h2>Простите, но сервис временно недоступен</h2>
                    <h5>Вы можете оформить заказ по телефону либо отправить намо <a href=\"";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("contacts");
        echo "\">сообщение</a></h5>
            </div>
        </div>
    </div>
</section>
                    
";
        // line 15
        $this->loadTemplate("footer.html.twig", "cart/checkout_mistake.html.twig", 15)->display($context);
        
        $__internal_c9d80814c0a2d6245df68a6c607a60f4f735959d06ad422fbf75f08cf1de608c->leave($__internal_c9d80814c0a2d6245df68a6c607a60f4f735959d06ad422fbf75f08cf1de608c_prof);

    }

    public function getTemplateName()
    {
        return "cart/checkout_mistake.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 15,  33 => 9,  24 => 2,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "cart/checkout_mistake.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\cart\\checkout_mistake.html.twig");
    }
}
