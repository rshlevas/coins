<?php

/* @FOSUser/Registration/check_email.html.twig */
class __TwigTemplate_5c4dd27bd0c922101ec73419604f596204bfbb9847a268107e272fd227e89a89 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bc57b1d2304ae63c65d050ba4f320f38be259cea7e03c3ed016af1218d3836e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bc57b1d2304ae63c65d050ba4f320f38be259cea7e03c3ed016af1218d3836e3->enter($__internal_bc57b1d2304ae63c65d050ba4f320f38be259cea7e03c3ed016af1218d3836e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/check_email.html.twig"));

        // line 1
        $this->loadTemplate("@FOSUser/header.html.twig", "@FOSUser/Registration/check_email.html.twig", 1)->display($context);
        // line 2
        echo "
";
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 24
        echo "
";
        // line 25
        $this->displayBlock('footer', $context, $blocks);
        
        $__internal_bc57b1d2304ae63c65d050ba4f320f38be259cea7e03c3ed016af1218d3836e3->leave($__internal_bc57b1d2304ae63c65d050ba4f320f38be259cea7e03c3ed016af1218d3836e3_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_24e0c300a1b104b20c676d2506a4d7f56342febbd8cb16e26f3a1961ce6485ea = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_24e0c300a1b104b20c676d2506a4d7f56342febbd8cb16e26f3a1961ce6485ea->enter($__internal_24e0c300a1b104b20c676d2506a4d7f56342febbd8cb16e26f3a1961ce6485ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <section>
    <div class=\"container\">
        <div class=\"row\">
            
                <br>
                <br>
                <br>
                <h4><center>Аккаунт успешно создан! На Bаш email (";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "email", array()), "html", null, true);
        echo ") отправлено письмо с инструкциями!
                    Благодарим за регистрацию!</center></h4>
                <br>
                <br>
                <br>
                <br>

        </div>
    </div>
</section>
";
        
        $__internal_24e0c300a1b104b20c676d2506a4d7f56342febbd8cb16e26f3a1961ce6485ea->leave($__internal_24e0c300a1b104b20c676d2506a4d7f56342febbd8cb16e26f3a1961ce6485ea_prof);

    }

    // line 25
    public function block_footer($context, array $blocks = array())
    {
        $__internal_6dc4f44aa26ab80dc6c7dd6faa2bd278d9aca79cba17170053f2876e348e1196 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6dc4f44aa26ab80dc6c7dd6faa2bd278d9aca79cba17170053f2876e348e1196->enter($__internal_6dc4f44aa26ab80dc6c7dd6faa2bd278d9aca79cba17170053f2876e348e1196_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 26
        $this->loadTemplate("@FOSUser/footer.html.twig", "@FOSUser/Registration/check_email.html.twig", 26)->display($context);
        
        $__internal_6dc4f44aa26ab80dc6c7dd6faa2bd278d9aca79cba17170053f2876e348e1196->leave($__internal_6dc4f44aa26ab80dc6c7dd6faa2bd278d9aca79cba17170053f2876e348e1196_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 26,  77 => 25,  59 => 13,  50 => 6,  44 => 5,  37 => 25,  34 => 24,  32 => 5,  29 => 4,  26 => 2,  24 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Registration/check_email.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Registration\\check_email.html.twig");
    }
}
