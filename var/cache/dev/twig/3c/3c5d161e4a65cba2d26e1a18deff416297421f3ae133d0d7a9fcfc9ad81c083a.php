<?php

/* FOSUserBundle::header.html.twig */
class __TwigTemplate_791f5a4f309e20063f75c8bbb2c4da7fe6ad2af1a341aedd2367b7e5f5ce51c0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0cbad9d264e982b7c4689f83b8ae911ba229bc643ba6570ab02fc77c8a41f502 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0cbad9d264e982b7c4689f83b8ae911ba229bc643ba6570ab02fc77c8a41f502->enter($__internal_0cbad9d264e982b7c4689f83b8ae911ba229bc643ba6570ab02fc77c8a41f502_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle::header.html.twig"));

        // line 2
        echo "<!DOCTYPE html>
<html lang=\"en\">
    <head>
        <meta charset=\"utf-8\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
        <meta name=\"description\" content=\"\">
        <meta name=\"author\" content=\"\">
        <title>Главная</title>
        <link href=\"/css/bootstrap.min.css\" rel=\"stylesheet\">
        <link href=\"/css/font-awesome.min.css\" rel=\"stylesheet\">
        <link href=\"/css/prettyPhoto.css\" rel=\"stylesheet\">
        <link href=\"/css/price-range.css\" rel=\"stylesheet\">
        <link href=\"/css/animate.css\" rel=\"stylesheet\">
        <link href=\"/css/main.css\" rel=\"stylesheet\">
        <link href=\"/css/responsive.css\" rel=\"stylesheet\">
        <link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css\">
        <script src=\"//code.jquery.com/jquery-1.10.2.js\"></script>
        <script src=\"//code.jquery.com/ui/1.11.4/jquery-ui.js\"></script>
        <script src=\"/js/jquery.js\"></script>
        <script src=\"/js/bootstrap.min.js\"></script>
        <script src=\"/js/jquery.scrollUp.min.js\"></script>
        <script src=\"/js/price-range.js\"></script>
        <script src=\"/js/main.js\"></script>
        
    </head><!--/head-->
        <body>
        <div class=\"page-wrapper\">


            <header id=\"header\"><!--header-->
                <div class=\"header_top\"><!--header_top-->
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-sm-6\">
                                <div class=\"contactinfo\">
                                    <ul class=\"nav nav-pills\">
                                        <li><a href=\"#\"><i class=\"fa fa-phone\"></i> +38 093 000 11 22</a></li>
                                        <li><a href=\"#\"><i class=\"fa fa-envelope\"></i> zinchenko.us@gmail.com</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class=\"col-sm-6\">
                                <div class=\"social-icons pull-right\">
                                    <ul class=\"nav navbar-nav\">
                                        <li><a href=\"#\"><i class=\"fa fa-facebook\"></i></a></li>
                                        <li><a href=\"#\"><i class=\"fa fa-google-plus\"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/header_top-->

                <div class=\"header-middle\"><!--header-middle-->
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-sm-4\">
                                <div class=\"logo pull-left\">
                                    <a href=\"/\"><img src=\"/images/home/logo.png\" alt=\"\" /></a>
                                </div>
                            </div>
                            <div class=\"col-sm-8\">
                                <div class=\"shop-menu pull-right\">
                                    <ul class=\"nav navbar-nav\">
                                        <li><a href=\"/cart\">
                                                <i class=\"fa fa-shopping-cart\"></i> Корзина 
                                                
                                            </a>
                                        </li>
                                        ";
        // line 71
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 72
            echo "                                            <li><a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_profile_show");
            echo "\">
                                                <i class=\"fa fa-user\"></i>";
            // line 73
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array()), "html", null, true);
            echo " |
                                            </a></li>
                                            <li><a href=\"";
            // line 75
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\">
                                                <i class=\"fa fa-unlock\"></i>";
            // line 76
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Выход", array(), "FOSUserBundle"), "html", null, true);
            echo "
                                            </a></li>
                                        ";
        } else {
            // line 79
            echo "                                            <li><a href=\"";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\">
                                                <i class=\"fa fa-lock\"></i>";
            // line 80
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Войти", array(), "FOSUserBundle"), "html", null, true);
            echo "
                                            </a></li>
                                        ";
        }
        // line 83
        echo "                                            
 
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/header-middle-->

                <div class=\"header-bottom\"><!--header-bottom-->
                    <div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-sm-12\">
                                <div class=\"navbar-header\">
                                    <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                                        <span class=\"sr-only\">Toggle navigation</span>
                                        <span class=\"icon-bar\"></span>
                                        <span class=\"icon-bar\"></span>
                                        <span class=\"icon-bar\"></span>
                                    </button>
                                </div>
                                <div class=\"mainmenu pull-left\">
                                    <ul class=\"nav navbar-nav collapse navbar-collapse\">
                                        <li><a href=\"/\">Главная</a></li>
                                        <li class=\"dropdown\"><a href=\"#\">Монеты<i class=\"fa fa-angle-down\"></i></a>
                                            <ul role=\"menu\" class=\"sub-menu\">
                                                <li><a href=\"/catalog/\">Европа</a></li>
                                                <li><a href=\"/cart/\">Африка</a></li> 
                                                <li><a href=\"/cart/\">Южная Америка</a></li>
                                                <li><a href=\"/cart/\">Северная Америка</a></li>
                                                <li><a href=\"/cart/\">Азия</a></li>
                                                <li><a href=\"/cart/\">Австралия и Океания</a></li>
                                                <li><a href=\"/cart/\">Другое</a></li>
                                            </ul>
                                        </li>
                                        <li class=\"dropdown\"><a href=\"#\">Банкноты<i class=\"fa fa-angle-down\"></i></a>
                                            <ul role=\"menu\" class=\"sub-menu\">
                                                <li><a href=\"/catalog/\">Европа</a></li>
                                                <li><a href=\"/cart/\">Африка</a></li> 
                                                <li><a href=\"/cart/\">Южная Америка</a></li>
                                                <li><a href=\"/cart/\">Северная Америка</a></li>
                                                <li><a href=\"/cart/\">Азия</a></li>
                                                <li><a href=\"/cart/\">Австралия и Океания</a></li>
                                                <li><a href=\"/cart/\">Другое</a></li>
                                            </ul>
                                        </li>
                                        <li><a href=\"/about/\">Аксессуары</a></li>
                                        <li><a href=\"/about/\">О магазине</a></li>
                                        <li><a href=\"/contacts/\">Контакты</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/header-bottom-->

            </header><!--/header-->
    
";
        
        $__internal_0cbad9d264e982b7c4689f83b8ae911ba229bc643ba6570ab02fc77c8a41f502->leave($__internal_0cbad9d264e982b7c4689f83b8ae911ba229bc643ba6570ab02fc77c8a41f502_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle::header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 83,  120 => 80,  115 => 79,  109 => 76,  105 => 75,  100 => 73,  95 => 72,  93 => 71,  22 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle::header.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle/Resources/views/header.html.twig");
    }
}
