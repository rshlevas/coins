<?php

/* @FOSUser/Resetting/check_email.html.twig */
class __TwigTemplate_18d174c0b151f8fe6e257c9a78ba65dc5aaf01b9ef4b80947dabbf59ef4134d4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Resetting/check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c87406529ed4d3fadb6288fc284481e08dda1985baed506cf16b6ad9025ea93a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c87406529ed4d3fadb6288fc284481e08dda1985baed506cf16b6ad9025ea93a->enter($__internal_c87406529ed4d3fadb6288fc284481e08dda1985baed506cf16b6ad9025ea93a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c87406529ed4d3fadb6288fc284481e08dda1985baed506cf16b6ad9025ea93a->leave($__internal_c87406529ed4d3fadb6288fc284481e08dda1985baed506cf16b6ad9025ea93a_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_3c885f767decd5e2c08e7be33f28c2db7ce3fe847ddbce2c1d9f4066d3718241 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3c885f767decd5e2c08e7be33f28c2db7ce3fe847ddbce2c1d9f4066d3718241->enter($__internal_3c885f767decd5e2c08e7be33f28c2db7ce3fe847ddbce2c1d9f4066d3718241_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo nl2br(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.check_email", array("%tokenLifetime%" => (isset($context["tokenLifetime"]) ? $context["tokenLifetime"] : null)), "FOSUserBundle"), "html", null, true));
        echo "
</p>
";
        
        $__internal_3c885f767decd5e2c08e7be33f28c2db7ce3fe847ddbce2c1d9f4066d3718241->leave($__internal_3c885f767decd5e2c08e7be33f28c2db7ce3fe847ddbce2c1d9f4066d3718241_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 7,  40 => 6,  34 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Resetting/check_email.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Resetting\\check_email.html.twig");
    }
}
