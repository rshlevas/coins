<?php

/* @Twig/Exception/exception.js.twig */
class __TwigTemplate_8da032a2097a629bce4bf4d02a6e13865cf74f3a6ec6b261a55bf36531223374 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a33e8a9856f239cabd8a309ac98752ccd70c87e845b20d8e94e5e4bb7bfd4757 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a33e8a9856f239cabd8a309ac98752ccd70c87e845b20d8e94e5e4bb7bfd4757->enter($__internal_a33e8a9856f239cabd8a309ac98752ccd70c87e845b20d8e94e5e4bb7bfd4757_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => (isset($context["exception"]) ? $context["exception"] : null)));
        echo "
*/
";
        
        $__internal_a33e8a9856f239cabd8a309ac98752ccd70c87e845b20d8e94e5e4bb7bfd4757->leave($__internal_a33e8a9856f239cabd8a309ac98752ccd70c87e845b20d8e94e5e4bb7bfd4757_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Twig/Exception/exception.js.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception.js.twig");
    }
}
