<?php

/* FOSUserBundle:Profile:edit.html.twig */
class __TwigTemplate_02e59c87abbd56288d8fd3b0de1c0f6e5d580b92b97d1682afac690418252ebc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Profile:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b3c0e60e71b283b49145b80ac4be5848d4d720fbf8fb9df1dc96356eb944497a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b3c0e60e71b283b49145b80ac4be5848d4d720fbf8fb9df1dc96356eb944497a->enter($__internal_b3c0e60e71b283b49145b80ac4be5848d4d720fbf8fb9df1dc96356eb944497a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Profile:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b3c0e60e71b283b49145b80ac4be5848d4d720fbf8fb9df1dc96356eb944497a->leave($__internal_b3c0e60e71b283b49145b80ac4be5848d4d720fbf8fb9df1dc96356eb944497a_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_f68de7fa1d7cd7c581f5a8cb8c7b82716bd340c78067233b8f3efe826d684745 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f68de7fa1d7cd7c581f5a8cb8c7b82716bd340c78067233b8f3efe826d684745->enter($__internal_f68de7fa1d7cd7c581f5a8cb8c7b82716bd340c78067233b8f3efe826d684745_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "FOSUserBundle:Profile:edit.html.twig", 4)->display($context);
        
        $__internal_f68de7fa1d7cd7c581f5a8cb8c7b82716bd340c78067233b8f3efe826d684745->leave($__internal_f68de7fa1d7cd7c581f5a8cb8c7b82716bd340c78067233b8f3efe826d684745_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Profile:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Profile:edit.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Profile/edit.html.twig");
    }
}
