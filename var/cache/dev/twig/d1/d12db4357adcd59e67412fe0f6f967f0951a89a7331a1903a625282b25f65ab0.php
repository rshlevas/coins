<?php

/* TwigBundle:Exception:error.js.twig */
class __TwigTemplate_44c1a8daf0112bb5e08fd70612d3d44e36bd62e927fae22eb88efa725086063f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ef3894b9c80bdb6fc3d5ee23b62aa4c5601d8c93e3dea45463f657d7ed9069aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ef3894b9c80bdb6fc3d5ee23b62aa4c5601d8c93e3dea45463f657d7ed9069aa->enter($__internal_ef3894b9c80bdb6fc3d5ee23b62aa4c5601d8c93e3dea45463f657d7ed9069aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : null), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : null), "js", null, true);
        echo "

*/
";
        
        $__internal_ef3894b9c80bdb6fc3d5ee23b62aa4c5601d8c93e3dea45463f657d7ed9069aa->leave($__internal_ef3894b9c80bdb6fc3d5ee23b62aa4c5601d8c93e3dea45463f657d7ed9069aa_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "TwigBundle:Exception:error.js.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/error.js.twig");
    }
}
