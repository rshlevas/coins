<?php

/* FOSUserBundle:Group:edit.html.twig */
class __TwigTemplate_a58f6685d66f38c5238ed425e86a1c7b82fa7111ed3444f7e0ddd07d14bbacf8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4e05c9fd37b62d3d1ada2379706b2a31dde138b817df45e0768dc982e3775412 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4e05c9fd37b62d3d1ada2379706b2a31dde138b817df45e0768dc982e3775412->enter($__internal_4e05c9fd37b62d3d1ada2379706b2a31dde138b817df45e0768dc982e3775412_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4e05c9fd37b62d3d1ada2379706b2a31dde138b817df45e0768dc982e3775412->leave($__internal_4e05c9fd37b62d3d1ada2379706b2a31dde138b817df45e0768dc982e3775412_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_95dcde7d21c836f366ad6ea4c684e303abc308cd876963e661267b77877b03c0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95dcde7d21c836f366ad6ea4c684e303abc308cd876963e661267b77877b03c0->enter($__internal_95dcde7d21c836f366ad6ea4c684e303abc308cd876963e661267b77877b03c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/edit_content.html.twig", "FOSUserBundle:Group:edit.html.twig", 4)->display($context);
        
        $__internal_95dcde7d21c836f366ad6ea4c684e303abc308cd876963e661267b77877b03c0->leave($__internal_95dcde7d21c836f366ad6ea4c684e303abc308cd876963e661267b77877b03c0_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Group:edit.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Group/edit.html.twig");
    }
}
