<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_b8b507b54d2d0491c57c93f9bcd8781a0e40b828c00f924a34e021d505170ea8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b78cc6c9b0bf4d26f7b333c550e1c5c8b448c9c65b576c82ab3402ebe62ef24a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b78cc6c9b0bf4d26f7b333c550e1c5c8b448c9c65b576c82ab3402ebe62ef24a->enter($__internal_b78cc6c9b0bf4d26f7b333c550e1c5c8b448c9c65b576c82ab3402ebe62ef24a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_b78cc6c9b0bf4d26f7b333c550e1c5c8b448c9c65b576c82ab3402ebe62ef24a->leave($__internal_b78cc6c9b0bf4d26f7b333c550e1c5c8b448c9c65b576c82ab3402ebe62ef24a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/submit_widget.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\submit_widget.html.php");
    }
}
