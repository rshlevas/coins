<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_6da10075438a7769e124c309864c76365cdd2deb26e2376b2ac4b4db49183e22 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8e9cbd78749c4dacd231d51743bc29df8d27c1c9e1e823d669ae17498e2e3b54 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8e9cbd78749c4dacd231d51743bc29df8d27c1c9e1e823d669ae17498e2e3b54->enter($__internal_8e9cbd78749c4dacd231d51743bc29df8d27c1c9e1e823d669ae17498e2e3b54_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_8e9cbd78749c4dacd231d51743bc29df8d27c1c9e1e823d669ae17498e2e3b54->leave($__internal_8e9cbd78749c4dacd231d51743bc29df8d27c1c9e1e823d669ae17498e2e3b54_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/choice_widget.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\choice_widget.html.php");
    }
}
