<?php

/* @Twig/Exception/exception.atom.twig */
class __TwigTemplate_8f8005dd96de2b81268e7a233ed08239c994dacdf2d4fc915fe2a55d5682f713 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6b365ddba1f60266d7962a51ed3288faeabe3fb6f5f17d94e551c112142a19f3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6b365ddba1f60266d7962a51ed3288faeabe3fb6f5f17d94e551c112142a19f3->enter($__internal_6b365ddba1f60266d7962a51ed3288faeabe3fb6f5f17d94e551c112142a19f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => (isset($context["exception"]) ? $context["exception"] : null)));
        echo "
";
        
        $__internal_6b365ddba1f60266d7962a51ed3288faeabe3fb6f5f17d94e551c112142a19f3->leave($__internal_6b365ddba1f60266d7962a51ed3288faeabe3fb6f5f17d94e551c112142a19f3_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Twig/Exception/exception.atom.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception.atom.twig");
    }
}
