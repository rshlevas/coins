<?php

/* @WebProfiler/Profiler/ajax_layout.html.twig */
class __TwigTemplate_cc162ebe04104548ca847f68389ceda45c41f880775b9266382ea9fc2334e4a8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4b1cdd92c57a6f017bc0c13822f2fba08378864fa3135d819a343b1845701bc9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4b1cdd92c57a6f017bc0c13822f2fba08378864fa3135d819a343b1845701bc9->enter($__internal_4b1cdd92c57a6f017bc0c13822f2fba08378864fa3135d819a343b1845701bc9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_4b1cdd92c57a6f017bc0c13822f2fba08378864fa3135d819a343b1845701bc9->leave($__internal_4b1cdd92c57a6f017bc0c13822f2fba08378864fa3135d819a343b1845701bc9_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_e374c72b45a9ce38c7b603787bb45dc3b7e86dee719682ee56f920cc74a625d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e374c72b45a9ce38c7b603787bb45dc3b7e86dee719682ee56f920cc74a625d1->enter($__internal_e374c72b45a9ce38c7b603787bb45dc3b7e86dee719682ee56f920cc74a625d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_e374c72b45a9ce38c7b603787bb45dc3b7e86dee719682ee56f920cc74a625d1->leave($__internal_e374c72b45a9ce38c7b603787bb45dc3b7e86dee719682ee56f920cc74a625d1_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@WebProfiler/Profiler/ajax_layout.html.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\ajax_layout.html.twig");
    }
}
