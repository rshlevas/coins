<?php

/* site/contacts.html.twig */
class __TwigTemplate_d4ac3f7193478c220455490e33b85a3c8080ca7fcc5f2f07cbcac1ded2c2681b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0b4b480667cb78c3939ea2c2a85d2935158f3d2f2988797da4b87b9f99fc5b6e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0b4b480667cb78c3939ea2c2a85d2935158f3d2f2988797da4b87b9f99fc5b6e->enter($__internal_0b4b480667cb78c3939ea2c2a85d2935158f3d2f2988797da4b87b9f99fc5b6e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "site/contacts.html.twig"));

        // line 1
        echo twig_include($this->env, $context, "header.html.twig");
        echo "

<section>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-4 col-sm-offset-4 padding-right\">
                <div class=\"signup-form\"><!--sign up form-->
                        <h2>Обратная связь</h2>
                        <h5>Есть вопрос? Напишите нам</h5>
                            ";
        // line 10
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_start');
        echo "
                            ";
        // line 11
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : null), 'errors');
        echo "

                            <div>
                                ";
        // line 14
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "name", array()), 'errors');
        echo "
                                ";
        // line 15
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "name", array()), 'widget');
        echo "
                            </div>
                            <div>
                                ";
        // line 18
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'errors');
        echo "
                                ";
        // line 19
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "email", array()), 'widget');
        echo "
                            </div>
                            <div>
                                ";
        // line 22
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "text", array()), 'errors');
        echo "
                                ";
        // line 23
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "text", array()), 'widget');
        echo "
                            </div>
                            <div>
                                ";
        // line 26
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : null), "save", array()), 'widget');
        echo "
                            </div>

                            ";
        // line 29
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : null), 'form_end');
        echo "
                    </div><!--/sign up form-->

            </div>
        </div>
    </div>
</section>
               
";
        // line 37
        echo twig_include($this->env, $context, "footer.html.twig");
        
        $__internal_0b4b480667cb78c3939ea2c2a85d2935158f3d2f2988797da4b87b9f99fc5b6e->leave($__internal_0b4b480667cb78c3939ea2c2a85d2935158f3d2f2988797da4b87b9f99fc5b6e_prof);

    }

    public function getTemplateName()
    {
        return "site/contacts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 37,  80 => 29,  74 => 26,  68 => 23,  64 => 22,  58 => 19,  54 => 18,  48 => 15,  44 => 14,  38 => 11,  34 => 10,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "site/contacts.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\site\\contacts.html.twig");
    }
}
