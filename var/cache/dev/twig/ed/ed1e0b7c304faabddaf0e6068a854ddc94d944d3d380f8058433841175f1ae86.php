<?php

/* FOSUserBundle:Registration:check_email.html.twig */
class __TwigTemplate_3b2353e7e8c39ad96d1a9251675bd8f83ae3b56e85c4889428244957023c88df extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_72fc9aabbde6965045e8c320e47f4cfc114e9c3944275f44fa7290ac4c79b165 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_72fc9aabbde6965045e8c320e47f4cfc114e9c3944275f44fa7290ac4c79b165->enter($__internal_72fc9aabbde6965045e8c320e47f4cfc114e9c3944275f44fa7290ac4c79b165_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        // line 1
        $this->loadTemplate("@FOSUser/header.html.twig", "FOSUserBundle:Registration:check_email.html.twig", 1)->display($context);
        // line 2
        echo "
";
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 24
        echo "
";
        // line 25
        $this->displayBlock('footer', $context, $blocks);
        
        $__internal_72fc9aabbde6965045e8c320e47f4cfc114e9c3944275f44fa7290ac4c79b165->leave($__internal_72fc9aabbde6965045e8c320e47f4cfc114e9c3944275f44fa7290ac4c79b165_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_7059cb25c51696efe31dd1ae3ce133c66f208dd7b3506f42df234a8045483cb3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7059cb25c51696efe31dd1ae3ce133c66f208dd7b3506f42df234a8045483cb3->enter($__internal_7059cb25c51696efe31dd1ae3ce133c66f208dd7b3506f42df234a8045483cb3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <section>
    <div class=\"container\">
        <div class=\"row\">
            
                <br>
                <br>
                <br>
                <h4><center>Аккаунт успешно создан! На Bаш email (";
        // line 13
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "email", array()), "html", null, true);
        echo ") отправлено письмо с инструкциями!
                    Благодарим за регистрацию!</center></h4>
                <br>
                <br>
                <br>
                <br>

        </div>
    </div>
</section>
";
        
        $__internal_7059cb25c51696efe31dd1ae3ce133c66f208dd7b3506f42df234a8045483cb3->leave($__internal_7059cb25c51696efe31dd1ae3ce133c66f208dd7b3506f42df234a8045483cb3_prof);

    }

    // line 25
    public function block_footer($context, array $blocks = array())
    {
        $__internal_067aef48db9703321131f37cf32c8578a1c4c387d41531c88299fa57041ec37f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_067aef48db9703321131f37cf32c8578a1c4c387d41531c88299fa57041ec37f->enter($__internal_067aef48db9703321131f37cf32c8578a1c4c387d41531c88299fa57041ec37f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 26
        $this->loadTemplate("@FOSUser/footer.html.twig", "FOSUserBundle:Registration:check_email.html.twig", 26)->display($context);
        
        $__internal_067aef48db9703321131f37cf32c8578a1c4c387d41531c88299fa57041ec37f->leave($__internal_067aef48db9703321131f37cf32c8578a1c4c387d41531c88299fa57041ec37f_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 26,  77 => 25,  59 => 13,  50 => 6,  44 => 5,  37 => 25,  34 => 24,  32 => 5,  29 => 4,  26 => 2,  24 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Registration:check_email.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Registration/check_email.html.twig");
    }
}
