<?php

/* @WebProfiler/Profiler/info.html.twig */
class __TwigTemplate_64da5c2e241e32b954199ad5ac5898c8b43fae13f230da1428835fc4b7eaa164 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Profiler/info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'block_summary'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a02435c4cbf288cee1c0352aa16a3fdb389b0b0855cb38da28d335d2e897a479 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a02435c4cbf288cee1c0352aa16a3fdb389b0b0855cb38da28d335d2e897a479->enter($__internal_a02435c4cbf288cee1c0352aa16a3fdb389b0b0855cb38da28d335d2e897a479_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/info.html.twig"));

        // line 3
        $context["messages"] = array("purge" => array("status" => "success", "title" => "The profiler database was purged successfully", "message" => "Now you need to browse some pages with the Symfony Profiler enabled to collect data."), "no_token" => array("status" => "error", "title" => (((((        // line 11
array_key_exists("token", $context)) ? (_twig_default_filter((isset($context["token"]) ? $context["token"] : null), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 12
array_key_exists("token", $context)) ? (_twig_default_filter((isset($context["token"]) ? $context["token"] : null), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((array_key_exists("token", $context)) ? (_twig_default_filter((isset($context["token"]) ? $context["token"] : null), "")) : (""))) . "\" was not found in the database.")))), "upload_error" => array("status" => "error", "title" => "A problem occurred when uploading the data", "message" => "No file given or the file was not uploaded successfully."), "already_exists" => array("status" => "error", "title" => "A problem occurred when uploading the data", "message" => "The token already exists in the database."));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a02435c4cbf288cee1c0352aa16a3fdb389b0b0855cb38da28d335d2e897a479->leave($__internal_a02435c4cbf288cee1c0352aa16a3fdb389b0b0855cb38da28d335d2e897a479_prof);

    }

    // line 26
    public function block_summary($context, array $blocks = array())
    {
        $__internal_90f6d75822f0d17b9cba370f57b2450a1133b645208f3ba0c3cbe03a73cfc120 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_90f6d75822f0d17b9cba370f57b2450a1133b645208f3ba0c3cbe03a73cfc120->enter($__internal_90f6d75822f0d17b9cba370f57b2450a1133b645208f3ba0c3cbe03a73cfc120_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        // line 27
        echo "    <div class=\"status status-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["messages"]) ? $context["messages"] : null), (isset($context["about"]) ? $context["about"] : null), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 29
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["messages"]) ? $context["messages"] : null), (isset($context["about"]) ? $context["about"] : null), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $__internal_90f6d75822f0d17b9cba370f57b2450a1133b645208f3ba0c3cbe03a73cfc120->leave($__internal_90f6d75822f0d17b9cba370f57b2450a1133b645208f3ba0c3cbe03a73cfc120_prof);

    }

    // line 34
    public function block_panel($context, array $blocks = array())
    {
        $__internal_75c1a4f86ba68aa21857e6bcc92ff6d5e145269d874271ff4259993b626d29c0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_75c1a4f86ba68aa21857e6bcc92ff6d5e145269d874271ff4259993b626d29c0->enter($__internal_75c1a4f86ba68aa21857e6bcc92ff6d5e145269d874271ff4259993b626d29c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 35
        echo "    <h2>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["messages"]) ? $context["messages"] : null), (isset($context["about"]) ? $context["about"] : null), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["messages"]) ? $context["messages"] : null), (isset($context["about"]) ? $context["about"] : null), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_75c1a4f86ba68aa21857e6bcc92ff6d5e145269d874271ff4259993b626d29c0->leave($__internal_75c1a4f86ba68aa21857e6bcc92ff6d5e145269d874271ff4259993b626d29c0_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 36,  69 => 35,  63 => 34,  52 => 29,  46 => 27,  40 => 26,  33 => 1,  31 => 12,  30 => 11,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@WebProfiler/Profiler/info.html.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\info.html.twig");
    }
}
