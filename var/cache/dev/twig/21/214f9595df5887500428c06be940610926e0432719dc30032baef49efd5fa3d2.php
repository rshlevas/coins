<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_db3d5a3ac7aece5cf01fc33404a4037fc9c4bce1748193872a1c5dcfb7c745e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ff2c47196158f972f64d7aa70a2174c20e9c0a266c50411f17d4ecdcff0e7109 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ff2c47196158f972f64d7aa70a2174c20e9c0a266c50411f17d4ecdcff0e7109->enter($__internal_ff2c47196158f972f64d7aa70a2174c20e9c0a266c50411f17d4ecdcff0e7109_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_ff2c47196158f972f64d7aa70a2174c20e9c0a266c50411f17d4ecdcff0e7109->leave($__internal_ff2c47196158f972f64d7aa70a2174c20e9c0a266c50411f17d4ecdcff0e7109_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/button_row.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\button_row.html.php");
    }
}
