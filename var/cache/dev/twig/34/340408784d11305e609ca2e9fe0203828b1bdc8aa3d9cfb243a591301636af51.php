<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_332279c1144ec947450a595f3d0d2feee7fee38191680265d26c0ef4d563d3cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_53cd422c87a62cb0ff481a629dea28192a3d742013c53cc0e08a3ec922ec9085 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_53cd422c87a62cb0ff481a629dea28192a3d742013c53cc0e08a3ec922ec9085->enter($__internal_53cd422c87a62cb0ff481a629dea28192a3d742013c53cc0e08a3ec922ec9085_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_53cd422c87a62cb0ff481a629dea28192a3d742013c53cc0e08a3ec922ec9085->leave($__internal_53cd422c87a62cb0ff481a629dea28192a3d742013c53cc0e08a3ec922ec9085_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/form.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form.html.php");
    }
}
