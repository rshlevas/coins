<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_cb073b442a5f62042976b2c9b29265f0f102533d761b12f2080d6a9b91b47fd0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5adc7d4c98bc667741def969ec12b386fa331ec011be296b1f3b48fe7ba70da3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5adc7d4c98bc667741def969ec12b386fa331ec011be296b1f3b48fe7ba70da3->enter($__internal_5adc7d4c98bc667741def969ec12b386fa331ec011be296b1f3b48fe7ba70da3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        $__internal_70bc1ea2062a9ad5ebbb94e0b264c8c6fe5e9828983b11f7174bf28a6b06f6e5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_70bc1ea2062a9ad5ebbb94e0b264c8c6fe5e9828983b11f7174bf28a6b06f6e5->enter($__internal_70bc1ea2062a9ad5ebbb94e0b264c8c6fe5e9828983b11f7174bf28a6b06f6e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_5adc7d4c98bc667741def969ec12b386fa331ec011be296b1f3b48fe7ba70da3->leave($__internal_5adc7d4c98bc667741def969ec12b386fa331ec011be296b1f3b48fe7ba70da3_prof);

        
        $__internal_70bc1ea2062a9ad5ebbb94e0b264c8c6fe5e9828983b11f7174bf28a6b06f6e5->leave($__internal_70bc1ea2062a9ad5ebbb94e0b264c8c6fe5e9828983b11f7174bf28a6b06f6e5_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_1614f9f1573711e09c225105e93531fddf6adf34802498a6d829b4733a797763 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1614f9f1573711e09c225105e93531fddf6adf34802498a6d829b4733a797763->enter($__internal_1614f9f1573711e09c225105e93531fddf6adf34802498a6d829b4733a797763_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_80ccf793fc85b9aae4f5884c75ff9cb52c48b3c1ada68b46736265c43e775cdf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_80ccf793fc85b9aae4f5884c75ff9cb52c48b3c1ada68b46736265c43e775cdf->enter($__internal_80ccf793fc85b9aae4f5884c75ff9cb52c48b3c1ada68b46736265c43e775cdf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_80ccf793fc85b9aae4f5884c75ff9cb52c48b3c1ada68b46736265c43e775cdf->leave($__internal_80ccf793fc85b9aae4f5884c75ff9cb52c48b3c1ada68b46736265c43e775cdf_prof);

        
        $__internal_1614f9f1573711e09c225105e93531fddf6adf34802498a6d829b4733a797763->leave($__internal_1614f9f1573711e09c225105e93531fddf6adf34802498a6d829b4733a797763_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_02012d886a9cd5c43d2e027811eadf7e68a8bbb06220c1b8a520f20fc15637eb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_02012d886a9cd5c43d2e027811eadf7e68a8bbb06220c1b8a520f20fc15637eb->enter($__internal_02012d886a9cd5c43d2e027811eadf7e68a8bbb06220c1b8a520f20fc15637eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_37372ce205f57002d7473256df94cc033ac7daf155be710b587f679379424493 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_37372ce205f57002d7473256df94cc033ac7daf155be710b587f679379424493->enter($__internal_37372ce205f57002d7473256df94cc033ac7daf155be710b587f679379424493_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_37372ce205f57002d7473256df94cc033ac7daf155be710b587f679379424493->leave($__internal_37372ce205f57002d7473256df94cc033ac7daf155be710b587f679379424493_prof);

        
        $__internal_02012d886a9cd5c43d2e027811eadf7e68a8bbb06220c1b8a520f20fc15637eb->leave($__internal_02012d886a9cd5c43d2e027811eadf7e68a8bbb06220c1b8a520f20fc15637eb_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_d622c73ae5c9d44ffb10f5aac60b188a03079696ced7df10de1b05e5bec3f8c1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d622c73ae5c9d44ffb10f5aac60b188a03079696ced7df10de1b05e5bec3f8c1->enter($__internal_d622c73ae5c9d44ffb10f5aac60b188a03079696ced7df10de1b05e5bec3f8c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_72cec5c1a44a885c436510b24b0c6e4d7f74144d2753cb4a0b8284130a114dcd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_72cec5c1a44a885c436510b24b0c6e4d7f74144d2753cb4a0b8284130a114dcd->enter($__internal_72cec5c1a44a885c436510b24b0c6e4d7f74144d2753cb4a0b8284130a114dcd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_72cec5c1a44a885c436510b24b0c6e4d7f74144d2753cb4a0b8284130a114dcd->leave($__internal_72cec5c1a44a885c436510b24b0c6e4d7f74144d2753cb4a0b8284130a114dcd_prof);

        
        $__internal_d622c73ae5c9d44ffb10f5aac60b188a03079696ced7df10de1b05e5bec3f8c1->leave($__internal_d622c73ae5c9d44ffb10f5aac60b188a03079696ced7df10de1b05e5bec3f8c1_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\layout.html.twig");
    }
}
