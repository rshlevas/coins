<?php

/* site/contacts_success.html.twig */
class __TwigTemplate_75357470fbba0a0649de9ee515d383d5c11c99038f36a2f3b21599668c27b27c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d34efa3fb04e5425a966e1c653d6475e828ac267aad81a9ccc494e357252ddce = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d34efa3fb04e5425a966e1c653d6475e828ac267aad81a9ccc494e357252ddce->enter($__internal_d34efa3fb04e5425a966e1c653d6475e828ac267aad81a9ccc494e357252ddce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "site/contacts_success.html.twig"));

        // line 1
        echo twig_include($this->env, $context, "header.html.twig");
        echo "

<section>
    <div class=\"container\">
        <div class=\"row\">
            <h2>Ваше собщение отправленно! Наш менеджер свяжется с Вами</h2>
        </div>
    </div>
</section>
               
";
        // line 11
        echo twig_include($this->env, $context, "footer.html.twig");
        
        $__internal_d34efa3fb04e5425a966e1c653d6475e828ac267aad81a9ccc494e357252ddce->leave($__internal_d34efa3fb04e5425a966e1c653d6475e828ac267aad81a9ccc494e357252ddce_prof);

    }

    public function getTemplateName()
    {
        return "site/contacts_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 11,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "site/contacts_success.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\site\\contacts_success.html.twig");
    }
}
