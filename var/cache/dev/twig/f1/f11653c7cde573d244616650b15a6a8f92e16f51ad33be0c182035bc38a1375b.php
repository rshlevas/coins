<?php

/* @FOSUser/Profile/history.html.twig */
class __TwigTemplate_302562da3a5d2b8a40c506749c20cc19ce2f8326ddec84bd0d5ed5b6c5d60090 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_70ba37076355bcaae59968581fa2d77aa51f3e0709cfd40d70e3e038a24831bf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_70ba37076355bcaae59968581fa2d77aa51f3e0709cfd40d70e3e038a24831bf->enter($__internal_70ba37076355bcaae59968581fa2d77aa51f3e0709cfd40d70e3e038a24831bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/history.html.twig"));

        $__internal_80fb95d7c5108647c29ae8679c4061a8b0015c7058a46e0d03dba62321f54265 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_80fb95d7c5108647c29ae8679c4061a8b0015c7058a46e0d03dba62321f54265->enter($__internal_80fb95d7c5108647c29ae8679c4061a8b0015c7058a46e0d03dba62321f54265_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/history.html.twig"));

        // line 1
        $this->loadTemplate("@FOSUser/header.html.twig", "@FOSUser/Profile/history.html.twig", 1)->display($context);
        // line 2
        echo "<section>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-9 padding-right\">
                <div class=\"features_items\"><!--features_items-->
                    <h2 class=\"title text-center\">История покупок</h2>
                    <div id=\"table_content\">
                        ";
        // line 9
        if ((isset($context["orders"]) ? $context["orders"] : $this->getContext($context, "orders"))) {
            // line 10
            echo "                        ";
            $this->loadTemplate("@FOSUser/Profile/product_table.html.twig", "@FOSUser/Profile/history.html.twig", 10)->display(array_merge($context, array("products" =>             // line 11
(isset($context["products"]) ? $context["products"] : $this->getContext($context, "products")), "orders" =>             // line 12
(isset($context["orders"]) ? $context["orders"] : $this->getContext($context, "orders")))));
            // line 14
            echo "                        ";
        }
        // line 15
        echo "                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

";
        // line 22
        $this->displayBlock('footer', $context, $blocks);
        
        $__internal_70ba37076355bcaae59968581fa2d77aa51f3e0709cfd40d70e3e038a24831bf->leave($__internal_70ba37076355bcaae59968581fa2d77aa51f3e0709cfd40d70e3e038a24831bf_prof);

        
        $__internal_80fb95d7c5108647c29ae8679c4061a8b0015c7058a46e0d03dba62321f54265->leave($__internal_80fb95d7c5108647c29ae8679c4061a8b0015c7058a46e0d03dba62321f54265_prof);

    }

    public function block_footer($context, array $blocks = array())
    {
        $__internal_a8b828abce25e5683d48732eb414f81ba8bddc28ff81f3a37ddba5f924c995b8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a8b828abce25e5683d48732eb414f81ba8bddc28ff81f3a37ddba5f924c995b8->enter($__internal_a8b828abce25e5683d48732eb414f81ba8bddc28ff81f3a37ddba5f924c995b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        $__internal_21b9ec6d32de18856059cdb7b731d57133644a87d559d781f34416ed42ad6866 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_21b9ec6d32de18856059cdb7b731d57133644a87d559d781f34416ed42ad6866->enter($__internal_21b9ec6d32de18856059cdb7b731d57133644a87d559d781f34416ed42ad6866_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 23
        $this->loadTemplate("@FOSUser/footer.html.twig", "@FOSUser/Profile/history.html.twig", 23)->display($context);
        
        $__internal_21b9ec6d32de18856059cdb7b731d57133644a87d559d781f34416ed42ad6866->leave($__internal_21b9ec6d32de18856059cdb7b731d57133644a87d559d781f34416ed42ad6866_prof);

        
        $__internal_a8b828abce25e5683d48732eb414f81ba8bddc28ff81f3a37ddba5f924c995b8->leave($__internal_a8b828abce25e5683d48732eb414f81ba8bddc28ff81f3a37ddba5f924c995b8_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/history.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 23,  56 => 22,  47 => 15,  44 => 14,  42 => 12,  41 => 11,  39 => 10,  37 => 9,  28 => 2,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include \"@FOSUser/header.html.twig\" %}
<section>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-9 padding-right\">
                <div class=\"features_items\"><!--features_items-->
                    <h2 class=\"title text-center\">История покупок</h2>
                    <div id=\"table_content\">
                        {% if orders %}
                        {% include (\"@FOSUser/Profile/product_table.html.twig\") with {
                            'products' : products,
                            'orders' : orders
                        }%}
                        {% endif %}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

{% block footer %}
{% include \"@FOSUser/footer.html.twig\" %}
{% endblock footer %}", "@FOSUser/Profile/history.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Profile\\history.html.twig");
    }
}
