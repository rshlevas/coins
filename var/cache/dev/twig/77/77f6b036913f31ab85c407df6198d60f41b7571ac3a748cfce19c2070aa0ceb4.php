<?php

/* ::footer.html.twig */
class __TwigTemplate_3a57498a8cdf01fac93819c28be8467d907b07a3ffcb5520662ae9a766ed79c1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_eee6cf79ae5a8c909afc5a140dc7c73282305dc286ed14a54695cae5eba9317a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eee6cf79ae5a8c909afc5a140dc7c73282305dc286ed14a54695cae5eba9317a->enter($__internal_eee6cf79ae5a8c909afc5a140dc7c73282305dc286ed14a54695cae5eba9317a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::footer.html.twig"));

        // line 1
        echo "﻿
<div class=\"page-buffer\"></div>
</div>

<footer id=\"footer\" class=\"page-footer\"><!--Footer-->
    <div class=\"footer-bottom\">
        <div class=\"container\">
            <div class=\"row\">
                <p class=\"pull-left\">Copyright © 2016</p>
                <p class=\"pull-right\"></p>
            </div>
        </div>
    </div>
</footer><!--/Footer-->
<script>
    \$(document).ready(function(){
        \$(\".add-to-cart\").click(function () {
            var id = \$(this).attr(\"data-id\");
            \$.ajax({
                type: 'post',
                url: \"/app_dev.php/cart/\"+id ,
                success: function (data) {
                    \$(\"#cart-count\").html(data);

                }
            });
            return false;
        });
    }); 
    
    \$(document).ready(function(){
        \$.ajax({
            type: 'post',
            url: '";
        // line 34
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("cart_count");
        echo "',
            success: function (data) {
                if (data != 0) {
                    \$(\"#cart-count\").html(data);

                }
            }
        });
        return false;
    });

</script>
        



</body>
</html>";
        
        $__internal_eee6cf79ae5a8c909afc5a140dc7c73282305dc286ed14a54695cae5eba9317a->leave($__internal_eee6cf79ae5a8c909afc5a140dc7c73282305dc286ed14a54695cae5eba9317a_prof);

    }

    public function getTemplateName()
    {
        return "::footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 34,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "::footer.html.twig", "D:\\wamp64\\www\\coins\\app/Resources\\views/footer.html.twig");
    }
}
