<?php

/* cart/view.html.twig */
class __TwigTemplate_dd87ec678d15431ff8350747726c0411534da63666b05cbb3258a13ad2537283 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b56d052fc682b001406c8972ce9f12931a2a5b56f4e4a5169b1d6ee1dd1a6e9d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b56d052fc682b001406c8972ce9f12931a2a5b56f4e4a5169b1d6ee1dd1a6e9d->enter($__internal_b56d052fc682b001406c8972ce9f12931a2a5b56f4e4a5169b1d6ee1dd1a6e9d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "cart/view.html.twig"));

        // line 1
        $this->loadTemplate("header.html.twig", "cart/view.html.twig", 1)->display($context);
        // line 2
        echo "
<section>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-9 padding-right\">
                <div class=\"features_items\"><!--features_items-->
                    <h2 class=\"title text-center\">Корзина</h2>
                    <div id=\"table_content\">
                        ";
        // line 10
        if ((isset($context["products"]) ? $context["products"] : null)) {
            // line 11
            echo "                        ";
            $this->loadTemplate("cart/products_table.html.twig", "cart/view.html.twig", 11)->display(array_merge($context, array("products" =>             // line 12
(isset($context["products"]) ? $context["products"] : null), "summ" =>             // line 13
(isset($context["summ"]) ? $context["summ"] : null), "count" =>             // line 14
(isset($context["count"]) ? $context["count"] : null))));
            // line 16
            echo "                        ";
        } else {
            // line 17
            echo "                        ";
            $this->loadTemplate("cart/no_products.html.twig", "cart/view.html.twig", 17)->display($context);
            echo "    
                        ";
        }
        // line 19
        echo "                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
                    
";
        // line 26
        $this->loadTemplate("footer.html.twig", "cart/view.html.twig", 26)->display($context);
        
        $__internal_b56d052fc682b001406c8972ce9f12931a2a5b56f4e4a5169b1d6ee1dd1a6e9d->leave($__internal_b56d052fc682b001406c8972ce9f12931a2a5b56f4e4a5169b1d6ee1dd1a6e9d_prof);

    }

    public function getTemplateName()
    {
        return "cart/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 26,  51 => 19,  45 => 17,  42 => 16,  40 => 14,  39 => 13,  38 => 12,  36 => 11,  34 => 10,  24 => 2,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "cart/view.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\cart\\view.html.twig");
    }
}
