<?php

/* FOSUserBundle:Resetting:check_email.html.twig */
class __TwigTemplate_b78b8167cf1c9576799a7b68a482138d8b0d6b9af9201005abec237679d567cf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6a66f897ce965900514cb4f75bedbf10937b79187723b9ca359e0af950d97c8c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6a66f897ce965900514cb4f75bedbf10937b79187723b9ca359e0af950d97c8c->enter($__internal_6a66f897ce965900514cb4f75bedbf10937b79187723b9ca359e0af950d97c8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6a66f897ce965900514cb4f75bedbf10937b79187723b9ca359e0af950d97c8c->leave($__internal_6a66f897ce965900514cb4f75bedbf10937b79187723b9ca359e0af950d97c8c_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_f65418267b4a61dcc247daac88171b7d57422576d31c930ad258475a11c3edcc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f65418267b4a61dcc247daac88171b7d57422576d31c930ad258475a11c3edcc->enter($__internal_f65418267b4a61dcc247daac88171b7d57422576d31c930ad258475a11c3edcc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo nl2br(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.check_email", array("%tokenLifetime%" => (isset($context["tokenLifetime"]) ? $context["tokenLifetime"] : null)), "FOSUserBundle"), "html", null, true));
        echo "
</p>
";
        
        $__internal_f65418267b4a61dcc247daac88171b7d57422576d31c930ad258475a11c3edcc->leave($__internal_f65418267b4a61dcc247daac88171b7d57422576d31c930ad258475a11c3edcc_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 7,  40 => 6,  34 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Resetting:check_email.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Resetting/check_email.html.twig");
    }
}
