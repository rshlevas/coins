<?php

/* @Twig/Exception/error.json.twig */
class __TwigTemplate_36bd71a46fd88e7c3b42574a9be470449ff0c9cf83ed1a065f04e8d297418625 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a47ebbb3e3a9232b388b8551c8abb23953658a8e2d3c2d8abdff3137f734a16b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a47ebbb3e3a9232b388b8551c8abb23953658a8e2d3c2d8abdff3137f734a16b->enter($__internal_a47ebbb3e3a9232b388b8551c8abb23953658a8e2d3c2d8abdff3137f734a16b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : null), "message" => (isset($context["status_text"]) ? $context["status_text"] : null))));
        echo "
";
        
        $__internal_a47ebbb3e3a9232b388b8551c8abb23953658a8e2d3c2d8abdff3137f734a16b->leave($__internal_a47ebbb3e3a9232b388b8551c8abb23953658a8e2d3c2d8abdff3137f734a16b_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Twig/Exception/error.json.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\error.json.twig");
    }
}
