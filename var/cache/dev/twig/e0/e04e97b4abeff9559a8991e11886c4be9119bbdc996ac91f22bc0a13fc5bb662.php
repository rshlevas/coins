<?php

/* @FOSUser/Profile/product_table.html.twig */
class __TwigTemplate_1d9e0811424e6cfec5be11248c3e3d6e3f247409a4d136d97264fa018c181950 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_194e116d6f4c67afb7a325f2f7578d76782a5e30e9b312fe083f450b970e357c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_194e116d6f4c67afb7a325f2f7578d76782a5e30e9b312fe083f450b970e357c->enter($__internal_194e116d6f4c67afb7a325f2f7578d76782a5e30e9b312fe083f450b970e357c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/product_table.html.twig"));

        $__internal_720bd3f148595d4bd773abed62e1a5f1c422377b3c29c46a4eee39905ec8d5c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_720bd3f148595d4bd773abed62e1a5f1c422377b3c29c46a4eee39905ec8d5c5->enter($__internal_720bd3f148595d4bd773abed62e1a5f1c422377b3c29c46a4eee39905ec8d5c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/product_table.html.twig"));

        // line 1
        echo "                        
    <p>Вами совершены следующие покупки</p>
    <table class=\"table-bordered table-striped table\">
        <tr>
            <th>№</th>
            <th>Товары</th>
            <th>Стомость, грн</th>
            <th>Количество, шт</th>
            <th>Сумма</th>
            <th>Дата</th>
            <th>Статус</th>
        </tr>
        ";
        // line 13
        $context["i"] = 1;
        // line 14
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["orders"]) ? $context["orders"] : $this->getContext($context, "orders")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["order"]) {
            // line 15
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["products"]) ? $context["products"] : $this->getContext($context, "products")), $this->getAttribute($context["order"], "id", array()), array(), "array"));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
                // line 16
                echo "            <tr>
                ";
                // line 17
                if (($this->getAttribute($context["loop"], "index", array()) == 1)) {
                    // line 18
                    echo "                <td rowspan=\"";
                    echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute((isset($context["products"]) ? $context["products"] : $this->getContext($context, "products")), $this->getAttribute($context["order"], "id", array()), array(), "array")), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")), "html", null, true);
                    echo "</td>
                ";
                }
                // line 20
                echo "                <td>
                    <a href=\"/product/";
                // line 21
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
                echo "\">
                       ";
                // line 22
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
                echo "
                    </a>
                </td>
                <td>";
                // line 25
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "price", array()), "html", null, true);
                echo "</td>
                <td>";
                // line 26
                echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "count", array()), "html", null, true);
                echo "</td> 
                ";
                // line 27
                if (($this->getAttribute($context["loop"], "index", array()) == 1)) {
                    // line 28
                    echo "                <td rowspan=\"";
                    echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute((isset($context["products"]) ? $context["products"] : $this->getContext($context, "products")), $this->getAttribute($context["order"], "id", array()), array(), "array")), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "summ", array()), "html", null, true);
                    echo "</td>
                ";
                }
                // line 30
                echo "                ";
                if (($this->getAttribute($context["loop"], "index", array()) == 1)) {
                    // line 31
                    echo "                <td rowspan=\"";
                    echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute((isset($context["products"]) ? $context["products"] : $this->getContext($context, "products")), $this->getAttribute($context["order"], "id", array()), array(), "array")), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["order"], "date", array()), "format", array(0 => "d/m/Y"), "method"), "html", null, true);
                    echo "</td>
                ";
                }
                // line 33
                echo "                ";
                if (($this->getAttribute($context["loop"], "index", array()) == 1)) {
                    // line 34
                    echo "                <td rowspan=\"";
                    echo twig_escape_filter($this->env, twig_length_filter($this->env, $this->getAttribute((isset($context["products"]) ? $context["products"] : $this->getContext($context, "products")), $this->getAttribute($context["order"], "id", array()), array(), "array")), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["order"], "status", array()), "html", null, true);
                    echo "</td>
                ";
                }
                // line 36
                echo "            </tr>
            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 38
            echo "            ";
            $context["i"] = ((isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")) + 1);
            // line 39
            echo "        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['order'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 40
        echo "    </table>";
        
        $__internal_194e116d6f4c67afb7a325f2f7578d76782a5e30e9b312fe083f450b970e357c->leave($__internal_194e116d6f4c67afb7a325f2f7578d76782a5e30e9b312fe083f450b970e357c_prof);

        
        $__internal_720bd3f148595d4bd773abed62e1a5f1c422377b3c29c46a4eee39905ec8d5c5->leave($__internal_720bd3f148595d4bd773abed62e1a5f1c422377b3c29c46a4eee39905ec8d5c5_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/product_table.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  175 => 40,  161 => 39,  158 => 38,  143 => 36,  135 => 34,  132 => 33,  124 => 31,  121 => 30,  113 => 28,  111 => 27,  107 => 26,  103 => 25,  97 => 22,  93 => 21,  90 => 20,  82 => 18,  80 => 17,  77 => 16,  59 => 15,  41 => 14,  39 => 13,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}                        
    <p>Вами совершены следующие покупки</p>
    <table class=\"table-bordered table-striped table\">
        <tr>
            <th>№</th>
            <th>Товары</th>
            <th>Стомость, грн</th>
            <th>Количество, шт</th>
            <th>Сумма</th>
            <th>Дата</th>
            <th>Статус</th>
        </tr>
        {% set i = 1 %}
        {% for order in orders %}
            {% for product in products[order.id] %}
            <tr>
                {% if loop.index == 1 %}
                <td rowspan=\"{{ products[order.id]|length }}\">{{ i }}</td>
                {% endif %}
                <td>
                    <a href=\"/product/{{ product.id }}\">
                       {{ product.name }}
                    </a>
                </td>
                <td>{{ product.price }}</td>
                <td>{{ product.count }}</td> 
                {% if loop.index == 1 %}
                <td rowspan=\"{{ products[order.id]|length }}\">{{ order.summ }}</td>
                {% endif %}
                {% if loop.index == 1 %}
                <td rowspan=\"{{ products[order.id]|length }}\">{{ order.date.format('d/m/Y') }}</td>
                {% endif %}
                {% if loop.index == 1 %}
                <td rowspan=\"{{ products[order.id]|length }}\">{{ order.status }}</td>
                {% endif %}
            </tr>
            {% endfor %}
            {% set i = i + 1 %}
        {% endfor %}
    </table>", "@FOSUser/Profile/product_table.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Profile\\product_table.html.twig");
    }
}
