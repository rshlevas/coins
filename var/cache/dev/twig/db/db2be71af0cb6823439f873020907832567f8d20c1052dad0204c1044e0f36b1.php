<?php

/* @FOSUser/Group/new.html.twig */
class __TwigTemplate_ebdfeeebc1305377b6f7fca2450e7abbb49161bf97e62afa114bc3741d4e86d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Group/new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ae0a17ee8be693d6c4b74daaac6990f8789ae84b9f3e687a58640e63cf4bad0a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ae0a17ee8be693d6c4b74daaac6990f8789ae84b9f3e687a58640e63cf4bad0a->enter($__internal_ae0a17ee8be693d6c4b74daaac6990f8789ae84b9f3e687a58640e63cf4bad0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ae0a17ee8be693d6c4b74daaac6990f8789ae84b9f3e687a58640e63cf4bad0a->leave($__internal_ae0a17ee8be693d6c4b74daaac6990f8789ae84b9f3e687a58640e63cf4bad0a_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_39a6a958de71bbcca5658e3f99ef18873d33b1c40cc0eec86f1a4b100226e895 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_39a6a958de71bbcca5658e3f99ef18873d33b1c40cc0eec86f1a4b100226e895->enter($__internal_39a6a958de71bbcca5658e3f99ef18873d33b1c40cc0eec86f1a4b100226e895_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/new_content.html.twig", "@FOSUser/Group/new.html.twig", 4)->display($context);
        
        $__internal_39a6a958de71bbcca5658e3f99ef18873d33b1c40cc0eec86f1a4b100226e895->leave($__internal_39a6a958de71bbcca5658e3f99ef18873d33b1c40cc0eec86f1a4b100226e895_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Group/new.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Group\\new.html.twig");
    }
}
