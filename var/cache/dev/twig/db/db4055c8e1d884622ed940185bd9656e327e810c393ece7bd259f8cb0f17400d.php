<?php

/* @WebProfiler/Profiler/toolbar_redirect.html.twig */
class __TwigTemplate_3872d74020024b781f91b0d0941d79dc8971941425383549c61cb75acb343aa0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@WebProfiler/Profiler/toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_29f59e7ae1e60992a373c11c6281752d003eb1ec12206225bf43a96e20718eea = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_29f59e7ae1e60992a373c11c6281752d003eb1ec12206225bf43a96e20718eea->enter($__internal_29f59e7ae1e60992a373c11c6281752d003eb1ec12206225bf43a96e20718eea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_29f59e7ae1e60992a373c11c6281752d003eb1ec12206225bf43a96e20718eea->leave($__internal_29f59e7ae1e60992a373c11c6281752d003eb1ec12206225bf43a96e20718eea_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_124cfb515f52fb36c92b02301821f7d0ccf85c4ba521a30676b5c78c9c34f8e4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_124cfb515f52fb36c92b02301821f7d0ccf85c4ba521a30676b5c78c9c34f8e4->enter($__internal_124cfb515f52fb36c92b02301821f7d0ccf85c4ba521a30676b5c78c9c34f8e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_124cfb515f52fb36c92b02301821f7d0ccf85c4ba521a30676b5c78c9c34f8e4->leave($__internal_124cfb515f52fb36c92b02301821f7d0ccf85c4ba521a30676b5c78c9c34f8e4_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_c7c79be48e30479651441e49ab7b961b543457101068a09630f3df506b40288a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c7c79be48e30479651441e49ab7b961b543457101068a09630f3df506b40288a->enter($__internal_c7c79be48e30479651441e49ab7b961b543457101068a09630f3df506b40288a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : null), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_c7c79be48e30479651441e49ab7b961b543457101068a09630f3df506b40288a->leave($__internal_c7c79be48e30479651441e49ab7b961b543457101068a09630f3df506b40288a_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@WebProfiler/Profiler/toolbar_redirect.html.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\toolbar_redirect.html.twig");
    }
}
