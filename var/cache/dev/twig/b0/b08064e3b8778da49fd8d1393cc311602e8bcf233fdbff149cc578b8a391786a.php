<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_b95f7ab30d76a04980f24bd0199c881ce189c1a769f07275d0dfd7c1b15da1bf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_de8a284c5156244f1c60348e590be944a24c153e0c8b36f09b34b588861ca752 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_de8a284c5156244f1c60348e590be944a24c153e0c8b36f09b34b588861ca752->enter($__internal_de8a284c5156244f1c60348e590be944a24c153e0c8b36f09b34b588861ca752_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_6daf839a0101287f47e395eef554af1b0c3ef170a0cec1eec823f4ceffddba7e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6daf839a0101287f47e395eef554af1b0c3ef170a0cec1eec823f4ceffddba7e->enter($__internal_6daf839a0101287f47e395eef554af1b0c3ef170a0cec1eec823f4ceffddba7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_de8a284c5156244f1c60348e590be944a24c153e0c8b36f09b34b588861ca752->leave($__internal_de8a284c5156244f1c60348e590be944a24c153e0c8b36f09b34b588861ca752_prof);

        
        $__internal_6daf839a0101287f47e395eef554af1b0c3ef170a0cec1eec823f4ceffddba7e->leave($__internal_6daf839a0101287f47e395eef554af1b0c3ef170a0cec1eec823f4ceffddba7e_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_c3edb135c318f70fb3a509ff771b4310e20d966c4e1cc95f21e09bd4da22e9e1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c3edb135c318f70fb3a509ff771b4310e20d966c4e1cc95f21e09bd4da22e9e1->enter($__internal_c3edb135c318f70fb3a509ff771b4310e20d966c4e1cc95f21e09bd4da22e9e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_25ca10f1ce9124255cf25d072e37efe7d5e3efa4d51978eda3c69fa12c073d35 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_25ca10f1ce9124255cf25d072e37efe7d5e3efa4d51978eda3c69fa12c073d35->enter($__internal_25ca10f1ce9124255cf25d072e37efe7d5e3efa4d51978eda3c69fa12c073d35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_25ca10f1ce9124255cf25d072e37efe7d5e3efa4d51978eda3c69fa12c073d35->leave($__internal_25ca10f1ce9124255cf25d072e37efe7d5e3efa4d51978eda3c69fa12c073d35_prof);

        
        $__internal_c3edb135c318f70fb3a509ff771b4310e20d966c4e1cc95f21e09bd4da22e9e1->leave($__internal_c3edb135c318f70fb3a509ff771b4310e20d966c4e1cc95f21e09bd4da22e9e1_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_ff383b0882aeb27762df42f4b3ba72671d12fb7204da427e1eae9f08aa771e43 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ff383b0882aeb27762df42f4b3ba72671d12fb7204da427e1eae9f08aa771e43->enter($__internal_ff383b0882aeb27762df42f4b3ba72671d12fb7204da427e1eae9f08aa771e43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_4f6c76d01c7a5f75e4902cb4f7968ead453f7f9b1bb908dbb2f3eb096b42f992 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4f6c76d01c7a5f75e4902cb4f7968ead453f7f9b1bb908dbb2f3eb096b42f992->enter($__internal_4f6c76d01c7a5f75e4902cb4f7968ead453f7f9b1bb908dbb2f3eb096b42f992_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_4f6c76d01c7a5f75e4902cb4f7968ead453f7f9b1bb908dbb2f3eb096b42f992->leave($__internal_4f6c76d01c7a5f75e4902cb4f7968ead453f7f9b1bb908dbb2f3eb096b42f992_prof);

        
        $__internal_ff383b0882aeb27762df42f4b3ba72671d12fb7204da427e1eae9f08aa771e43->leave($__internal_ff383b0882aeb27762df42f4b3ba72671d12fb7204da427e1eae9f08aa771e43_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_0a548ba2cd8aa8c2bae0bdf04e7aa9ce27725bb95c0768339d912151ea4e2118 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0a548ba2cd8aa8c2bae0bdf04e7aa9ce27725bb95c0768339d912151ea4e2118->enter($__internal_0a548ba2cd8aa8c2bae0bdf04e7aa9ce27725bb95c0768339d912151ea4e2118_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_3142148ca47f810d8b39b829407c226a13d428d2bcd40c856b4881f68c652848 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3142148ca47f810d8b39b829407c226a13d428d2bcd40c856b4881f68c652848->enter($__internal_3142148ca47f810d8b39b829407c226a13d428d2bcd40c856b4881f68c652848_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_3142148ca47f810d8b39b829407c226a13d428d2bcd40c856b4881f68c652848->leave($__internal_3142148ca47f810d8b39b829407c226a13d428d2bcd40c856b4881f68c652848_prof);

        
        $__internal_0a548ba2cd8aa8c2bae0bdf04e7aa9ce27725bb95c0768339d912151ea4e2118->leave($__internal_0a548ba2cd8aa8c2bae0bdf04e7aa9ce27725bb95c0768339d912151ea4e2118_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
