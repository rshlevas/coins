<?php

/* :site:contacts_success.html.twig */
class __TwigTemplate_edd8104d82ac459fd1368829283a22ac81b3c4aa1aeae813c2de34c21b3b5e1f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2223aef7058859d3a4048e54cb583dee2fe68c8a3a0f78efc27ff7a54dbc11a2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2223aef7058859d3a4048e54cb583dee2fe68c8a3a0f78efc27ff7a54dbc11a2->enter($__internal_2223aef7058859d3a4048e54cb583dee2fe68c8a3a0f78efc27ff7a54dbc11a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":site:contacts_success.html.twig"));

        // line 1
        echo twig_include($this->env, $context, "header.html.twig");
        echo "

<section>
    <div class=\"container\">
        <div class=\"row\">
            <h2>Ваше собщение отправленно! Наш менеджер свяжется с Вами</h2>
        </div>
    </div>
</section>
               
";
        // line 11
        echo twig_include($this->env, $context, "footer.html.twig");
        
        $__internal_2223aef7058859d3a4048e54cb583dee2fe68c8a3a0f78efc27ff7a54dbc11a2->leave($__internal_2223aef7058859d3a4048e54cb583dee2fe68c8a3a0f78efc27ff7a54dbc11a2_prof);

    }

    public function getTemplateName()
    {
        return ":site:contacts_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 11,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":site:contacts_success.html.twig", "D:\\wamp64\\www\\coins\\app/Resources\\views/site/contacts_success.html.twig");
    }
}
