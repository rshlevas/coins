<?php

/* :cart:view.html.twig */
class __TwigTemplate_0efc91277c9a8d6db314e197115442b1cf2f8901e49742644b817e9c60336975 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7f884d121276fa625ffc12f35ac924b315e28c3cfd024895cf1b70c255321d82 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7f884d121276fa625ffc12f35ac924b315e28c3cfd024895cf1b70c255321d82->enter($__internal_7f884d121276fa625ffc12f35ac924b315e28c3cfd024895cf1b70c255321d82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":cart:view.html.twig"));

        // line 1
        $this->loadTemplate("header.html.twig", ":cart:view.html.twig", 1)->display($context);
        // line 2
        echo "
<section>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-9 padding-right\">
                <div class=\"features_items\"><!--features_items-->
                    <h2 class=\"title text-center\">Корзина</h2>
                    <div id=\"table_content\">
                        ";
        // line 10
        if ((isset($context["products"]) ? $context["products"] : null)) {
            // line 11
            echo "                        ";
            $this->loadTemplate("cart/products_table.html.twig", ":cart:view.html.twig", 11)->display(array_merge($context, array("products" =>             // line 12
(isset($context["products"]) ? $context["products"] : null), "summ" =>             // line 13
(isset($context["summ"]) ? $context["summ"] : null), "count" =>             // line 14
(isset($context["count"]) ? $context["count"] : null))));
            // line 16
            echo "                        ";
        } else {
            // line 17
            echo "                        ";
            $this->loadTemplate("cart/no_products.html.twig", ":cart:view.html.twig", 17)->display($context);
            echo "    
                        ";
        }
        // line 19
        echo "                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
                    
";
        // line 26
        $this->loadTemplate("footer.html.twig", ":cart:view.html.twig", 26)->display($context);
        
        $__internal_7f884d121276fa625ffc12f35ac924b315e28c3cfd024895cf1b70c255321d82->leave($__internal_7f884d121276fa625ffc12f35ac924b315e28c3cfd024895cf1b70c255321d82_prof);

    }

    public function getTemplateName()
    {
        return ":cart:view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 26,  51 => 19,  45 => 17,  42 => 16,  40 => 14,  39 => 13,  38 => 12,  36 => 11,  34 => 10,  24 => 2,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":cart:view.html.twig", "D:\\wamp64\\www\\coins\\app/Resources\\views/cart/view.html.twig");
    }
}
