<?php

/* base.html.twig */
class __TwigTemplate_bb4682898c1c55f41bd5a1ddae806dcd59807f660c722f83c638d7ce4e06557f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aeb15662305fe1342448d0cc4b199d9d19996cd267229d01066c9d0434899edc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aeb15662305fe1342448d0cc4b199d9d19996cd267229d01066c9d0434899edc->enter($__internal_aeb15662305fe1342448d0cc4b199d9d19996cd267229d01066c9d0434899edc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_aeb15662305fe1342448d0cc4b199d9d19996cd267229d01066c9d0434899edc->leave($__internal_aeb15662305fe1342448d0cc4b199d9d19996cd267229d01066c9d0434899edc_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_d257d13e8efd8d61157c2fa4f841a31bb19a791d6d7eb39016cb33f23a9a4250 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d257d13e8efd8d61157c2fa4f841a31bb19a791d6d7eb39016cb33f23a9a4250->enter($__internal_d257d13e8efd8d61157c2fa4f841a31bb19a791d6d7eb39016cb33f23a9a4250_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_d257d13e8efd8d61157c2fa4f841a31bb19a791d6d7eb39016cb33f23a9a4250->leave($__internal_d257d13e8efd8d61157c2fa4f841a31bb19a791d6d7eb39016cb33f23a9a4250_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_8abae88b0a65ee6706576f6bcade601cc0c03485ab1a37defed7c7858f535f70 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8abae88b0a65ee6706576f6bcade601cc0c03485ab1a37defed7c7858f535f70->enter($__internal_8abae88b0a65ee6706576f6bcade601cc0c03485ab1a37defed7c7858f535f70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_8abae88b0a65ee6706576f6bcade601cc0c03485ab1a37defed7c7858f535f70->leave($__internal_8abae88b0a65ee6706576f6bcade601cc0c03485ab1a37defed7c7858f535f70_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_acd915ed0fd85e1caca279e830b9cac1d3e4ae847174dfc7a5c3032144d3973e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_acd915ed0fd85e1caca279e830b9cac1d3e4ae847174dfc7a5c3032144d3973e->enter($__internal_acd915ed0fd85e1caca279e830b9cac1d3e4ae847174dfc7a5c3032144d3973e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_acd915ed0fd85e1caca279e830b9cac1d3e4ae847174dfc7a5c3032144d3973e->leave($__internal_acd915ed0fd85e1caca279e830b9cac1d3e4ae847174dfc7a5c3032144d3973e_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_cacc8d55610b52c87d2212eea320b71b1c492bde31895f48547928d17b78bac1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cacc8d55610b52c87d2212eea320b71b1c492bde31895f48547928d17b78bac1->enter($__internal_cacc8d55610b52c87d2212eea320b71b1c492bde31895f48547928d17b78bac1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_cacc8d55610b52c87d2212eea320b71b1c492bde31895f48547928d17b78bac1->leave($__internal_cacc8d55610b52c87d2212eea320b71b1c492bde31895f48547928d17b78bac1_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "base.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\base.html.twig");
    }
}
