<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_b53f2fa24fc299282bc59d2464c771b81f467ebfa42b21ee4ac1c1470d86d4f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_21fc0663f5bb6f6f02b2d1139ef166a8a4d1a657f728b023d99559082d8161a5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_21fc0663f5bb6f6f02b2d1139ef166a8a4d1a657f728b023d99559082d8161a5->enter($__internal_21fc0663f5bb6f6f02b2d1139ef166a8a4d1a657f728b023d99559082d8161a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_21fc0663f5bb6f6f02b2d1139ef166a8a4d1a657f728b023d99559082d8161a5->leave($__internal_21fc0663f5bb6f6f02b2d1139ef166a8a4d1a657f728b023d99559082d8161a5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/search_widget.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\search_widget.html.php");
    }
}
