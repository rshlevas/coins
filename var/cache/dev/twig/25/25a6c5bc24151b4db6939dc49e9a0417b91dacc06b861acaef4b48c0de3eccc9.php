<?php

/* cart/checkout_success.html.twig */
class __TwigTemplate_7564b56c0fea276d29e8c883cd85dddcf08fd8a788e1bfa72c0cf8045a0ae54d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7a91517fe743d572ae77c46e26043f42bd865a2c404a9573582761f842e9aa14 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7a91517fe743d572ae77c46e26043f42bd865a2c404a9573582761f842e9aa14->enter($__internal_7a91517fe743d572ae77c46e26043f42bd865a2c404a9573582761f842e9aa14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "cart/checkout_success.html.twig"));

        $__internal_3a42471599ee19de80094f5c7df1313b359859ec1297a5a5da1fa0ad1120f140 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3a42471599ee19de80094f5c7df1313b359859ec1297a5a5da1fa0ad1120f140->enter($__internal_3a42471599ee19de80094f5c7df1313b359859ec1297a5a5da1fa0ad1120f140_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "cart/checkout_success.html.twig"));

        // line 1
        $this->loadTemplate("header.html.twig", "cart/checkout_success.html.twig", 1)->display($context);
        // line 2
        echo "
<section>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-4 col-sm-offset-4 padding-right\">
                <div class=\"signup-form\"><!--sign up form-->
                    <h2>Заказ успешно оформлен</h2>
                    <h5>Наш менеджер свяжется с вами!</h5>
                    <h5>Есть вопрос? <a href=\"";
        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("contacts");
        echo "\">Напишите</a> нам</h5>
            </div>
        </div>
    </div>
</section>
                    
";
        // line 16
        $this->loadTemplate("footer.html.twig", "cart/checkout_success.html.twig", 16)->display($context);
        
        $__internal_7a91517fe743d572ae77c46e26043f42bd865a2c404a9573582761f842e9aa14->leave($__internal_7a91517fe743d572ae77c46e26043f42bd865a2c404a9573582761f842e9aa14_prof);

        
        $__internal_3a42471599ee19de80094f5c7df1313b359859ec1297a5a5da1fa0ad1120f140->leave($__internal_3a42471599ee19de80094f5c7df1313b359859ec1297a5a5da1fa0ad1120f140_prof);

    }

    public function getTemplateName()
    {
        return "cart/checkout_success.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 16,  37 => 10,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include ('header.html.twig') %}

<section>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-4 col-sm-offset-4 padding-right\">
                <div class=\"signup-form\"><!--sign up form-->
                    <h2>Заказ успешно оформлен</h2>
                    <h5>Наш менеджер свяжется с вами!</h5>
                    <h5>Есть вопрос? <a href=\"{{ path('contacts') }}\">Напишите</a> нам</h5>
            </div>
        </div>
    </div>
</section>
                    
{% include ('footer.html.twig') %}", "cart/checkout_success.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\cart\\checkout_success.html.twig");
    }
}
