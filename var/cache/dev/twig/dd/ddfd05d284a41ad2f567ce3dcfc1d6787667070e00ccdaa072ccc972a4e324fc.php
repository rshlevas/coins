<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_d01e31c2b75d37f6f365698ec28570b55c205cb521818fe04674fd3adb729780 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bba324500dba75f854e16ea47aee9ce5bd1459fe3c76e06ca8fa567f4c5cf8ee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bba324500dba75f854e16ea47aee9ce5bd1459fe3c76e06ca8fa567f4c5cf8ee->enter($__internal_bba324500dba75f854e16ea47aee9ce5bd1459fe3c76e06ca8fa567f4c5cf8ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_bba324500dba75f854e16ea47aee9ce5bd1459fe3c76e06ca8fa567f4c5cf8ee->leave($__internal_bba324500dba75f854e16ea47aee9ce5bd1459fe3c76e06ca8fa567f4c5cf8ee_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/form_row.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_row.html.php");
    }
}
