<?php

/* :emails:contact.html.twig */
class __TwigTemplate_cda1ab3339453ba20b30afb5b77f043dbbf28873cee4358a0139de307942c87b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d9d5503a7639f63452725e242fe2bfa52400054d7951dbe4f8608eae089f4fc1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d9d5503a7639f63452725e242fe2bfa52400054d7951dbe4f8608eae089f4fc1->enter($__internal_d9d5503a7639f63452725e242fe2bfa52400054d7951dbe4f8608eae089f4fc1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":emails:contact.html.twig"));

        // line 1
        echo "<h3>Вам пришло сообщение!</h3>

Имя: ";
        // line 3
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["contact"]) ? $context["contact"] : null), "name", array()), "html", null, true);
        echo "
Email: ";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["contact"]) ? $context["contact"] : null), "email", array()), "html", null, true);
        echo "
Текст: ";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["contact"]) ? $context["contact"] : null), "text", array()), "html", null, true);
        echo "


";
        
        $__internal_d9d5503a7639f63452725e242fe2bfa52400054d7951dbe4f8608eae089f4fc1->leave($__internal_d9d5503a7639f63452725e242fe2bfa52400054d7951dbe4f8608eae089f4fc1_prof);

    }

    public function getTemplateName()
    {
        return ":emails:contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  34 => 5,  30 => 4,  26 => 3,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":emails:contact.html.twig", "D:\\wamp64\\www\\coins\\app/Resources\\views/emails/contact.html.twig");
    }
}
