<?php

/* FOSUserBundle:Group:new.html.twig */
class __TwigTemplate_911be0b191842706c47d436f46cea1b0e4a75190627274a4515030df611fe8e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:new.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2b661d20956da869e1f2e70700e207efad9cec4786cd1de4686c901d4f1bc8b6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2b661d20956da869e1f2e70700e207efad9cec4786cd1de4686c901d4f1bc8b6->enter($__internal_2b661d20956da869e1f2e70700e207efad9cec4786cd1de4686c901d4f1bc8b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2b661d20956da869e1f2e70700e207efad9cec4786cd1de4686c901d4f1bc8b6->leave($__internal_2b661d20956da869e1f2e70700e207efad9cec4786cd1de4686c901d4f1bc8b6_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_9b02b05c1c3fe2f084a514512be6165d001e993c450082d4367f27e98145f57d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9b02b05c1c3fe2f084a514512be6165d001e993c450082d4367f27e98145f57d->enter($__internal_9b02b05c1c3fe2f084a514512be6165d001e993c450082d4367f27e98145f57d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/new_content.html.twig", "FOSUserBundle:Group:new.html.twig", 4)->display($context);
        
        $__internal_9b02b05c1c3fe2f084a514512be6165d001e993c450082d4367f27e98145f57d->leave($__internal_9b02b05c1c3fe2f084a514512be6165d001e993c450082d4367f27e98145f57d_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Group:new.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Group/new.html.twig");
    }
}
