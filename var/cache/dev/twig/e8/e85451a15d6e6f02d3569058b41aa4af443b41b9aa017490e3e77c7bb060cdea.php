<?php

/* @FOSUser/Resetting/email.txt.twig */
class __TwigTemplate_e7bd851c325ef1897d8a6b238d76b7170b203b3372c3533f61f32d2310c2ad2a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d7927db839f452a99018670e76620981f21f6ba2f4e06d48ae95801992a00e1c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d7927db839f452a99018670e76620981f21f6ba2f4e06d48ae95801992a00e1c->enter($__internal_d7927db839f452a99018670e76620981f21f6ba2f4e06d48ae95801992a00e1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Resetting/email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_d7927db839f452a99018670e76620981f21f6ba2f4e06d48ae95801992a00e1c->leave($__internal_d7927db839f452a99018670e76620981f21f6ba2f4e06d48ae95801992a00e1c_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_e92f45b95a6a0091590b6c7b9c668d241a329388a0bb24e745db82bc6174d9ec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e92f45b95a6a0091590b6c7b9c668d241a329388a0bb24e745db82bc6174d9ec->enter($__internal_e92f45b95a6a0091590b6c7b9c668d241a329388a0bb24e745db82bc6174d9ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array())), "FOSUserBundle");
        
        $__internal_e92f45b95a6a0091590b6c7b9c668d241a329388a0bb24e745db82bc6174d9ec->leave($__internal_e92f45b95a6a0091590b6c7b9c668d241a329388a0bb24e745db82bc6174d9ec_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_f021d68f1dd9bd08d709740cfa0de329a6fbb1a1c3cc55bb6da8b77cefd36778 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f021d68f1dd9bd08d709740cfa0de329a6fbb1a1c3cc55bb6da8b77cefd36778->enter($__internal_f021d68f1dd9bd08d709740cfa0de329a6fbb1a1c3cc55bb6da8b77cefd36778_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : null)), "FOSUserBundle");
        echo "
";
        
        $__internal_f021d68f1dd9bd08d709740cfa0de329a6fbb1a1c3cc55bb6da8b77cefd36778->leave($__internal_f021d68f1dd9bd08d709740cfa0de329a6fbb1a1c3cc55bb6da8b77cefd36778_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_aca61fb34cd0c8a0c387b338d19ce2428e15a56aaee908cf52b5b5ee36863a1d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aca61fb34cd0c8a0c387b338d19ce2428e15a56aaee908cf52b5b5ee36863a1d->enter($__internal_aca61fb34cd0c8a0c387b338d19ce2428e15a56aaee908cf52b5b5ee36863a1d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_aca61fb34cd0c8a0c387b338d19ce2428e15a56aaee908cf52b5b5ee36863a1d->leave($__internal_aca61fb34cd0c8a0c387b338d19ce2428e15a56aaee908cf52b5b5ee36863a1d_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Resetting/email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  67 => 13,  58 => 10,  52 => 8,  45 => 4,  39 => 2,  32 => 13,  30 => 8,  27 => 7,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Resetting/email.txt.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Resetting\\email.txt.twig");
    }
}
