<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_e78626fc1c936ede62581995afc21a2a68001c09f1974894da1cee48208b2ce2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4dcbfaa79fec4b4e3188aa98868f4ad46c60ebf74e5f5bad7caa18d36c01c5e5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4dcbfaa79fec4b4e3188aa98868f4ad46c60ebf74e5f5bad7caa18d36c01c5e5->enter($__internal_4dcbfaa79fec4b4e3188aa98868f4ad46c60ebf74e5f5bad7caa18d36c01c5e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_4dcbfaa79fec4b4e3188aa98868f4ad46c60ebf74e5f5bad7caa18d36c01c5e5->leave($__internal_4dcbfaa79fec4b4e3188aa98868f4ad46c60ebf74e5f5bad7caa18d36c01c5e5_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/form_rows.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_rows.html.php");
    }
}
