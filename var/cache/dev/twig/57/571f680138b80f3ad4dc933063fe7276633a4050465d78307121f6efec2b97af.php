<?php

/* footer.html.twig */
class __TwigTemplate_b83aa947454a5a7221fe684811755bf9a39905096ff92546dd4f87468653673d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_67f50b1b0ed79db192c5fe7475bd8a2a80bd99b25ae0c746755a7cba4651ca04 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_67f50b1b0ed79db192c5fe7475bd8a2a80bd99b25ae0c746755a7cba4651ca04->enter($__internal_67f50b1b0ed79db192c5fe7475bd8a2a80bd99b25ae0c746755a7cba4651ca04_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "footer.html.twig"));

        $__internal_4e2b93643b68add2ad135df4e58e8d47a262178444bccf98cd1c05c640cfd418 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4e2b93643b68add2ad135df4e58e8d47a262178444bccf98cd1c05c640cfd418->enter($__internal_4e2b93643b68add2ad135df4e58e8d47a262178444bccf98cd1c05c640cfd418_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "footer.html.twig"));

        // line 1
        echo "﻿
<div class=\"page-buffer\"></div>
</div>

<footer id=\"footer\" class=\"page-footer\"><!--Footer-->
    <div class=\"footer-bottom\">
        <div class=\"container\">
            <div class=\"row\">
                <p class=\"pull-left\">Copyright © 2016</p>
                <p class=\"pull-right\"></p>
            </div>
        </div>
    </div>
</footer><!--/Footer-->
<script>
    \$(document).ready(function(){
        \$(\".add-to-cart\").click(function () {
            var id = \$(this).attr(\"data-id\");
            \$.ajax({
                type: 'post',
                url: \"/app_dev.php/cart/\"+id ,
                success: function (data) {
                    \$(\"#cart-count\").html(data);

                }
            });
            return false;
        });
    }); 
    
    \$(document).ready(function(){
        \$.ajax({
            type: 'post',
            url: '";
        // line 34
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("cart_count");
        echo "',
            success: function (data) {
                if (data != 0) {
                    \$(\"#cart-count\").html(data);

                }
            }
        });
        return false;
    });

</script>
        



</body>
</html>";
        
        $__internal_67f50b1b0ed79db192c5fe7475bd8a2a80bd99b25ae0c746755a7cba4651ca04->leave($__internal_67f50b1b0ed79db192c5fe7475bd8a2a80bd99b25ae0c746755a7cba4651ca04_prof);

        
        $__internal_4e2b93643b68add2ad135df4e58e8d47a262178444bccf98cd1c05c640cfd418->leave($__internal_4e2b93643b68add2ad135df4e58e8d47a262178444bccf98cd1c05c640cfd418_prof);

    }

    public function getTemplateName()
    {
        return "footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 34,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# app/Resources/views/footer.html.twig #}﻿
<div class=\"page-buffer\"></div>
</div>

<footer id=\"footer\" class=\"page-footer\"><!--Footer-->
    <div class=\"footer-bottom\">
        <div class=\"container\">
            <div class=\"row\">
                <p class=\"pull-left\">Copyright © 2016</p>
                <p class=\"pull-right\"></p>
            </div>
        </div>
    </div>
</footer><!--/Footer-->
<script>
    \$(document).ready(function(){
        \$(\".add-to-cart\").click(function () {
            var id = \$(this).attr(\"data-id\");
            \$.ajax({
                type: 'post',
                url: \"/app_dev.php/cart/\"+id ,
                success: function (data) {
                    \$(\"#cart-count\").html(data);

                }
            });
            return false;
        });
    }); 
    
    \$(document).ready(function(){
        \$.ajax({
            type: 'post',
            url: '{{ path('cart_count') }}',
            success: function (data) {
                if (data != 0) {
                    \$(\"#cart-count\").html(data);

                }
            }
        });
        return false;
    });

</script>
        



</body>
</html>{# empty Twig template #}
", "footer.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\footer.html.twig");
    }
}
