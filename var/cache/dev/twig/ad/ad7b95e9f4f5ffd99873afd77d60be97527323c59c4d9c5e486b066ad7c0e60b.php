<?php

/* @FOSUser/Profile/edit.html.twig */
class __TwigTemplate_d70f6168a24f8c62bd0085680297efdf16c7b2b46ea446686e4d69f55ffc08b4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Profile/edit.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_94737cbed536bc3b7878b7a953835b79dbfe0c9df77cecfe2dc0e466c4286020 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_94737cbed536bc3b7878b7a953835b79dbfe0c9df77cecfe2dc0e466c4286020->enter($__internal_94737cbed536bc3b7878b7a953835b79dbfe0c9df77cecfe2dc0e466c4286020_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_94737cbed536bc3b7878b7a953835b79dbfe0c9df77cecfe2dc0e466c4286020->leave($__internal_94737cbed536bc3b7878b7a953835b79dbfe0c9df77cecfe2dc0e466c4286020_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_60620e5074ff6177e6095bab9a4d62cc33245736946026d7aebdb4572345c3b7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_60620e5074ff6177e6095bab9a4d62cc33245736946026d7aebdb4572345c3b7->enter($__internal_60620e5074ff6177e6095bab9a4d62cc33245736946026d7aebdb4572345c3b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Profile/edit_content.html.twig", "@FOSUser/Profile/edit.html.twig", 4)->display($context);
        
        $__internal_60620e5074ff6177e6095bab9a4d62cc33245736946026d7aebdb4572345c3b7->leave($__internal_60620e5074ff6177e6095bab9a4d62cc33245736946026d7aebdb4572345c3b7_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Profile/edit.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Profile\\edit.html.twig");
    }
}
