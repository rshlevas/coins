<?php

/* @FOSUser/ChangePassword/change_password.html.twig */
class __TwigTemplate_556d10d0fd435cf03af1cd09f73a848eb2783524f5fbaedd0486d359aae8f781 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/ChangePassword/change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_03a136014a3b6f067812641d4205f571e5fd03544ae7f5b19fde1fb6e9a0bc8f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_03a136014a3b6f067812641d4205f571e5fd03544ae7f5b19fde1fb6e9a0bc8f->enter($__internal_03a136014a3b6f067812641d4205f571e5fd03544ae7f5b19fde1fb6e9a0bc8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/ChangePassword/change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_03a136014a3b6f067812641d4205f571e5fd03544ae7f5b19fde1fb6e9a0bc8f->leave($__internal_03a136014a3b6f067812641d4205f571e5fd03544ae7f5b19fde1fb6e9a0bc8f_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_68b3f98d67ed3dbc478657959632b05485835f2b3b89c44d38dd24a4f4b364e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_68b3f98d67ed3dbc478657959632b05485835f2b3b89c44d38dd24a4f4b364e3->enter($__internal_68b3f98d67ed3dbc478657959632b05485835f2b3b89c44d38dd24a4f4b364e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "@FOSUser/ChangePassword/change_password.html.twig", 4)->display($context);
        
        $__internal_68b3f98d67ed3dbc478657959632b05485835f2b3b89c44d38dd24a4f4b364e3->leave($__internal_68b3f98d67ed3dbc478657959632b05485835f2b3b89c44d38dd24a4f4b364e3_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/ChangePassword/change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/ChangePassword/change_password.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\ChangePassword\\change_password.html.twig");
    }
}
