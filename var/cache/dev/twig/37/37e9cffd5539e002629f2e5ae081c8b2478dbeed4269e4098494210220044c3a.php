<?php

/* :main:index.html.twig */
class __TwigTemplate_2b3b6e0724d74b17a2571e62c4a8dc307c69db4a6d6b296f9d597f0b1c2b8b33 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2d82e29258bdd844b9b40e3e1a6bf8a3df47510164cc427554fc25551767a24a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2d82e29258bdd844b9b40e3e1a6bf8a3df47510164cc427554fc25551767a24a->enter($__internal_2d82e29258bdd844b9b40e3e1a6bf8a3df47510164cc427554fc25551767a24a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":main:index.html.twig"));

        // line 1
        echo twig_include($this->env, $context, "header.html.twig");
        echo "

<h1>Твой первый номер - ";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["number1"]) ? $context["number1"] : null), "html", null, true);
        echo "</h1>
</br>
<h2><a href='#'>Твой второй номер - ";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["number2"]) ? $context["number2"] : null), "html", null, true);
        echo "</a></h2>
</br>
<h3>Их сумма - ";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["summ"]) ? $context["summ"] : null), "html", null, true);
        echo "</h3>

";
        // line 9
        echo twig_include($this->env, $context, "footer.html.twig");
        
        $__internal_2d82e29258bdd844b9b40e3e1a6bf8a3df47510164cc427554fc25551767a24a->leave($__internal_2d82e29258bdd844b9b40e3e1a6bf8a3df47510164cc427554fc25551767a24a_prof);

    }

    public function getTemplateName()
    {
        return ":main:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 9,  37 => 7,  32 => 5,  27 => 3,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":main:index.html.twig", "D:\\wamp64\\www\\coins\\app/Resources\\views/main/index.html.twig");
    }
}
