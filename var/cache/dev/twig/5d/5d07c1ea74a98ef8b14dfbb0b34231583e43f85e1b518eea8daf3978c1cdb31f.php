<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_56745bdf28f212fcf52b50ac2607bdd26c6013188634a2eb77cdd9314d6984d5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a12f97a7d1e1c7fb9f3fd8677ab6bc0ad9f90a3a84d14aae9b8f15a744e8abfb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a12f97a7d1e1c7fb9f3fd8677ab6bc0ad9f90a3a84d14aae9b8f15a744e8abfb->enter($__internal_a12f97a7d1e1c7fb9f3fd8677ab6bc0ad9f90a3a84d14aae9b8f15a744e8abfb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_a12f97a7d1e1c7fb9f3fd8677ab6bc0ad9f90a3a84d14aae9b8f15a744e8abfb->leave($__internal_a12f97a7d1e1c7fb9f3fd8677ab6bc0ad9f90a3a84d14aae9b8f15a744e8abfb_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/url_widget.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\url_widget.html.php");
    }
}
