<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_d57ee067aac08cbd83d1bf16900adaa4395434f0938a92299f0e837711e4b36f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c47f74ebd8ed09d04f81f1dc77bf5293ddb2c9a85f3d42886f7914162162f7d9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c47f74ebd8ed09d04f81f1dc77bf5293ddb2c9a85f3d42886f7914162162f7d9->enter($__internal_c47f74ebd8ed09d04f81f1dc77bf5293ddb2c9a85f3d42886f7914162162f7d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => (isset($context["exception"]) ? $context["exception"] : null)));
        echo "
*/
";
        
        $__internal_c47f74ebd8ed09d04f81f1dc77bf5293ddb2c9a85f3d42886f7914162162f7d9->leave($__internal_c47f74ebd8ed09d04f81f1dc77bf5293ddb2c9a85f3d42886f7914162162f7d9_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "TwigBundle:Exception:exception.js.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/exception.js.twig");
    }
}
