<?php

/* @FOSUser/footer.html.twig */
class __TwigTemplate_cd77e4704922252a52b1af00cdd9a3d22f231e0bcc232489d08c6eb47ed7f095 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a1ec0f4485ed1308a92a43212c20b7c997ced7e500aa47303e24a5e6a4f49095 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a1ec0f4485ed1308a92a43212c20b7c997ced7e500aa47303e24a5e6a4f49095->enter($__internal_a1ec0f4485ed1308a92a43212c20b7c997ced7e500aa47303e24a5e6a4f49095_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/footer.html.twig"));

        $__internal_342907c8bfd5d1e1bee9de9be1796047c7fb72a7b4f51694777bd3d06a345c43 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_342907c8bfd5d1e1bee9de9be1796047c7fb72a7b4f51694777bd3d06a345c43->enter($__internal_342907c8bfd5d1e1bee9de9be1796047c7fb72a7b4f51694777bd3d06a345c43_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/footer.html.twig"));

        // line 2
        echo "

<footer id=\"footer\" class=\"page-footer\"><!--Footer-->
    <div class=\"footer-bottom\">
        <div class=\"container\">
            <div class=\"row\">
                <p class=\"pull-left\">Copyright © 2016</p>
                <p class=\"pull-right\"></p>
            </div>
        </div>
    </div>
</footer><!--/Footer-->





</body>
</html>";
        
        $__internal_a1ec0f4485ed1308a92a43212c20b7c997ced7e500aa47303e24a5e6a4f49095->leave($__internal_a1ec0f4485ed1308a92a43212c20b7c997ced7e500aa47303e24a5e6a4f49095_prof);

        
        $__internal_342907c8bfd5d1e1bee9de9be1796047c7fb72a7b4f51694777bd3d06a345c43->leave($__internal_342907c8bfd5d1e1bee9de9be1796047c7fb72a7b4f51694777bd3d06a345c43_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}


<footer id=\"footer\" class=\"page-footer\"><!--Footer-->
    <div class=\"footer-bottom\">
        <div class=\"container\">
            <div class=\"row\">
                <p class=\"pull-left\">Copyright © 2016</p>
                <p class=\"pull-right\"></p>
            </div>
        </div>
    </div>
</footer><!--/Footer-->





</body>
</html>{# empty Twig template #}
", "@FOSUser/footer.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\footer.html.twig");
    }
}
