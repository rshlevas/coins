<?php

/* @Twig/images/chevron-right.svg */
class __TwigTemplate_e24c6cf10863e8b289c86affa34330c03c85b8b1cd8f8a4c690c9200622392e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d3a167d66bd4636ca7a14759f9a61ffe3b6f21f82a37e88828ff088fbf3f054a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d3a167d66bd4636ca7a14759f9a61ffe3b6f21f82a37e88828ff088fbf3f054a->enter($__internal_d3a167d66bd4636ca7a14759f9a61ffe3b6f21f82a37e88828ff088fbf3f054a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        $__internal_2868fcf8c1a70f65ed84850d2deccfa00c88a757f6bb7ad6d4e196de4205d1b9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2868fcf8c1a70f65ed84850d2deccfa00c88a757f6bb7ad6d4e196de4205d1b9->enter($__internal_2868fcf8c1a70f65ed84850d2deccfa00c88a757f6bb7ad6d4e196de4205d1b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
";
        
        $__internal_d3a167d66bd4636ca7a14759f9a61ffe3b6f21f82a37e88828ff088fbf3f054a->leave($__internal_d3a167d66bd4636ca7a14759f9a61ffe3b6f21f82a37e88828ff088fbf3f054a_prof);

        
        $__internal_2868fcf8c1a70f65ed84850d2deccfa00c88a757f6bb7ad6d4e196de4205d1b9->leave($__internal_2868fcf8c1a70f65ed84850d2deccfa00c88a757f6bb7ad6d4e196de4205d1b9_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/chevron-right.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
", "@Twig/images/chevron-right.svg", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\images\\chevron-right.svg");
    }
}
