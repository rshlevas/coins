<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_8b8df360178c4794d4259d33b625ded60839a769a15295313c102b74838153da extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ae7521eb3e4ab7d1944565e1cee84d9b20421eba32755061a639b020b4159675 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ae7521eb3e4ab7d1944565e1cee84d9b20421eba32755061a639b020b4159675->enter($__internal_ae7521eb3e4ab7d1944565e1cee84d9b20421eba32755061a639b020b4159675_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_ae7521eb3e4ab7d1944565e1cee84d9b20421eba32755061a639b020b4159675->leave($__internal_ae7521eb3e4ab7d1944565e1cee84d9b20421eba32755061a639b020b4159675_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_aceb90b38b253c102247eb0748ca1cfebbf58cb1ab2636c4fb187422cf1d8446 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aceb90b38b253c102247eb0748ca1cfebbf58cb1ab2636c4fb187422cf1d8446->enter($__internal_aceb90b38b253c102247eb0748ca1cfebbf58cb1ab2636c4fb187422cf1d8446_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_aceb90b38b253c102247eb0748ca1cfebbf58cb1ab2636c4fb187422cf1d8446->leave($__internal_aceb90b38b253c102247eb0748ca1cfebbf58cb1ab2636c4fb187422cf1d8446_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
