<?php

/* ::base.html.twig */
class __TwigTemplate_85886a0aedeae0ef7e8a9ceaed49e30ec1363b394e216ac6700d37f526d56ac1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_181e53a45a6ff0451201a0934b1f53634e28101eea4a52f9c4039c6bcf02abb4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_181e53a45a6ff0451201a0934b1f53634e28101eea4a52f9c4039c6bcf02abb4->enter($__internal_181e53a45a6ff0451201a0934b1f53634e28101eea4a52f9c4039c6bcf02abb4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";
        
        $__internal_181e53a45a6ff0451201a0934b1f53634e28101eea4a52f9c4039c6bcf02abb4->leave($__internal_181e53a45a6ff0451201a0934b1f53634e28101eea4a52f9c4039c6bcf02abb4_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_fa5947535f0f41ff1faaa7a0a3b212c9a07d64d5eea784c69b08e17287549c79 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fa5947535f0f41ff1faaa7a0a3b212c9a07d64d5eea784c69b08e17287549c79->enter($__internal_fa5947535f0f41ff1faaa7a0a3b212c9a07d64d5eea784c69b08e17287549c79_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_fa5947535f0f41ff1faaa7a0a3b212c9a07d64d5eea784c69b08e17287549c79->leave($__internal_fa5947535f0f41ff1faaa7a0a3b212c9a07d64d5eea784c69b08e17287549c79_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_cde10091221ed2dc363e93e02926a9781a93f9343f5f73c5764f5cd4009df8be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cde10091221ed2dc363e93e02926a9781a93f9343f5f73c5764f5cd4009df8be->enter($__internal_cde10091221ed2dc363e93e02926a9781a93f9343f5f73c5764f5cd4009df8be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_cde10091221ed2dc363e93e02926a9781a93f9343f5f73c5764f5cd4009df8be->leave($__internal_cde10091221ed2dc363e93e02926a9781a93f9343f5f73c5764f5cd4009df8be_prof);

    }

    // line 10
    public function block_body($context, array $blocks = array())
    {
        $__internal_a6621929b35f672ed0324b8bca4f84d118cf6ea7611b7975e24d40e13377d4f4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a6621929b35f672ed0324b8bca4f84d118cf6ea7611b7975e24d40e13377d4f4->enter($__internal_a6621929b35f672ed0324b8bca4f84d118cf6ea7611b7975e24d40e13377d4f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_a6621929b35f672ed0324b8bca4f84d118cf6ea7611b7975e24d40e13377d4f4->leave($__internal_a6621929b35f672ed0324b8bca4f84d118cf6ea7611b7975e24d40e13377d4f4_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_218d89d122a5d5ef1d3640f10ad481635aebf303b6f53deb86aedc20ee7da5db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_218d89d122a5d5ef1d3640f10ad481635aebf303b6f53deb86aedc20ee7da5db->enter($__internal_218d89d122a5d5ef1d3640f10ad481635aebf303b6f53deb86aedc20ee7da5db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_218d89d122a5d5ef1d3640f10ad481635aebf303b6f53deb86aedc20ee7da5db->leave($__internal_218d89d122a5d5ef1d3640f10ad481635aebf303b6f53deb86aedc20ee7da5db_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 11,  82 => 10,  71 => 6,  59 => 5,  50 => 12,  47 => 11,  45 => 10,  38 => 7,  36 => 6,  32 => 5,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "::base.html.twig", "D:\\wamp64\\www\\coins\\app/Resources\\views/base.html.twig");
    }
}
