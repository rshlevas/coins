<?php

/* FOSUserBundle:ChangePassword:change_password.html.twig */
class __TwigTemplate_e689bfc04dba54870dfb5624b312d91c46b6229bd258bafc6373b3336c521ff4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_66505ebb573afcadb782ef11f74211a4201c0d97ed6ba48e4647cecb59cb8ac7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_66505ebb573afcadb782ef11f74211a4201c0d97ed6ba48e4647cecb59cb8ac7->enter($__internal_66505ebb573afcadb782ef11f74211a4201c0d97ed6ba48e4647cecb59cb8ac7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:ChangePassword:change_password.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_66505ebb573afcadb782ef11f74211a4201c0d97ed6ba48e4647cecb59cb8ac7->leave($__internal_66505ebb573afcadb782ef11f74211a4201c0d97ed6ba48e4647cecb59cb8ac7_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_698d3c681772d67d03d6f1d4258a3ba88c3bfa8e7f11c83cdc6068eaa054d3bd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_698d3c681772d67d03d6f1d4258a3ba88c3bfa8e7f11c83cdc6068eaa054d3bd->enter($__internal_698d3c681772d67d03d6f1d4258a3ba88c3bfa8e7f11c83cdc6068eaa054d3bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/ChangePassword/change_password_content.html.twig", "FOSUserBundle:ChangePassword:change_password.html.twig", 4)->display($context);
        
        $__internal_698d3c681772d67d03d6f1d4258a3ba88c3bfa8e7f11c83cdc6068eaa054d3bd->leave($__internal_698d3c681772d67d03d6f1d4258a3ba88c3bfa8e7f11c83cdc6068eaa054d3bd_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:ChangePassword:change_password.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:ChangePassword:change_password.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle/Resources/views/ChangePassword/change_password.html.twig");
    }
}
