<?php

/* FOSUserBundle:Group:list.html.twig */
class __TwigTemplate_a45c541a5e8f0e9416614f92c4eea7661b1400013466b0e86bf82a9405692a3f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b4829978cbb431c48c0d3dd94206676874936b8ac8bec94ae41428c7d788adff = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b4829978cbb431c48c0d3dd94206676874936b8ac8bec94ae41428c7d788adff->enter($__internal_b4829978cbb431c48c0d3dd94206676874936b8ac8bec94ae41428c7d788adff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b4829978cbb431c48c0d3dd94206676874936b8ac8bec94ae41428c7d788adff->leave($__internal_b4829978cbb431c48c0d3dd94206676874936b8ac8bec94ae41428c7d788adff_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_1a2cd45cf647ddd1de71b27e52f8da379f3e0d0e8753b2e6a88b2dc54c6aaaf0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1a2cd45cf647ddd1de71b27e52f8da379f3e0d0e8753b2e6a88b2dc54c6aaaf0->enter($__internal_1a2cd45cf647ddd1de71b27e52f8da379f3e0d0e8753b2e6a88b2dc54c6aaaf0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/list_content.html.twig", "FOSUserBundle:Group:list.html.twig", 4)->display($context);
        
        $__internal_1a2cd45cf647ddd1de71b27e52f8da379f3e0d0e8753b2e6a88b2dc54c6aaaf0->leave($__internal_1a2cd45cf647ddd1de71b27e52f8da379f3e0d0e8753b2e6a88b2dc54c6aaaf0_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Group:list.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Group/list.html.twig");
    }
}
