<?php

/* @FOSUser/Registration/email.txt.twig */
class __TwigTemplate_1d9fb89ed061172a4befcb350344a008ae5a5b49550188da6f513f96cb443e2f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_359b499b8a05f1d82c925ddfd572d8cf65d9a6bddcc11340dc8e93fd3b258b77 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_359b499b8a05f1d82c925ddfd572d8cf65d9a6bddcc11340dc8e93fd3b258b77->enter($__internal_359b499b8a05f1d82c925ddfd572d8cf65d9a6bddcc11340dc8e93fd3b258b77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_359b499b8a05f1d82c925ddfd572d8cf65d9a6bddcc11340dc8e93fd3b258b77->leave($__internal_359b499b8a05f1d82c925ddfd572d8cf65d9a6bddcc11340dc8e93fd3b258b77_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_f3d58dbe2c6954c29379dc33bf71f1fdd0f6446ce94488ca26a64f99002a7a24 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f3d58dbe2c6954c29379dc33bf71f1fdd0f6446ce94488ca26a64f99002a7a24->enter($__internal_f3d58dbe2c6954c29379dc33bf71f1fdd0f6446ce94488ca26a64f99002a7a24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : null)), "FOSUserBundle");
        
        $__internal_f3d58dbe2c6954c29379dc33bf71f1fdd0f6446ce94488ca26a64f99002a7a24->leave($__internal_f3d58dbe2c6954c29379dc33bf71f1fdd0f6446ce94488ca26a64f99002a7a24_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_bcb373e3643ad8b888b8cd65ce088865f88301d843519dcd0dafd1709dab51ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bcb373e3643ad8b888b8cd65ce088865f88301d843519dcd0dafd1709dab51ba->enter($__internal_bcb373e3643ad8b888b8cd65ce088865f88301d843519dcd0dafd1709dab51ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : null)), "FOSUserBundle");
        echo "
";
        
        $__internal_bcb373e3643ad8b888b8cd65ce088865f88301d843519dcd0dafd1709dab51ba->leave($__internal_bcb373e3643ad8b888b8cd65ce088865f88301d843519dcd0dafd1709dab51ba_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_e9ce74e8a047eff6044e61ce711ac1500bbef724ff9b8161dbbe27373f6c1d71 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e9ce74e8a047eff6044e61ce711ac1500bbef724ff9b8161dbbe27373f6c1d71->enter($__internal_e9ce74e8a047eff6044e61ce711ac1500bbef724ff9b8161dbbe27373f6c1d71_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_e9ce74e8a047eff6044e61ce711ac1500bbef724ff9b8161dbbe27373f6c1d71->leave($__internal_e9ce74e8a047eff6044e61ce711ac1500bbef724ff9b8161dbbe27373f6c1d71_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  67 => 13,  58 => 10,  52 => 8,  45 => 4,  39 => 2,  32 => 13,  30 => 8,  27 => 7,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Registration/email.txt.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Registration\\email.txt.twig");
    }
}
