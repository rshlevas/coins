<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_f61fd54696004b9c8edb194c7c2a4a91ab3c46a746f6ce731dd45c2ae77db7f1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_422bfb6c35fdf26ad228e1bfe62799ee9c40e8c582742350d27637e0b866df7a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_422bfb6c35fdf26ad228e1bfe62799ee9c40e8c582742350d27637e0b866df7a->enter($__internal_422bfb6c35fdf26ad228e1bfe62799ee9c40e8c582742350d27637e0b866df7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_28cbdbb3e888da8f1568f8d18928c39bac51f9d9d20844bdd7c6068460cdc92b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28cbdbb3e888da8f1568f8d18928c39bac51f9d9d20844bdd7c6068460cdc92b->enter($__internal_28cbdbb3e888da8f1568f8d18928c39bac51f9d9d20844bdd7c6068460cdc92b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_422bfb6c35fdf26ad228e1bfe62799ee9c40e8c582742350d27637e0b866df7a->leave($__internal_422bfb6c35fdf26ad228e1bfe62799ee9c40e8c582742350d27637e0b866df7a_prof);

        
        $__internal_28cbdbb3e888da8f1568f8d18928c39bac51f9d9d20844bdd7c6068460cdc92b->leave($__internal_28cbdbb3e888da8f1568f8d18928c39bac51f9d9d20844bdd7c6068460cdc92b_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_6c1d92980313dd91dc0de74ec2efe288ea7cf16eaac1416c391541a7b902d257 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6c1d92980313dd91dc0de74ec2efe288ea7cf16eaac1416c391541a7b902d257->enter($__internal_6c1d92980313dd91dc0de74ec2efe288ea7cf16eaac1416c391541a7b902d257_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_57a5cf4afabb7299d178bdfa95f4f11431d0f07ff12a4afbb212f555d28d99f1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_57a5cf4afabb7299d178bdfa95f4f11431d0f07ff12a4afbb212f555d28d99f1->enter($__internal_57a5cf4afabb7299d178bdfa95f4f11431d0f07ff12a4afbb212f555d28d99f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_57a5cf4afabb7299d178bdfa95f4f11431d0f07ff12a4afbb212f555d28d99f1->leave($__internal_57a5cf4afabb7299d178bdfa95f4f11431d0f07ff12a4afbb212f555d28d99f1_prof);

        
        $__internal_6c1d92980313dd91dc0de74ec2efe288ea7cf16eaac1416c391541a7b902d257->leave($__internal_6c1d92980313dd91dc0de74ec2efe288ea7cf16eaac1416c391541a7b902d257_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_bc08973d0b18d28dcae357e4a56812bc5198dff2f4969fd681aa2b58e0b36094 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bc08973d0b18d28dcae357e4a56812bc5198dff2f4969fd681aa2b58e0b36094->enter($__internal_bc08973d0b18d28dcae357e4a56812bc5198dff2f4969fd681aa2b58e0b36094_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_6d3a03a5746e43683d351b80d04307feb6af08d80487aa80c4173733c0d0d682 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6d3a03a5746e43683d351b80d04307feb6af08d80487aa80c4173733c0d0d682->enter($__internal_6d3a03a5746e43683d351b80d04307feb6af08d80487aa80c4173733c0d0d682_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_6d3a03a5746e43683d351b80d04307feb6af08d80487aa80c4173733c0d0d682->leave($__internal_6d3a03a5746e43683d351b80d04307feb6af08d80487aa80c4173733c0d0d682_prof);

        
        $__internal_bc08973d0b18d28dcae357e4a56812bc5198dff2f4969fd681aa2b58e0b36094->leave($__internal_bc08973d0b18d28dcae357e4a56812bc5198dff2f4969fd681aa2b58e0b36094_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_6bb1257c47318554bcd72c781a78dd5f22dbd3c568c6cbb6d7373d8baa9f1a77 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6bb1257c47318554bcd72c781a78dd5f22dbd3c568c6cbb6d7373d8baa9f1a77->enter($__internal_6bb1257c47318554bcd72c781a78dd5f22dbd3c568c6cbb6d7373d8baa9f1a77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_a70902f57343e435613f67b4823f5e33df8c37a178b6857ac2a6b6a4c2b2c248 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a70902f57343e435613f67b4823f5e33df8c37a178b6857ac2a6b6a4c2b2c248->enter($__internal_a70902f57343e435613f67b4823f5e33df8c37a178b6857ac2a6b6a4c2b2c248_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_a70902f57343e435613f67b4823f5e33df8c37a178b6857ac2a6b6a4c2b2c248->leave($__internal_a70902f57343e435613f67b4823f5e33df8c37a178b6857ac2a6b6a4c2b2c248_prof);

        
        $__internal_6bb1257c47318554bcd72c781a78dd5f22dbd3c568c6cbb6d7373d8baa9f1a77->leave($__internal_6bb1257c47318554bcd72c781a78dd5f22dbd3c568c6cbb6d7373d8baa9f1a77_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception_full.html.twig");
    }
}
