<?php

/* FOSUserBundle:Resetting:reset.html.twig */
class __TwigTemplate_da8568cf4d555db97062da17cffdb68bd6776bb5212a8cac055b8d15a2c12b4b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a756370c01a1028392797890a50d348a182504f51cae058c1a832bfdd9c102a2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a756370c01a1028392797890a50d348a182504f51cae058c1a832bfdd9c102a2->enter($__internal_a756370c01a1028392797890a50d348a182504f51cae058c1a832bfdd9c102a2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:reset.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a756370c01a1028392797890a50d348a182504f51cae058c1a832bfdd9c102a2->leave($__internal_a756370c01a1028392797890a50d348a182504f51cae058c1a832bfdd9c102a2_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_c33dd33815fdcac84d254269f1600cfc84df4d03e3306cf7b02542b32f64839f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c33dd33815fdcac84d254269f1600cfc84df4d03e3306cf7b02542b32f64839f->enter($__internal_c33dd33815fdcac84d254269f1600cfc84df4d03e3306cf7b02542b32f64839f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Resetting/reset_content.html.twig", "FOSUserBundle:Resetting:reset.html.twig", 4)->display($context);
        
        $__internal_c33dd33815fdcac84d254269f1600cfc84df4d03e3306cf7b02542b32f64839f->leave($__internal_c33dd33815fdcac84d254269f1600cfc84df4d03e3306cf7b02542b32f64839f_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Resetting:reset.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Resetting/reset.html.twig");
    }
}
