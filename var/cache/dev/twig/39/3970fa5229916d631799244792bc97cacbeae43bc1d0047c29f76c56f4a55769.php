<?php

/* cart/no_products.html.twig */
class __TwigTemplate_10841e7b3f69a5ac6742884014b96b69f7d541cc59262a1e9cde252045ae3162 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4a1c6e8233b015628ac474cabe21961a17d64ac1b0816a1b276ba3b9203b0fe2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a1c6e8233b015628ac474cabe21961a17d64ac1b0816a1b276ba3b9203b0fe2->enter($__internal_4a1c6e8233b015628ac474cabe21961a17d64ac1b0816a1b276ba3b9203b0fe2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "cart/no_products.html.twig"));

        // line 1
        echo "<br>
<h3>Корзина пуста</h3>
<br>
<a class=\"btn btn-default checkout\" href=\"/\"><i class=\"fa fa-shopping-cart\"></i> Вернуться к покупкам</a>";
        
        $__internal_4a1c6e8233b015628ac474cabe21961a17d64ac1b0816a1b276ba3b9203b0fe2->leave($__internal_4a1c6e8233b015628ac474cabe21961a17d64ac1b0816a1b276ba3b9203b0fe2_prof);

    }

    public function getTemplateName()
    {
        return "cart/no_products.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "cart/no_products.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\cart\\no_products.html.twig");
    }
}
