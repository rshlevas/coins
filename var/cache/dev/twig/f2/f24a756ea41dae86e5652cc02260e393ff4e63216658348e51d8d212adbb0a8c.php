<?php

/* :cart:no_products.html.twig */
class __TwigTemplate_358af996a060890eb496bd496c21df486c2a29b4a20d8673dd5727a116d44e20 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bf82d92cc835a066443cd3f0e79c68fba8cc01e4c4ebc885e12a6f56d6a36d5c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bf82d92cc835a066443cd3f0e79c68fba8cc01e4c4ebc885e12a6f56d6a36d5c->enter($__internal_bf82d92cc835a066443cd3f0e79c68fba8cc01e4c4ebc885e12a6f56d6a36d5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":cart:no_products.html.twig"));

        // line 1
        echo "<br>
<h3>Корзина пуста</h3>
<br>
<a class=\"btn btn-default checkout\" href=\"/\"><i class=\"fa fa-shopping-cart\"></i> Вернуться к покупкам</a>";
        
        $__internal_bf82d92cc835a066443cd3f0e79c68fba8cc01e4c4ebc885e12a6f56d6a36d5c->leave($__internal_bf82d92cc835a066443cd3f0e79c68fba8cc01e4c4ebc885e12a6f56d6a36d5c_prof);

    }

    public function getTemplateName()
    {
        return ":cart:no_products.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", ":cart:no_products.html.twig", "D:\\wamp64\\www\\coins\\app/Resources\\views/cart/no_products.html.twig");
    }
}
