<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_3f5a5d1d3d9d71420fa02b0814711d7916a82dd50c717ad2fb3de114ad3099e7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d245c9008a36a6d9b5c343c5a4a4694ffacfd058ae5b7dc7deeba7364edcf0bc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d245c9008a36a6d9b5c343c5a4a4694ffacfd058ae5b7dc7deeba7364edcf0bc->enter($__internal_d245c9008a36a6d9b5c343c5a4a4694ffacfd058ae5b7dc7deeba7364edcf0bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_d245c9008a36a6d9b5c343c5a4a4694ffacfd058ae5b7dc7deeba7364edcf0bc->leave($__internal_d245c9008a36a6d9b5c343c5a4a4694ffacfd058ae5b7dc7deeba7364edcf0bc_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/checkbox_widget.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\checkbox_widget.html.php");
    }
}
