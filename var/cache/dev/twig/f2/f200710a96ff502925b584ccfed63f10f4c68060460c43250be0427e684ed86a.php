<?php

/* @FOSUser/Group/list.html.twig */
class __TwigTemplate_0764d5693c9606cca4881bef484266c53f21f57c5d374d52bb479922f65f8d79 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Group/list.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5f3c16bfc2a4512e601e3d31feb94675ce81ba0c82b8b46501290568d3907e2c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5f3c16bfc2a4512e601e3d31feb94675ce81ba0c82b8b46501290568d3907e2c->enter($__internal_5f3c16bfc2a4512e601e3d31feb94675ce81ba0c82b8b46501290568d3907e2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5f3c16bfc2a4512e601e3d31feb94675ce81ba0c82b8b46501290568d3907e2c->leave($__internal_5f3c16bfc2a4512e601e3d31feb94675ce81ba0c82b8b46501290568d3907e2c_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_7d66cc661dd6cd8600624ecb7ffd9f24e34d31aef105e8ab9bd8a45941d8f2e8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7d66cc661dd6cd8600624ecb7ffd9f24e34d31aef105e8ab9bd8a45941d8f2e8->enter($__internal_7d66cc661dd6cd8600624ecb7ffd9f24e34d31aef105e8ab9bd8a45941d8f2e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/list_content.html.twig", "@FOSUser/Group/list.html.twig", 4)->display($context);
        
        $__internal_7d66cc661dd6cd8600624ecb7ffd9f24e34d31aef105e8ab9bd8a45941d8f2e8->leave($__internal_7d66cc661dd6cd8600624ecb7ffd9f24e34d31aef105e8ab9bd8a45941d8f2e8_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Group/list.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Group\\list.html.twig");
    }
}
