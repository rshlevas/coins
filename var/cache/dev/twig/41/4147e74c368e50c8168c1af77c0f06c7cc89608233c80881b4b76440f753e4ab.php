<?php

/* @Framework/Form/widget_container_attributes.html.php */
class __TwigTemplate_e0a5ea468b3e292a166e7ba4429fe6b09cc5282160fddf0d697b615c2d25c38b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6ac9020c2d2fab0a6cfcb08b642dc6dee92eeb79cd4e244ef0a0b3cbe9414fbb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6ac9020c2d2fab0a6cfcb08b642dc6dee92eeb79cd4e244ef0a0b3cbe9414fbb->enter($__internal_6ac9020c2d2fab0a6cfcb08b642dc6dee92eeb79cd4e244ef0a0b3cbe9414fbb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_container_attributes.html.php"));

        // line 1
        echo "<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_6ac9020c2d2fab0a6cfcb08b642dc6dee92eeb79cd4e244ef0a0b3cbe9414fbb->leave($__internal_6ac9020c2d2fab0a6cfcb08b642dc6dee92eeb79cd4e244ef0a0b3cbe9414fbb_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/widget_container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/widget_container_attributes.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\widget_container_attributes.html.php");
    }
}
