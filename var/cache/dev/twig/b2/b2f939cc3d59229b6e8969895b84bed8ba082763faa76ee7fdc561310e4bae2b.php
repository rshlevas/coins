<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_2be7f0bc2bbba4237754e413b8e11c7e6b5c488410f752b5a40a5cc1792e0439 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6bdd78f6b8669093f2e7739fa2d571f11887b63cca5a30549ce6efb9b4d71d5f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6bdd78f6b8669093f2e7739fa2d571f11887b63cca5a30549ce6efb9b4d71d5f->enter($__internal_6bdd78f6b8669093f2e7739fa2d571f11887b63cca5a30549ce6efb9b4d71d5f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_6bdd78f6b8669093f2e7739fa2d571f11887b63cca5a30549ce6efb9b4d71d5f->leave($__internal_6bdd78f6b8669093f2e7739fa2d571f11887b63cca5a30549ce6efb9b4d71d5f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/email_widget.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\email_widget.html.php");
    }
}
