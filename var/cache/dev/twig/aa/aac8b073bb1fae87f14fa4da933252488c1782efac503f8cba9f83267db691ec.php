<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_acd5cdb2b1c37f9c2bcac58a126a52cf68193a8145d5efded9f68ffa25a176da extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fcc81e68f559d48856140f777f23125829ead1706667c81a910a3d461a61e69f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fcc81e68f559d48856140f777f23125829ead1706667c81a910a3d461a61e69f->enter($__internal_fcc81e68f559d48856140f777f23125829ead1706667c81a910a3d461a61e69f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        // line 1
        $this->loadTemplate("@FOSUser/header.html.twig", "@FOSUser/Security/login.html.twig", 1)->display($context);
        // line 2
        echo "<section>
    <div class=\"container\">
        <div class=\"row\">
            
            <div class=\"col-sm-4 col-sm-offset-4 padding-right\">
                ";
        // line 7
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 10
        echo "            
                <br>
                <p>Забыли пароль? Нажмите 
                   <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_resetting_request");
        echo "\"> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("ТУТ", array(), "FOSUserBundle"), "html", null, true);
        echo "
                   </a></p>
                <br>
            </div>
        </div>
    </div>
</section>

";
        // line 21
        $this->displayBlock('footer', $context, $blocks);
        // line 24
        echo "
";
        
        $__internal_fcc81e68f559d48856140f777f23125829ead1706667c81a910a3d461a61e69f->leave($__internal_fcc81e68f559d48856140f777f23125829ead1706667c81a910a3d461a61e69f_prof);

    }

    // line 7
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_ebc834ff06af13712917bac0c02a07a5bdcc45f643736f7db6deac098ef5daa4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ebc834ff06af13712917bac0c02a07a5bdcc45f643736f7db6deac098ef5daa4->enter($__internal_ebc834ff06af13712917bac0c02a07a5bdcc45f643736f7db6deac098ef5daa4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 8
        echo "                ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
                ";
        
        $__internal_ebc834ff06af13712917bac0c02a07a5bdcc45f643736f7db6deac098ef5daa4->leave($__internal_ebc834ff06af13712917bac0c02a07a5bdcc45f643736f7db6deac098ef5daa4_prof);

    }

    // line 21
    public function block_footer($context, array $blocks = array())
    {
        $__internal_97ebd919e775002b22aeb59edaca1bed3e6af04466bed8acdf58d2e5f14c8dfe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_97ebd919e775002b22aeb59edaca1bed3e6af04466bed8acdf58d2e5f14c8dfe->enter($__internal_97ebd919e775002b22aeb59edaca1bed3e6af04466bed8acdf58d2e5f14c8dfe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 22
        $this->loadTemplate("@FOSUser/footer.html.twig", "@FOSUser/Security/login.html.twig", 22)->display($context);
        
        $__internal_97ebd919e775002b22aeb59edaca1bed3e6af04466bed8acdf58d2e5f14c8dfe->leave($__internal_97ebd919e775002b22aeb59edaca1bed3e6af04466bed8acdf58d2e5f14c8dfe_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 22,  79 => 21,  69 => 8,  63 => 7,  55 => 24,  53 => 21,  40 => 13,  35 => 10,  33 => 7,  26 => 2,  24 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Security/login.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Security\\login.html.twig");
    }
}
