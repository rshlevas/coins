<?php

/* @FOSUser/Registration/register.html.twig */
class __TwigTemplate_27492cfc5f90b493c5ac47120db2e0602b88c56441c21c72d78e722f32b6265e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e55be2407eb09964d9dd7f0e9db65c941ee64e430193a01db0bbac6920a70667 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e55be2407eb09964d9dd7f0e9db65c941ee64e430193a01db0bbac6920a70667->enter($__internal_e55be2407eb09964d9dd7f0e9db65c941ee64e430193a01db0bbac6920a70667_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Registration/register.html.twig"));

        // line 1
        $this->loadTemplate("@FOSUser/header.html.twig", "@FOSUser/Registration/register.html.twig", 1)->display($context);
        // line 2
        echo "<section>
    <div class=\"container\">
        <div class=\"row\">
            
            <div class=\"col-sm-4 col-sm-offset-4 padding-right\">
                ";
        // line 7
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 10
        echo "            
                <br>
                <p>Если вы уже зарегестрированы нажмите 
                   <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
        echo "\"> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("ТУТ", array(), "FOSUserBundle"), "html", null, true);
        echo "
                   </a> для входа</p>
                <br>
            </div>
        </div>
    </div>
</section>

";
        // line 21
        $this->displayBlock('footer', $context, $blocks);
        
        $__internal_e55be2407eb09964d9dd7f0e9db65c941ee64e430193a01db0bbac6920a70667->leave($__internal_e55be2407eb09964d9dd7f0e9db65c941ee64e430193a01db0bbac6920a70667_prof);

    }

    // line 7
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_216e4f155a6dce99c2ac5a184c800862317a1f8aa2afb1024ac0067086830b08 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_216e4f155a6dce99c2ac5a184c800862317a1f8aa2afb1024ac0067086830b08->enter($__internal_216e4f155a6dce99c2ac5a184c800862317a1f8aa2afb1024ac0067086830b08_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 8
        echo "                ";
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "@FOSUser/Registration/register.html.twig", 8)->display($context);
        // line 9
        echo "                ";
        
        $__internal_216e4f155a6dce99c2ac5a184c800862317a1f8aa2afb1024ac0067086830b08->leave($__internal_216e4f155a6dce99c2ac5a184c800862317a1f8aa2afb1024ac0067086830b08_prof);

    }

    // line 21
    public function block_footer($context, array $blocks = array())
    {
        $__internal_95920b286a758de8e4985118a8de209f4328de42f35d80a1623666f1d65ff2d0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95920b286a758de8e4985118a8de209f4328de42f35d80a1623666f1d65ff2d0->enter($__internal_95920b286a758de8e4985118a8de209f4328de42f35d80a1623666f1d65ff2d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 22
        $this->loadTemplate("@FOSUser/footer.html.twig", "@FOSUser/Registration/register.html.twig", 22)->display($context);
        
        $__internal_95920b286a758de8e4985118a8de209f4328de42f35d80a1623666f1d65ff2d0->leave($__internal_95920b286a758de8e4985118a8de209f4328de42f35d80a1623666f1d65ff2d0_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Registration/register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 22,  76 => 21,  69 => 9,  66 => 8,  60 => 7,  53 => 21,  40 => 13,  35 => 10,  33 => 7,  26 => 2,  24 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Registration/register.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Registration\\register.html.twig");
    }
}
