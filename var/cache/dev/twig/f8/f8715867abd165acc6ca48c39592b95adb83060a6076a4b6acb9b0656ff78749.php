<?php

/* FOSUserBundle:Registration:register.html.twig */
class __TwigTemplate_b97f1d1d844da5dd22a51af6a7bda074e58936110a762e0363e936d34edfd207 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0de132411aecf831514277262697b47fc3e6e3f3b255d4955f66c024bec6511c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0de132411aecf831514277262697b47fc3e6e3f3b255d4955f66c024bec6511c->enter($__internal_0de132411aecf831514277262697b47fc3e6e3f3b255d4955f66c024bec6511c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:register.html.twig"));

        // line 1
        $this->loadTemplate("@FOSUser/header.html.twig", "FOSUserBundle:Registration:register.html.twig", 1)->display($context);
        // line 2
        echo "<section>
    <div class=\"container\">
        <div class=\"row\">
            
            <div class=\"col-sm-4 col-sm-offset-4 padding-right\">
                ";
        // line 7
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 10
        echo "            
                <br>
                <p>Если вы уже зарегестрированы нажмите 
                   <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
        echo "\"> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("ТУТ", array(), "FOSUserBundle"), "html", null, true);
        echo "
                   </a> для входа</p>
                <br>
            </div>
        </div>
    </div>
</section>

";
        // line 21
        $this->displayBlock('footer', $context, $blocks);
        
        $__internal_0de132411aecf831514277262697b47fc3e6e3f3b255d4955f66c024bec6511c->leave($__internal_0de132411aecf831514277262697b47fc3e6e3f3b255d4955f66c024bec6511c_prof);

    }

    // line 7
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_0692b113deecedbaec508e5b674f959d8333d69025df62f0d25ff722e48e6c21 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0692b113deecedbaec508e5b674f959d8333d69025df62f0d25ff722e48e6c21->enter($__internal_0692b113deecedbaec508e5b674f959d8333d69025df62f0d25ff722e48e6c21_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 8
        echo "                ";
        $this->loadTemplate("@FOSUser/Registration/register_content.html.twig", "FOSUserBundle:Registration:register.html.twig", 8)->display($context);
        // line 9
        echo "                ";
        
        $__internal_0692b113deecedbaec508e5b674f959d8333d69025df62f0d25ff722e48e6c21->leave($__internal_0692b113deecedbaec508e5b674f959d8333d69025df62f0d25ff722e48e6c21_prof);

    }

    // line 21
    public function block_footer($context, array $blocks = array())
    {
        $__internal_6acfb251800ff4b2f634c8c97c19b8aaa9fcc0db5335414595bfd3164b8ae717 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6acfb251800ff4b2f634c8c97c19b8aaa9fcc0db5335414595bfd3164b8ae717->enter($__internal_6acfb251800ff4b2f634c8c97c19b8aaa9fcc0db5335414595bfd3164b8ae717_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 22
        $this->loadTemplate("@FOSUser/footer.html.twig", "FOSUserBundle:Registration:register.html.twig", 22)->display($context);
        
        $__internal_6acfb251800ff4b2f634c8c97c19b8aaa9fcc0db5335414595bfd3164b8ae717->leave($__internal_6acfb251800ff4b2f634c8c97c19b8aaa9fcc0db5335414595bfd3164b8ae717_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 22,  76 => 21,  69 => 9,  66 => 8,  60 => 7,  53 => 21,  40 => 13,  35 => 10,  33 => 7,  26 => 2,  24 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Registration:register.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Registration/register.html.twig");
    }
}
