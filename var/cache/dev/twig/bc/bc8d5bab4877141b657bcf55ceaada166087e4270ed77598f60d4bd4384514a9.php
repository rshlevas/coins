<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_f62c71e87c55f41fe3c6ec4959c528b58e4b2f56a3b90d256ff412af821b4d29 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0309671fe547934b4a0129cd1b7010d13baeaa22bbecd802c0bcee6f7310edbf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0309671fe547934b4a0129cd1b7010d13baeaa22bbecd802c0bcee6f7310edbf->enter($__internal_0309671fe547934b4a0129cd1b7010d13baeaa22bbecd802c0bcee6f7310edbf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $__internal_d3f5786d19e58a2bc29667db1e19aa91c7e8a8de51af319964e4557b96adf60f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d3f5786d19e58a2bc29667db1e19aa91c7e8a8de51af319964e4557b96adf60f->enter($__internal_d3f5786d19e58a2bc29667db1e19aa91c7e8a8de51af319964e4557b96adf60f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        // line 1
        $this->loadTemplate("@FOSUser/header.html.twig", "@FOSUser/Security/login.html.twig", 1)->display($context);
        // line 2
        echo "<section>
    <div class=\"container\">
        <div class=\"row\">
            
            <div class=\"col-sm-4 col-sm-offset-4 padding-right\">
                ";
        // line 7
        $this->displayBlock('fos_user_content', $context, $blocks);
        // line 10
        echo "            
                <br>
                <p>Забыли пароль? Нажмите 
                   <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_resetting_request");
        echo "\"> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("ТУТ", array(), "FOSUserBundle"), "html", null, true);
        echo "
                   </a></p>
                <br>
            </div>
        </div>
    </div>
</section>

";
        // line 21
        $this->displayBlock('footer', $context, $blocks);
        // line 24
        echo "
";
        
        $__internal_0309671fe547934b4a0129cd1b7010d13baeaa22bbecd802c0bcee6f7310edbf->leave($__internal_0309671fe547934b4a0129cd1b7010d13baeaa22bbecd802c0bcee6f7310edbf_prof);

        
        $__internal_d3f5786d19e58a2bc29667db1e19aa91c7e8a8de51af319964e4557b96adf60f->leave($__internal_d3f5786d19e58a2bc29667db1e19aa91c7e8a8de51af319964e4557b96adf60f_prof);

    }

    // line 7
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_4fe208a9f6a75c392f454de9172a553759708dd4e32badf10ff4f29a2e4ed205 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4fe208a9f6a75c392f454de9172a553759708dd4e32badf10ff4f29a2e4ed205->enter($__internal_4fe208a9f6a75c392f454de9172a553759708dd4e32badf10ff4f29a2e4ed205_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_14abee93bda0c3954ac6114897401227004050d85a68f2e5cc65f39e21e441e8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_14abee93bda0c3954ac6114897401227004050d85a68f2e5cc65f39e21e441e8->enter($__internal_14abee93bda0c3954ac6114897401227004050d85a68f2e5cc65f39e21e441e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 8
        echo "                ";
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
                ";
        
        $__internal_14abee93bda0c3954ac6114897401227004050d85a68f2e5cc65f39e21e441e8->leave($__internal_14abee93bda0c3954ac6114897401227004050d85a68f2e5cc65f39e21e441e8_prof);

        
        $__internal_4fe208a9f6a75c392f454de9172a553759708dd4e32badf10ff4f29a2e4ed205->leave($__internal_4fe208a9f6a75c392f454de9172a553759708dd4e32badf10ff4f29a2e4ed205_prof);

    }

    // line 21
    public function block_footer($context, array $blocks = array())
    {
        $__internal_5362c1293fbd1b89db708cb7ed35c87a5d4863f4b9f86fb7b495f4c5416e95be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5362c1293fbd1b89db708cb7ed35c87a5d4863f4b9f86fb7b495f4c5416e95be->enter($__internal_5362c1293fbd1b89db708cb7ed35c87a5d4863f4b9f86fb7b495f4c5416e95be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        $__internal_7d849a1f96178a0adaedf915b2af8f85dd5470987df3b8c08122028518796b3c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d849a1f96178a0adaedf915b2af8f85dd5470987df3b8c08122028518796b3c->enter($__internal_7d849a1f96178a0adaedf915b2af8f85dd5470987df3b8c08122028518796b3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 22
        $this->loadTemplate("@FOSUser/footer.html.twig", "@FOSUser/Security/login.html.twig", 22)->display($context);
        
        $__internal_7d849a1f96178a0adaedf915b2af8f85dd5470987df3b8c08122028518796b3c->leave($__internal_7d849a1f96178a0adaedf915b2af8f85dd5470987df3b8c08122028518796b3c_prof);

        
        $__internal_5362c1293fbd1b89db708cb7ed35c87a5d4863f4b9f86fb7b495f4c5416e95be->leave($__internal_5362c1293fbd1b89db708cb7ed35c87a5d4863f4b9f86fb7b495f4c5416e95be_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 22,  91 => 21,  78 => 8,  69 => 7,  58 => 24,  56 => 21,  43 => 13,  38 => 10,  36 => 7,  29 => 2,  27 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include \"@FOSUser/header.html.twig\" %}
<section>
    <div class=\"container\">
        <div class=\"row\">
            
            <div class=\"col-sm-4 col-sm-offset-4 padding-right\">
                {% block fos_user_content %}
                {{ include('@FOSUser/Security/login_content.html.twig') }}
                {% endblock fos_user_content %}
            
                <br>
                <p>Забыли пароль? Нажмите 
                   <a href=\"{{ path('fos_user_resetting_request') }}\"> {{ 'ТУТ'|trans({}, 'FOSUserBundle') }}
                   </a></p>
                <br>
            </div>
        </div>
    </div>
</section>

{% block footer %}
{% include \"@FOSUser/footer.html.twig\" %}
{% endblock footer %}

", "@FOSUser/Security/login.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Security\\login.html.twig");
    }
}
