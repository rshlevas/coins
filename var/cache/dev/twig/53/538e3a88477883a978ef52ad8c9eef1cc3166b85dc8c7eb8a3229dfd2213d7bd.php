<?php

/* category/main.html.twig */
class __TwigTemplate_1677e87e34e58f727486f81ca84da001556c463eced399c484ca6a9dea0eb01c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2d73a2f9cfa9b34003ef97c0838935bb2d917a247d6c5a5d3fa4a623f4e83306 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2d73a2f9cfa9b34003ef97c0838935bb2d917a247d6c5a5d3fa4a623f4e83306->enter($__internal_2d73a2f9cfa9b34003ef97c0838935bb2d917a247d6c5a5d3fa4a623f4e83306_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "category/main.html.twig"));

        $__internal_b4713938d9425661d79c205216a07866455f0b1a1ff9d1faa4981df4883d03af = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b4713938d9425661d79c205216a07866455f0b1a1ff9d1faa4981df4883d03af->enter($__internal_b4713938d9425661d79c205216a07866455f0b1a1ff9d1faa4981df4883d03af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "category/main.html.twig"));

        // line 1
        echo twig_include($this->env, $context, "header.html.twig");
        echo "

<section>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-3\">
                <div class=\"left-sidebar\">
                    <div class=\"panel-group category-products\">
                        <h2>Регионы</h2>
                            ";
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["regions"]) ? $context["regions"] : $this->getContext($context, "regions")));
        foreach ($context['_seq'] as $context["_key"] => $context["region"]) {
            // line 11
            echo "                                <div class=\"panel panel-default\">
                                    <div class=\"panel-heading\">
                                        <h4 class=\"panel-title\">
                                            <a href=\"/";
            // line 14
            echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "html", null, true);
            echo "/";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["regions_for_route"]) ? $context["regions_for_route"] : $this->getContext($context, "regions_for_route")), $this->getAttribute($context["region"], "id", array()), array(), "array"), "html", null, true);
            echo "\">                                                                                    
                                                   ";
            // line 15
            echo twig_escape_filter($this->env, $this->getAttribute($context["region"], "name", array()), "html", null, true);
            echo "
                                            </a>
                                        </h4>
                                    </div>
                                </div>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['region'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "                        <h2><a class=\"countries_touch\" href=\"#\">Страны</a></h2>
                            ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["countries"]) ? $context["countries"] : $this->getContext($context, "countries")));
        foreach ($context['_seq'] as $context["_key"] => $context["country"]) {
            // line 23
            echo "                                <div class=\"panel panel-default countries\" style=\"display:none\">
                                        <p class=\"panel-title-countries\">
                                            <a href=\"/";
            // line 25
            echo twig_escape_filter($this->env, (isset($context["type"]) ? $context["type"] : $this->getContext($context, "type")), "html", null, true);
            echo "/countries/";
            echo twig_escape_filter($this->env, $this->getAttribute($context["country"], "id", array()), "html", null, true);
            echo "\">                                                                                    
                                                   ";
            // line 26
            echo twig_escape_filter($this->env, $this->getAttribute($context["country"], "name", array()), "html", null, true);
            echo "
                                            </a>
                                        </p>
                                </div>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['country'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "                    </div>
                </div>
            </div>

            <div class=\"col-sm-9 padding-right\">
                <div class=\"features_items\"><!--features_items-->
                    <h2 class=\"title text-center\">Последние товары</h2>

                    ";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["products"]) ? $context["products"] : $this->getContext($context, "products")));
        foreach ($context['_seq'] as $context["_key"] => $context["product"]) {
            // line 40
            echo "                        <div class=\"col-sm-4\">
                            <div class=\"product-image-wrapper\">
                                <div class=\"single-products\">
                                    <div class=\"productinfo text-center\">
                                        <img src=\"/images/product/";
            // line 44
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo ".jpg\" alt=\"\" />
                                        <h2> ";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "price", array()), "html", null, true);
            echo "</h2>
                                        <p>
                                            <a href=\"/product/";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "\">
                                                ";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "country_name", array()), "html", null, true);
            echo " - ";
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "name", array()), "html", null, true);
            echo "
                                            </a>
                                        </p>
                                        <a href=\"#\" class=\"btn btn-default add-to-cart\" data-id=\"";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["product"], "id", array()), "html", null, true);
            echo "\">
                                            <i class=\"fa fa-shopping-cart\"></i>В корзину
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['product'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        echo "                              

                </div><!--features_items-->
                
            </div>
        </div>
    </div>
</section>

<script>
    \$(document).ready(function(){
        \$(\".countries_touch\").click(function() {
            \$(\".panel.panel-default.countries\").slideToggle( \"fast\" );
            return false;
        });
    });
</script> 
                    
";
        // line 76
        echo twig_include($this->env, $context, "footer.html.twig");
        
        $__internal_2d73a2f9cfa9b34003ef97c0838935bb2d917a247d6c5a5d3fa4a623f4e83306->leave($__internal_2d73a2f9cfa9b34003ef97c0838935bb2d917a247d6c5a5d3fa4a623f4e83306_prof);

        
        $__internal_b4713938d9425661d79c205216a07866455f0b1a1ff9d1faa4981df4883d03af->leave($__internal_b4713938d9425661d79c205216a07866455f0b1a1ff9d1faa4981df4883d03af_prof);

    }

    public function getTemplateName()
    {
        return "category/main.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  167 => 76,  147 => 58,  133 => 51,  125 => 48,  121 => 47,  116 => 45,  112 => 44,  106 => 40,  102 => 39,  92 => 31,  81 => 26,  75 => 25,  71 => 23,  67 => 22,  64 => 21,  52 => 15,  46 => 14,  41 => 11,  37 => 10,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include ('header.html.twig') }}

<section>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-3\">
                <div class=\"left-sidebar\">
                    <div class=\"panel-group category-products\">
                        <h2>Регионы</h2>
                            {% for region in regions %}
                                <div class=\"panel panel-default\">
                                    <div class=\"panel-heading\">
                                        <h4 class=\"panel-title\">
                                            <a href=\"/{{ type }}/{{ regions_for_route[region.id] }}\">                                                                                    
                                                   {{ region.name }}
                                            </a>
                                        </h4>
                                    </div>
                                </div>
                            {% endfor %}
                        <h2><a class=\"countries_touch\" href=\"#\">Страны</a></h2>
                            {% for country in countries %}
                                <div class=\"panel panel-default countries\" style=\"display:none\">
                                        <p class=\"panel-title-countries\">
                                            <a href=\"/{{ type }}/countries/{{ country.id }}\">                                                                                    
                                                   {{ country.name }}
                                            </a>
                                        </p>
                                </div>
                            {% endfor %}
                    </div>
                </div>
            </div>

            <div class=\"col-sm-9 padding-right\">
                <div class=\"features_items\"><!--features_items-->
                    <h2 class=\"title text-center\">Последние товары</h2>

                    {% for product in products %}
                        <div class=\"col-sm-4\">
                            <div class=\"product-image-wrapper\">
                                <div class=\"single-products\">
                                    <div class=\"productinfo text-center\">
                                        <img src=\"/images/product/{{ product.id }}.jpg\" alt=\"\" />
                                        <h2> {{ product.price }}</h2>
                                        <p>
                                            <a href=\"/product/{{ product.id }}\">
                                                {{ product.country_name }} - {{ product.name }}
                                            </a>
                                        </p>
                                        <a href=\"#\" class=\"btn btn-default add-to-cart\" data-id=\"{{ product.id }}\">
                                            <i class=\"fa fa-shopping-cart\"></i>В корзину
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    {% endfor %}                              

                </div><!--features_items-->
                
            </div>
        </div>
    </div>
</section>

<script>
    \$(document).ready(function(){
        \$(\".countries_touch\").click(function() {
            \$(\".panel.panel-default.countries\").slideToggle( \"fast\" );
            return false;
        });
    });
</script> 
                    
{{ include ('footer.html.twig') }}", "category/main.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\category\\main.html.twig");
    }
}
