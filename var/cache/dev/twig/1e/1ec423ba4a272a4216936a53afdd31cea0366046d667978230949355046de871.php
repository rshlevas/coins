<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_63527b96e4ff246ee849178555a0706d301efb43bbb90093f60eed98b4671b62 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3a6f952333a99cf1151bbc4faca8dca69e688cf76ae218d23d59b39bfe44a2d2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3a6f952333a99cf1151bbc4faca8dca69e688cf76ae218d23d59b39bfe44a2d2->enter($__internal_3a6f952333a99cf1151bbc4faca8dca69e688cf76ae218d23d59b39bfe44a2d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_3a6f952333a99cf1151bbc4faca8dca69e688cf76ae218d23d59b39bfe44a2d2->leave($__internal_3a6f952333a99cf1151bbc4faca8dca69e688cf76ae218d23d59b39bfe44a2d2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/hidden_widget.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\hidden_widget.html.php");
    }
}
