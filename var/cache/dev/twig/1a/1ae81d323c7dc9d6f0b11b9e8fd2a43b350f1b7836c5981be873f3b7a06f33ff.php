<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_c1f34452d542fbb19eb577b8a17b0b00d6cc350fad0023f99919d6c0e6e1b469 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6f71c43f2a6453abbb5f0e34a2aa1373be72346648f60cda1ddd6a3c6d542351 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6f71c43f2a6453abbb5f0e34a2aa1373be72346648f60cda1ddd6a3c6d542351->enter($__internal_6f71c43f2a6453abbb5f0e34a2aa1373be72346648f60cda1ddd6a3c6d542351_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, (isset($context["widget"]) ? $context["widget"] : null), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_6f71c43f2a6453abbb5f0e34a2aa1373be72346648f60cda1ddd6a3c6d542351->leave($__internal_6f71c43f2a6453abbb5f0e34a2aa1373be72346648f60cda1ddd6a3c6d542351_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/money_widget.html.php", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\money_widget.html.php");
    }
}
