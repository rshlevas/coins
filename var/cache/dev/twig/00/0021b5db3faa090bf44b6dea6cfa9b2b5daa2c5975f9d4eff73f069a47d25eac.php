<?php

/* cart/checkout.html.twig */
class __TwigTemplate_d812c2f1728d9f88bd2efb6bbda4170cd37874514159c761f153c7e149e45a8d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ef8c0b85a21078e63060400c434fae7041a26e8a863b633d2d565b92e9eeebfa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ef8c0b85a21078e63060400c434fae7041a26e8a863b633d2d565b92e9eeebfa->enter($__internal_ef8c0b85a21078e63060400c434fae7041a26e8a863b633d2d565b92e9eeebfa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "cart/checkout.html.twig"));

        $__internal_77fd0b5a9f7bc62e0b2cc326281265dca125630ff6a7936006dcf3b2b5df9823 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_77fd0b5a9f7bc62e0b2cc326281265dca125630ff6a7936006dcf3b2b5df9823->enter($__internal_77fd0b5a9f7bc62e0b2cc326281265dca125630ff6a7936006dcf3b2b5df9823_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "cart/checkout.html.twig"));

        // line 1
        $this->loadTemplate("header.html.twig", "cart/checkout.html.twig", 1)->display($context);
        // line 2
        echo "
<section>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-4 col-sm-offset-4 padding-right\">
                <div class=\"signup-form\"><!--sign up form-->
                        <h2>Оформление заказа</h2>
                        <h5>Заполните пожалуйста форму</h5>
                            ";
        // line 10
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
                            ";
        // line 11
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'errors');
        echo "

                            <div>
                                ";
        // line 14
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'label');
        echo "
                                ";
        // line 15
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'errors');
        echo "
                                ";
        // line 16
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "name", array()), 'widget');
        echo "
                            </div>
                            <div>
                                ";
        // line 19
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'label');
        echo "
                                ";
        // line 20
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'errors');
        echo "
                                ";
        // line 21
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "email", array()), 'widget');
        echo "
                            </div>
                            <div>
                                ";
        // line 24
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "phone", array()), 'label');
        echo "
                                ";
        // line 25
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "phone", array()), 'errors');
        echo "
                                ";
        // line 26
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "phone", array()), 'widget');
        echo "
                            </div>
                            <div>
                                ";
        // line 29
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "delivery_type", array()), 'label');
        echo "
                                ";
        // line 30
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "delivery_type", array()), 'errors');
        echo "
                                ";
        // line 31
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "delivery_type", array()), 'widget');
        echo "
                            </div>
                            <div>
                                ";
        // line 34
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "payment_type", array()), 'label');
        echo "
                                ";
        // line 35
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "payment_type", array()), 'errors');
        echo "
                                ";
        // line 36
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "payment_type", array()), 'widget');
        echo "
                            </div>
                            <div>
                                ";
        // line 39
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "save", array()), 'widget');
        echo "
                            </div>

                            ";
        // line 42
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
                    </div><!--/sign up form-->
                <h5>Есть вопрос? <a href=\"";
        // line 44
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("contacts");
        echo "\">Напишите</a> нам</h5>
            </div>
        </div>
    </div>
</section>
                    
";
        // line 50
        $this->loadTemplate("footer.html.twig", "cart/checkout.html.twig", 50)->display($context);
        
        $__internal_ef8c0b85a21078e63060400c434fae7041a26e8a863b633d2d565b92e9eeebfa->leave($__internal_ef8c0b85a21078e63060400c434fae7041a26e8a863b633d2d565b92e9eeebfa_prof);

        
        $__internal_77fd0b5a9f7bc62e0b2cc326281265dca125630ff6a7936006dcf3b2b5df9823->leave($__internal_77fd0b5a9f7bc62e0b2cc326281265dca125630ff6a7936006dcf3b2b5df9823_prof);

    }

    public function getTemplateName()
    {
        return "cart/checkout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 50,  128 => 44,  123 => 42,  117 => 39,  111 => 36,  107 => 35,  103 => 34,  97 => 31,  93 => 30,  89 => 29,  83 => 26,  79 => 25,  75 => 24,  69 => 21,  65 => 20,  61 => 19,  55 => 16,  51 => 15,  47 => 14,  41 => 11,  37 => 10,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include ('header.html.twig') %}

<section>
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-sm-4 col-sm-offset-4 padding-right\">
                <div class=\"signup-form\"><!--sign up form-->
                        <h2>Оформление заказа</h2>
                        <h5>Заполните пожалуйста форму</h5>
                            {{ form_start(form) }}
                            {{ form_errors(form) }}

                            <div>
                                {{ form_label(form.name) }}
                                {{ form_errors(form.name) }}
                                {{ form_widget(form.name) }}
                            </div>
                            <div>
                                {{ form_label(form.email) }}
                                {{ form_errors(form.email) }}
                                {{ form_widget(form.email) }}
                            </div>
                            <div>
                                {{ form_label(form.phone) }}
                                {{ form_errors(form.phone) }}
                                {{ form_widget(form.phone) }}
                            </div>
                            <div>
                                {{ form_label(form.delivery_type) }}
                                {{ form_errors(form.delivery_type) }}
                                {{ form_widget(form.delivery_type) }}
                            </div>
                            <div>
                                {{ form_label(form.payment_type) }}
                                {{ form_errors(form.payment_type) }}
                                {{ form_widget(form.payment_type) }}
                            </div>
                            <div>
                                {{ form_widget(form.save) }}
                            </div>

                            {{ form_end(form) }}
                    </div><!--/sign up form-->
                <h5>Есть вопрос? <a href=\"{{ path('contacts') }}\">Напишите</a> нам</h5>
            </div>
        </div>
    </div>
</section>
                    
{% include ('footer.html.twig') %}", "cart/checkout.html.twig", "D:\\wamp64\\www\\coins\\app\\Resources\\views\\cart\\checkout.html.twig");
    }
}
