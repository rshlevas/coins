<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_10670958a099ebdbc3fd6a8ca638d100afbae552179085589f3f01be28a5da44 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_33d4842c14d9e2cce30a5258ae325251028d09b679111ef8c3e7b1def0e2da6d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_33d4842c14d9e2cce30a5258ae325251028d09b679111ef8c3e7b1def0e2da6d->enter($__internal_33d4842c14d9e2cce30a5258ae325251028d09b679111ef8c3e7b1def0e2da6d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_33d4842c14d9e2cce30a5258ae325251028d09b679111ef8c3e7b1def0e2da6d->leave($__internal_33d4842c14d9e2cce30a5258ae325251028d09b679111ef8c3e7b1def0e2da6d_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_6234ef0b12080140961557428c6cd40e247a2d045a6affb91511812fc72446cf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6234ef0b12080140961557428c6cd40e247a2d045a6affb91511812fc72446cf->enter($__internal_6234ef0b12080140961557428c6cd40e247a2d045a6affb91511812fc72446cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_6234ef0b12080140961557428c6cd40e247a2d045a6affb91511812fc72446cf->leave($__internal_6234ef0b12080140961557428c6cd40e247a2d045a6affb91511812fc72446cf_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_3aeab0800018e29113a6272e443fdd949bf6ce3601e8fcfdcf28e87f2fd04647 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3aeab0800018e29113a6272e443fdd949bf6ce3601e8fcfdcf28e87f2fd04647->enter($__internal_3aeab0800018e29113a6272e443fdd949bf6ce3601e8fcfdcf28e87f2fd04647_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_3aeab0800018e29113a6272e443fdd949bf6ce3601e8fcfdcf28e87f2fd04647->leave($__internal_3aeab0800018e29113a6272e443fdd949bf6ce3601e8fcfdcf28e87f2fd04647_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_622aec55ccadbe9780e48ddf132f2c767592cd0efd0a7924c8f67e2386a12f5a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_622aec55ccadbe9780e48ddf132f2c767592cd0efd0a7924c8f67e2386a12f5a->enter($__internal_622aec55ccadbe9780e48ddf132f2c767592cd0efd0a7924c8f67e2386a12f5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_622aec55ccadbe9780e48ddf132f2c767592cd0efd0a7924c8f67e2386a12f5a->leave($__internal_622aec55ccadbe9780e48ddf132f2c767592cd0efd0a7924c8f67e2386a12f5a_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 33,  108 => 10,  97 => 7,  85 => 34,  83 => 33,  73 => 26,  63 => 19,  56 => 15,  50 => 11,  48 => 10,  44 => 9,  40 => 8,  36 => 7,  30 => 4,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Twig/layout.html.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\layout.html.twig");
    }
}
