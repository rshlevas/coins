<?php

/* @FOSUser/Profile/show.html.twig */
class __TwigTemplate_3a65611645fe0f563c8d771e0f1cdb38e0daef14c10b74f9917bbd2320982519 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_50c807d0648fb47cdce990df31d26a339fbc2c8c187619491a25c17a7cb36d4d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_50c807d0648fb47cdce990df31d26a339fbc2c8c187619491a25c17a7cb36d4d->enter($__internal_50c807d0648fb47cdce990df31d26a339fbc2c8c187619491a25c17a7cb36d4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show.html.twig"));

        $__internal_b03801cf2f0c932740469f165dbe613567dc84f2d9c4d67894f13c96854b8707 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b03801cf2f0c932740469f165dbe613567dc84f2d9c4d67894f13c96854b8707->enter($__internal_b03801cf2f0c932740469f165dbe613567dc84f2d9c4d67894f13c96854b8707_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Profile/show.html.twig"));

        // line 1
        $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Profile/show.html.twig", 1)->display($context);
        // line 2
        echo "
<p><a href=\"";
        // line 3
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_profile_edit");
        echo "\">Edit</a></p>
<br>
<p><a href=\"";
        // line 5
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_profile_history");
        echo "\">History</a></p>

";
        // line 7
        $this->displayBlock('fos_user_content', $context, $blocks);
        
        $__internal_50c807d0648fb47cdce990df31d26a339fbc2c8c187619491a25c17a7cb36d4d->leave($__internal_50c807d0648fb47cdce990df31d26a339fbc2c8c187619491a25c17a7cb36d4d_prof);

        
        $__internal_b03801cf2f0c932740469f165dbe613567dc84f2d9c4d67894f13c96854b8707->leave($__internal_b03801cf2f0c932740469f165dbe613567dc84f2d9c4d67894f13c96854b8707_prof);

    }

    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_cf2d364a98feb114903eb422002885de81e45761e5a3412313003290e7f79e23 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cf2d364a98feb114903eb422002885de81e45761e5a3412313003290e7f79e23->enter($__internal_cf2d364a98feb114903eb422002885de81e45761e5a3412313003290e7f79e23_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_c893f043aded0ee487d6ebfe2174a6eb2745beea3eb7d758c162d00adc8a2611 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c893f043aded0ee487d6ebfe2174a6eb2745beea3eb7d758c162d00adc8a2611->enter($__internal_c893f043aded0ee487d6ebfe2174a6eb2745beea3eb7d758c162d00adc8a2611_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 8
        $this->loadTemplate("@FOSUser/Profile/show_content.html.twig", "@FOSUser/Profile/show.html.twig", 8)->display($context);
        
        $__internal_c893f043aded0ee487d6ebfe2174a6eb2745beea3eb7d758c162d00adc8a2611->leave($__internal_c893f043aded0ee487d6ebfe2174a6eb2745beea3eb7d758c162d00adc8a2611_prof);

        
        $__internal_cf2d364a98feb114903eb422002885de81e45761e5a3412313003290e7f79e23->leave($__internal_cf2d364a98feb114903eb422002885de81e45761e5a3412313003290e7f79e23_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Profile/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 8,  41 => 7,  36 => 5,  31 => 3,  28 => 2,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% include \"@FOSUser/layout.html.twig\" %}

<p><a href=\"{{ path('fos_user_profile_edit') }}\">Edit</a></p>
<br>
<p><a href=\"{{ path('fos_user_profile_history') }}\">History</a></p>

{% block fos_user_content %}
{% include \"@FOSUser/Profile/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "@FOSUser/Profile/show.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Profile\\show.html.twig");
    }
}
