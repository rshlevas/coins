<?php

/* WebProfilerBundle:Profiler:open.html.twig */
class __TwigTemplate_c931d96f985a9e3038eb9e7a220f1769e34179a6346d6502a95ddfad70f436ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "WebProfilerBundle:Profiler:open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_289c943d73f8441698cf5a4dbc6281c1b41db8fec5a077d3d25d820791804521 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_289c943d73f8441698cf5a4dbc6281c1b41db8fec5a077d3d25d820791804521->enter($__internal_289c943d73f8441698cf5a4dbc6281c1b41db8fec5a077d3d25d820791804521_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_289c943d73f8441698cf5a4dbc6281c1b41db8fec5a077d3d25d820791804521->leave($__internal_289c943d73f8441698cf5a4dbc6281c1b41db8fec5a077d3d25d820791804521_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_2a09b434b3f50ec60c55c0dbc796962139ac39334ac6bdcb9e5f0ac080345cee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a09b434b3f50ec60c55c0dbc796962139ac39334ac6bdcb9e5f0ac080345cee->enter($__internal_2a09b434b3f50ec60c55c0dbc796962139ac39334ac6bdcb9e5f0ac080345cee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_2a09b434b3f50ec60c55c0dbc796962139ac39334ac6bdcb9e5f0ac080345cee->leave($__internal_2a09b434b3f50ec60c55c0dbc796962139ac39334ac6bdcb9e5f0ac080345cee_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_ceb9874b8935e4f3e543ad8c27faa83ef1935ec69f1317a5bd259084f79a6c6a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ceb9874b8935e4f3e543ad8c27faa83ef1935ec69f1317a5bd259084f79a6c6a->enter($__internal_ceb9874b8935e4f3e543ad8c27faa83ef1935ec69f1317a5bd259084f79a6c6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["file"]) ? $context["file"] : null), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, (isset($context["line"]) ? $context["line"] : null), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt((isset($context["filename"]) ? $context["filename"] : null), (isset($context["line"]) ? $context["line"] : null),  -1);
        echo "
</div>
";
        
        $__internal_ceb9874b8935e4f3e543ad8c27faa83ef1935ec69f1317a5bd259084f79a6c6a->leave($__internal_ceb9874b8935e4f3e543ad8c27faa83ef1935ec69f1317a5bd259084f79a6c6a_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 15,  69 => 12,  63 => 11,  60 => 10,  54 => 9,  44 => 5,  41 => 4,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "WebProfilerBundle:Profiler:open.html.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}
