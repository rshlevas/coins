<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_71949274fed030bd11cd60103deffc62db4250c202bb90b360a356bb462c54a6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8d85417aade412cf7d33ea4b9ce0c8ec5fa2f4ae3aab3cf2c0aa4bd6871c4b38 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8d85417aade412cf7d33ea4b9ce0c8ec5fa2f4ae3aab3cf2c0aa4bd6871c4b38->enter($__internal_8d85417aade412cf7d33ea4b9ce0c8ec5fa2f4ae3aab3cf2c0aa4bd6871c4b38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8d85417aade412cf7d33ea4b9ce0c8ec5fa2f4ae3aab3cf2c0aa4bd6871c4b38->leave($__internal_8d85417aade412cf7d33ea4b9ce0c8ec5fa2f4ae3aab3cf2c0aa4bd6871c4b38_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_9321d61e06307430c0cfa57278be8f36407681962312e61b91e7daea8621a190 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9321d61e06307430c0cfa57278be8f36407681962312e61b91e7daea8621a190->enter($__internal_9321d61e06307430c0cfa57278be8f36407681962312e61b91e7daea8621a190_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_9321d61e06307430c0cfa57278be8f36407681962312e61b91e7daea8621a190->leave($__internal_9321d61e06307430c0cfa57278be8f36407681962312e61b91e7daea8621a190_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_a0297eea57274962d40db33a243b3c9b88ed372cab30f7c769595c3a44564e9f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a0297eea57274962d40db33a243b3c9b88ed372cab30f7c769595c3a44564e9f->enter($__internal_a0297eea57274962d40db33a243b3c9b88ed372cab30f7c769595c3a44564e9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_a0297eea57274962d40db33a243b3c9b88ed372cab30f7c769595c3a44564e9f->leave($__internal_a0297eea57274962d40db33a243b3c9b88ed372cab30f7c769595c3a44564e9f_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_2a5f6571b9fd2a4fe34f823d1dfeb22df48e5eb972bb33e1990d2aaa9365dcc4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a5f6571b9fd2a4fe34f823d1dfeb22df48e5eb972bb33e1990d2aaa9365dcc4->enter($__internal_2a5f6571b9fd2a4fe34f823d1dfeb22df48e5eb972bb33e1990d2aaa9365dcc4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : null))));
        echo "
";
        
        $__internal_2a5f6571b9fd2a4fe34f823d1dfeb22df48e5eb972bb33e1990d2aaa9365dcc4->leave($__internal_2a5f6571b9fd2a4fe34f823d1dfeb22df48e5eb972bb33e1990d2aaa9365dcc4_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "WebProfilerBundle:Collector:router.html.twig", "D:\\wamp64\\www\\coins\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
