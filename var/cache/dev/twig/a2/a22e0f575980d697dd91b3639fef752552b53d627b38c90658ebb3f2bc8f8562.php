<?php

/* @FOSUser/Group/show.html.twig */
class __TwigTemplate_e4ad29c3942c67e886fd10e485d223a78dea4c003abe5ac504c0944f01ef9cd2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "@FOSUser/Group/show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5d1b0374b5b296472b25a477aae5caebad03db07c3f1f8887e6913d78ba63031 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5d1b0374b5b296472b25a477aae5caebad03db07c3f1f8887e6913d78ba63031->enter($__internal_5d1b0374b5b296472b25a477aae5caebad03db07c3f1f8887e6913d78ba63031_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Group/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5d1b0374b5b296472b25a477aae5caebad03db07c3f1f8887e6913d78ba63031->leave($__internal_5d1b0374b5b296472b25a477aae5caebad03db07c3f1f8887e6913d78ba63031_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_9963dbe0ce81967cd28e0b69d01473e6c630c882c173d3d9f0bc02b3b7943e65 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9963dbe0ce81967cd28e0b69d01473e6c630c882c173d3d9f0bc02b3b7943e65->enter($__internal_9963dbe0ce81967cd28e0b69d01473e6c630c882c173d3d9f0bc02b3b7943e65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/show_content.html.twig", "@FOSUser/Group/show.html.twig", 4)->display($context);
        
        $__internal_9963dbe0ce81967cd28e0b69d01473e6c630c882c173d3d9f0bc02b3b7943e65->leave($__internal_9963dbe0ce81967cd28e0b69d01473e6c630c882c173d3d9f0bc02b3b7943e65_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Group/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@FOSUser/Group/show.html.twig", "D:\\wamp64\\www\\coins\\vendor\\friendsofsymfony\\user-bundle\\Resources\\views\\Group\\show.html.twig");
    }
}
